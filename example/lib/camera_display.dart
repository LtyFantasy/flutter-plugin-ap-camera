
import 'dart:async';
import 'dart:developer';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:ap_camera/ap_camera_plugin.dart';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';

class CameraDisplay extends StatefulWidget {

	final ApCameraPosition? position;

	CameraDisplay(this.position);

	@override
  State<StatefulWidget> createState() {
    return _CameraDisplayState();
  }
}

class _CameraDisplayState extends State<CameraDisplay> {

	bool initOk = false;

	bool _beautifyFilterEnable = false;
	double _filterBright = 0.44;
	double _filterBeauty = 0.68;
	double _filterTone = 0.0;

	bool _faceDetectEnable = false;
	bool _faceTakePhotoEnable = true;

	bool hasCaptureSmile = false;
	bool hasCaptureEyeClose = false;

	late ApCameraController _controller;

	bool isRecording = false;
	List<ApCameraFaceData>? faces;

	late StreamSubscription<ApCameraEvent> _subscription;

	@override
  void initState() {

    super.initState();
    _controller = ApCameraController.camera(
			position: widget.position ?? ApCameraPosition.Front,
			resolution: ApResolution.high,
		);
    _cameraInit();

    _subscription = _controller.addListener((event) {

    	if (event is ApCameraEventFaceDetect) {
    		setState(() {
    		  faces = event.faces;
    		});
			}
    	else if (event is ApCameraEventFaceDetectTakePhoto) {

    		if (event.action == 1) {
    			setState(() {
    			  hasCaptureSmile = true;
    			});
				}
    		else {
    			setState(() {
    			  hasCaptureEyeClose = true;
    			});
				}
			}
		});
  }

  @override
  void dispose() {

		_subscription.cancel();
		_controller.release();
    super.dispose();
  }

	@override
  Widget build(BuildContext context) {

    return Scaffold(

			body: Container(
				alignment: Alignment.center,
				clipBehavior: Clip.hardEdge,
				decoration: BoxDecoration(
					color: Colors.white,
				),
				child: initOk
						? _createContent()
						: Center(child: Text('Loading...'),)
			),
		);
  }

  Future<void> _cameraInit() async {

		/// 创建相机
		await _controller.create();

		/// 相机开始视频流
		await _controller.start();

		if (mounted) {
			setState(() {
				initOk = true;
			});
		}
	}

	/// 内容区
	Widget _createContent() {

		return Stack(
			children: [
				_createTexture(),
				_createTopInfoBanner(),
				_createBottomTools(),
			],
		);
	}

	/// Flutter 纹理接入
	Widget _createTexture() {
		
		return AspectRatio(
			aspectRatio: _controller.cameraInfo.aspectRatio,
			child: Texture(
				textureId: _controller.textureId ?? 0,
			),
		);
	}

	/// 顶部信息栏
	Widget _createTopInfoBanner() {

		Widget child;
		if (faces == null || faces!.length == 0) {
			child = Center(child: Text('没有检测到人脸'));
		}
		else if (faces!.length > 1) {
			child = Center(child: Text('检测到 ${faces!.length} 张人脸'));
		}
		else {
			child = Row(
				mainAxisAlignment: MainAxisAlignment.spaceBetween,
				children: [
					Flexible(
						child: Container(
							child: Text('微笑 ${faces!.first.smile.toStringAsFixed(2)}', style: TextStyle(color: Colors.white),),
						),
					),
					Flexible(
						child: Container(
							child: Text('左眼 ${faces!.first.leftEyeOpen.toStringAsFixed(2)}', style: TextStyle(color: Colors.white),),
						),
					),
					Flexible(
						child: Container(
							child: Text('右眼 ${faces!.first.rightEyeOpen.toStringAsFixed(2)}', style: TextStyle(color: Colors.white),),
						),
					)
				],
			);
		}

		return Positioned(
			top: 0, left: 0, right: 0,
			child: Container(
				height: 80,
				color: Colors.black26,
				child: child,
			),
		);
	}

	/// 底部工具栏
	Widget _createBottomTools() {

		return Positioned(
			left: 0, right: 0, bottom: 0,
			child: Container(
				color: Colors.black26,
				child: Column(
					children: [
						_createIOSFilterParamsAdjust(),
						Row(
							mainAxisAlignment: MainAxisAlignment.spaceBetween,
							children: [
								_recordButton(),
								_beautyButton(),
								_faceDetectButton(),
								_faceTakePhotoButton(),
							],
						)
					],
				),
			),
		);
	}

	/// 滤镜参数调节栏
	Widget _createIOSFilterParamsAdjust() {

		if (!Platform.isIOS) return Container();
		if (!_beautifyFilterEnable) return Container();

		void _submit() {
			_controller.setBeautifyFilterParams(
				bright: _filterBright,
				beauty: _filterBeauty,
				tone: _filterTone
			);
		}

		return Container(
			child: Column(
				children: [
					Slider(
							value: _filterBright,
							onChanged: (value) => setState(() {_filterBright = value;}),
							onChangeEnd: (value) {
								setState(() {
						  		_filterBright = value;
								});
								_submit();
							}
					),
					Slider(
							value: _filterBeauty,
							onChanged: (value) => setState(() {_filterBeauty = value;}),
							onChangeEnd: (value) {
								setState(() {
									_filterBeauty = value;
								});
								_submit();
							}
					),
					Slider(
							value: _filterTone,
							onChanged: (value) => setState(() {_filterTone = value;}),
							onChangeEnd: (value) {
								setState(() {
									_filterTone = value;
								});
								_submit();
							}
					),
				],
			),
		);
	}

	/// 录制按钮
	Widget _recordButton() {
		return Builder(
			builder: (context) {
				return IconButton(
						icon: Icon(isRecording ? Icons.stop : Icons.slow_motion_video, color: Colors.blue,),
						onPressed: () async {

							if (isRecording) {

								String? filepath = await _controller.stopRecord();
								Scaffold.of(context).showSnackBar(
										SnackBar(content: Text(
												'Record success: $filepath'
										))
								);
							}
							else {

								bool success = await _controller.startRecord();
								if (!success) {
									Scaffold.of(context).showSnackBar(
											SnackBar(content: Text(
													'Start record failed'
											))
									);
									return;
								}
							}

							setState(() {
								isRecording = !isRecording;
							});
						}
				);
			},
		);
	}

	Widget _beautyButton() {

		return GestureDetector(
				onTap: () {

					_controller.setBeautifyFilter(!_beautifyFilterEnable);
					setState(() {
						_beautifyFilterEnable = !_beautifyFilterEnable;
					});
				},
				child: Text(
						_beautifyFilterEnable ? '关闭美颜' : '开启美颜'
				)
		);
	}

	/// 人脸识别开启按钮
	Widget _faceDetectButton() {

		return GestureDetector(
			onTap: () {

				_controller.setFaceDetect(!_faceDetectEnable);
				setState(() {
				  _faceDetectEnable = !_faceDetectEnable;
				});
			},
			child: Text(
				_faceDetectEnable ? '关闭人脸检测' : '开启人脸检测'
			)
		);
	}

	/// 开始抓拍人脸
	Widget _faceTakePhotoButton() {

		return GestureDetector(
				onTap: () {

					if (_faceTakePhotoEnable == false || _faceDetectEnable == false) return;
					_controller.startFaceDetectTakePhoto();
					setState(() {
						_faceTakePhotoEnable = false;
					});
				},
				child: Column(
					mainAxisSize: MainAxisSize.min,
					children: [
						Text(
							_faceTakePhotoEnable ? '开始抓拍人脸' : '已经开启抓拍了',
							style: TextStyle(
									color: _faceTakePhotoEnable ? Colors.blue : Colors.grey
							),
						),
						Text(
							hasCaptureSmile ? '微笑 - 成功' : '微笑 - 待抓拍',
							style: TextStyle(
								color: hasCaptureSmile ? Colors.green : Colors.grey
							),
						),
						Text(
							hasCaptureEyeClose ? '眨眼 - 成功' : '眨眼 - 待抓拍',
							style: TextStyle(
									color: hasCaptureEyeClose ? Colors.green : Colors.grey
							),
						),
					],
				)
		);
	}
}