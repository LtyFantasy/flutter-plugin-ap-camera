
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

class VideoPlayView extends StatefulWidget {
  
  final String file;
  VideoPlayView(this.file);
  
  @override
  State<StatefulWidget> createState() {
    return VideoPlayViewState();
  }
}

class VideoPlayViewState extends State<VideoPlayView> {
  
  late VideoPlayerController _controller;
  
  @override
  void initState() {
    
    super.initState();
    
    _controller = VideoPlayerController.file(File(widget.file));
    //_controller = VideoPlayerController.network("https://video-stage2.apiteamn.com/2021/05/24/60ab210994395bf7c9c78ed6/index_video.m3u8");
    _controller.initialize().then((_) async {
      await _controller.play();
      setState(() {});
    });
    
    _controller.addListener(() {
      setState(() {});
    });
  }
  
   @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
  
  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      appBar: AppBar(
        title: Text('Video Play'),
      ),
      body: _controller.value.isInitialized
        ? AspectRatio(
          aspectRatio: _controller.value.aspectRatio,
          child: VideoPlayer(_controller),
        )
        : Center(child: Text('Loading...'),),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            
            if (!_controller.value.isPlaying &&
                _controller.value.position.inMilliseconds == _controller.value.duration.inMilliseconds) {
              _controller.seekTo(Duration.zero);
            }
            
            _controller.value.isPlaying
                ? _controller.pause()
                : _controller.play();
          });
        },
        child: Icon(
          _controller.value.isPlaying ? Icons.pause : Icons.play_arrow,
        ),
      ),
    );
  }
}