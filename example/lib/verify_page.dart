import 'dart:async';
import 'dart:developer';
import 'dart:io';
import 'dart:typed_data';

import 'package:ap_camera/ap_camera_plugin.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:just_audio/just_audio.dart';
import 'package:video_player/video_player.dart';

class VerifyPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _VerifyPageState();
  }
}

class _VerifyPageState extends State<VerifyPage> {
  /// 场景初始化标记
  bool _initOk = false;

  /// 场景阶段
  ///
  /// 0 - 人脸识别
  /// 1 - 拍摄预备，贴纸准备
  /// 2 - 倒计时播放，隐藏其他所有按钮
  /// 3 - 拍摄中
  /// 4 - 拍摄完毕，合成中
  /// 5 - 合成完毕，准备上传
  /// 6 - 上传完毕
  int _stage = 0;

  /// SDK 控制器
  ApCameraController? _controller;

  /// SDK 控制器事件监听
  StreamSubscription<ApCameraEvent>? _subscription;

  /// 上一次识别到人脸的时间，用于录制中2秒丢失检测
  int latestFaceDetectTimeMS = 0;

  /// 是否正在停止认证中
  bool isStoppingVerify = false;

  /// 美颜开关状态
  bool _beautyEnable = false;

  /// 声音开关状态
  bool _audioEnable = true;

  /// 录制中时间进度
  int _recordingTimeMS = 0;

  /// 视频文件播放器
  late VideoPlayerController? _player;

  /// 上传中进度
  double _uploadProgress = 0;

  /// 音频播放器
  late AudioPlayer? _audioPlayer;

  /// -------- 测试 --------

  /// 屏幕尺寸比例
  late double _ratio;

  /// 屏幕高dp
  late double _height;

  /// 屏幕宽dp
  late double _width;

  double _cameraHeight = 1280;

  double _cameraWidth = 720;

  /// 人脸识别框框
  Rect? _faceRect;

  /// 人脸识别二进制数据
  Uint8List? _faceData;

  @override
  void initState() {
    super.initState();

    _sdkInit();
    _flareInit();
    _audioInit();
  }

  @override
  void dispose() {
    _subscription?.cancel();
    _controller?.release();
    _player?.dispose();
    _audioPlayer?.dispose();
    super.dispose();
  }

  /// APCamera SDK 初始化
  void _sdkInit() {
    /// 摄像头控制器
    _controller = ApCameraController.camera(
      position: ApCameraPosition.Front,
      resolution: ApResolution.high,
    );
    _cameraInit();

    // 事件监听
    _subscription = _controller?.addListener((event) async {
      /// 人脸识别
      if (event is ApCameraEventFaceDetect) {
        // 如果在阶段0，识别到人脸后就可以进入阶段1
        if (_stage == 0 && event.faces.length > 0 && mounted) {
          // 加载展示 Emoji
          await _controller?.showEmoji(true);
          // 播放Emoji
          await _controller?.startEmojiGif();

          // 进入阶段1
          if (mounted) {
            setState(() {
              _stage = 1;
            });
          }
        }
        // 录制中
        else if (_stage == 2 || _stage == 3) {
          if (latestFaceDetectTimeMS == 0) {
            latestFaceDetectTimeMS = DateTime.now().millisecondsSinceEpoch;
          }

          if (event.faces.length > 0) {
            latestFaceDetectTimeMS = DateTime.now().millisecondsSinceEpoch;
          } else {
            int currentMS = DateTime.now().millisecondsSinceEpoch;
            if (currentMS - latestFaceDetectTimeMS > 2 * 1000 &&
                isStoppingVerify == false) {
              if (mounted) {
                isStoppingVerify = true;
                log('未识别到人脸超过2秒，停止认证');
                _controller?.stopVerify().then((value) async {
                  await _return();
                  isStoppingVerify = false;
                });
              }
            }
          }
        }

        if (event.faces.length > 0) {
          ApCameraFaceData face = event.faces.first;
          log('识别到人脸， 图片数据(${event.data.length})， 人脸 ${event.faces}');

          double ratio = _width / _cameraWidth;
          setState(() {
            _faceData = event.data;
            _faceRect = Rect.fromLTWH(face.x * ratio, face.y * ratio,
                face.width * ratio, face.height * ratio);
          });
        }
      }

      /// 录制中，时间进度通知
      else if (event is ApCameraEventRecordingProgress) {
        if (mounted) {
          setState(() {
            _recordingTimeMS = event.timeMS;
          });
        }
      }

      /// 录制完成通知
      else if (event is ApCameraEventVerifyRecordComplete) {
        if (mounted) {
          setState(() {
            _stage = 4;
            _controller?.startVideoBGMCompose(videoFile: event.videoFile);
          });
        }
      }

      /// 视频BGM合成进度通知
      else if (event is ApCameraEventVideoBGMCompose) {
        if (mounted) {
          // 合成完毕，进入阶段5
          if (event.videoFile != null) {
            // 暂停摄像头输出
            await _controller?.pause();
            // 开始播放视频文件
            _player = VideoPlayerController.file(File(event.videoFile));
            _player?.setVolume(_audioEnable ? 1.0 : 0.0);

            await _player?.initialize().then((value) {
              _player?.play();
              _player?.setLooping(true);
            });

            setState(() {
              _stage = 5;
            });
          } else {
            // 错误处理
            if (mounted) {
              Scaffold.of(context).showSnackBar(SnackBar(
                content: Text('Compose error: ${event.toString()}'),
              ));
            }
          }
        }
      } else if (event is ApCameraEventFaceDetectTakePhoto) {
        if (event.action == 1) {
        } else {}
      }
    });

    // Future.delayed(Duration(seconds: 1),()async{
    //   // 加载展示 Emoji
    //   await _controller??.showEmoji(true);
    //   // 播放Emoji
    //   await _controller??.startEmojiGif();
    //   // 进入阶段1
    //   if (mounted) {
    //     setState(() {
    //       _stage = 1;
    //     });
    //   }
    // });
  }

  /// 摄像机初始化
  Future<void> _cameraInit() async {
    // 创建并初始化摄像头
    await _controller?.create();
    await _controller?.start();
    // 获取Emoji贴纸组
    List<String> emojis = await _controller!.getAllEmojis();
    log("获取到Emojis: $emojis");
    // TODO 根据性别选择Emoji，这里临时写死
    await _controller?.useEmojis(['smile', 'blink', 'angry', 'surprise']);

    if (mounted) {
      // 初始化完成后就开启人脸识别
      _controller?.setFaceDetect(true);
      _controller?.setFaceDetectPhotoDataEnable(true);
      setState(() {
        _initOk = true;
      });
    }
  }

  /// Flare初始化预热
  void _flareInit() {}

  /// 音频播放初始化
  void _audioInit() {
    _audioPlayer = AudioPlayer();
    _audioPlayer!.setAsset('assets/verify_play_bgm.mp3');
  }

  @override
  Widget build(BuildContext context) {
    if (_ratio == null) {
      _ratio = MediaQuery.of(context).devicePixelRatio;
      _width = MediaQuery.of(context).size.width;
      _height = MediaQuery.of(context).size.height;
    }

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        automaticallyImplyLeading: _stage <= 1,
      ),
      extendBodyBehindAppBar: true,
      body: Stack(
        children: [
          _buildBody(),
          _buildFaceBorder(),
          _buildFaceData(),
        ],
      ),
    );
  }

  /// 人脸识别定位框
  Widget _buildFaceBorder() {
    if (_faceRect == null) return Container();

    return Positioned(
      top: _faceRect!.top,
      left: _faceRect!.left,
      child: Container(
        width: _faceRect!.width,
        height: _faceRect!.height,
        decoration:
            BoxDecoration(border: Border.all(color: Colors.red, width: 1)),
      ),
    );
  }

  /// 人脸识别图片数据
  Widget _buildFaceData() {
    // if (_faceData == null)
    return Container();
    // return Positioned(
    //   top: 200,
    //   left: 0,
    //   child: Container(
    //     width: 100,
    //     height: 100,
    //     child: Image.memory(_faceData),
    //   ),
    // );
  }

  Widget _buildBody() {
    if (!_initOk)
      return Container(
        alignment: Alignment.center,
        child: Text('初始化中....'),
      );

    switch (_stage) {
      case 0:
        return _stage0Content();
      case 1:
        return _stage1Content();
      case 2:
        return _stage2Content();
      case 3:
        return _stage3Content();
      case 4:
        return _stage4Content();
      case 5:
        return _stage5Content();
      case 6:
        return _stage6Content();
    }
    return _createTexture();
  }

  /// 阶段0 - 人脸识别
  Widget _stage0Content() {
    return Stack(
      children: [
        _createTexture(),
        Container(
          color: Colors.black45,
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.only(top: 80),
                child: Image.asset('assets/person_no_face.png'),
              ),
              Container(
                margin: EdgeInsets.only(top: 80),
                alignment: Alignment.topCenter,
                child: Container(
                    height: 40,
                    padding: EdgeInsets.only(left: 20, right: 20),
                    decoration: BoxDecoration(
                      color: Colors.black87,
                      borderRadius: BorderRadius.circular(25),
                    ),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Image.asset(
                          'assets/failure_notice.png',
                          width: 20,
                          height: 20,
                        ),
                        SizedBox(
                          width: 20,
                        ),
                        Text(
                          'Make sure your face is exposed',
                          style: TextStyle(color: Colors.white),
                        )
                      ],
                    )),
              ),
            ],
          ),
        )
      ],
    );
  }

  /// 阶段1 - 准备拍摄阶段
  Widget _stage1Content() {
    return Stack(
      children: [
        _createTexture(),
        _createBottomItems(
            otherButton: [
              _createBeautyButton(),
              _createAudioButton(),
            ],
            mainIcon: 'assets/record.png',
            mainOnTap: () {
              // 进入阶段2 - 倒计时
              setState(() {
                _stage = 2;

                _audioPlayer?.seek(Duration.zero);
                _audioPlayer?.play().then((value) {
                  _audioPlayer?.setVolume(_audioEnable ? 1.0 : 0.0);
                });
              });
            }),
      ],
    );
  }

  /// 阶段2 - 倒计时 3-2-1-Go
  Widget _stage2Content() {
    return Stack(
      children: [
        _createTexture(),
        _createCountDownFlare(),
        _createBottomItems(otherButton: [_createAudioButton()]),
      ],
    );
  }

  /// 阶段3 - 拍摄中
  Widget _stage3Content() {
    final int second = _recordingTimeMS ~/ 1000;
    final double progress = _recordingTimeMS / (11 * 1000);
    return Stack(
      children: [
        _createTexture(),
        _createBottomItems(
            otherButton: [_createAudioButton()],
            mainTitle: '$second',
            mainProgress: progress),
      ],
    );
  }

  /// 阶段4 - 视频BGM合成中
  Widget _stage4Content() {
    return Stack(
      children: [
        _createTexture(),
        _createBottomItems(
          otherButton: [_createAudioButton()],
          mainTitle: '处理中',
        ),
      ],
    );
  }

  /// 阶段5 - 合成完毕，准备上传
  Widget _stage5Content() {
    return Stack(
      children: [
        _playerView(size: MediaQuery.of(context).size),
        _createBottomItems(
            otherButton: [
              _createReturnButton(),
              _createAudioButton(),
            ],
            mainTitle: _uploadProgress <= 0 ? '上传' : '上传中',
            mainStyle: TextStyle(fontSize: 18, color: Colors.white),
            mainProgress: _uploadProgress,
            mainOnTap: () {
              // TODO 假的上传进度条
              Timer.periodic(Duration(milliseconds: 50), (timer) {
                _uploadProgress += 0.02;
                if (_uploadProgress > 1) {
                  _uploadProgress = 1;
                  timer.cancel();
                }

                if (mounted) {
                  // 进入最终阶段，只能静音播放
                  _player?.setVolume(0);
                  setState(() {
                    if (_uploadProgress >= 1) {
                      _stage = 6;
                    }
                  });
                }
              });
            }),
      ],
    );
  }

  /// 阶段6 - 上传完毕
  Widget _stage6Content() {
    return Container(
      padding: EdgeInsets.only(left: 40, right: 40),
      color: Colors.black,
      child: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Column(
                children: [
                  Expanded(
                    child: LayoutBuilder(builder:
                        (BuildContext context, BoxConstraints constraints) {
                      return _playerView(
                          size: Size(
                              constraints.maxWidth, constraints.maxHeight));
                    }),
                  ),
                  SizedBox(
                    height: 40,
                  ),
                  Container(
                    child: Text(
                      'Your video is being reviewed and you will get the badge once it\'s approved.'
                      '\nYou can check it in your profile page.',
                      style: TextStyle(fontSize: 16, color: Colors.white),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(
              height: 40,
            ),
            GestureDetector(
              onTap: () {
                Navigator.of(context).pop();
              },
              child: Container(
                height: 60,
                decoration: BoxDecoration(
                    color: Colors.red, borderRadius: BorderRadius.circular(30)),
                alignment: Alignment.center,
                child: Text(
                  'Got It',
                  style: TextStyle(fontSize: 18, color: Colors.white),
                ),
              ),
            ),
            SizedBox(
              height: 40,
            ),
          ],
        ),
      ),
    );
  }

  /// 底部按钮栏
  Widget _createBottomItems({
    String? mainTitle,
    TextStyle? mainStyle,
    String? mainIcon,
    double? mainProgress,
    VoidCallback? mainOnTap,
    List<Widget>? otherButton,
  }) {
    List<Widget> children = [];
    if (mainTitle != null || mainIcon != null) {
      children.add(_createMainButton(
        title: mainTitle,
        style: mainStyle,
        iconAsset: mainIcon,
        progress: mainProgress,
        onTap: mainOnTap,
      ));
    }

    if (otherButton != null && otherButton.length > 0) {
      children.addAll(otherButton);
    }

    return Positioned(
      bottom: 0,
      left: 0,
      right: 0,
      child: Container(
        height: 100,
        child: Stack(
          children: children,
        ),
      ),
    );
  }

  /// 底部主按钮
  Widget _createMainButton(
      {String? title,
      String? iconAsset,
      TextStyle? style,
      VoidCallback? onTap,
      double? progress}) {
    Widget content;
    if (iconAsset != null) {
      content = Image.asset(iconAsset);
    } else {
      content = Text(
        title ?? '',
        style: style ?? TextStyle(fontSize: 24, color: Colors.white),
      );
    }

    return Container(
      alignment: Alignment.topCenter,
      child: GestureDetector(
        onTap: onTap,
        child: Stack(
          alignment: Alignment.center,
          children: [
            Container(
              width: 80,
              height: 80,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: Colors.black45,
                  borderRadius: BorderRadius.circular(40)),
              child: content,
            ),
            Container(
              width: 72,
              height: 72,
              child: CircularProgressIndicator(
                value: progress ?? 0,
                strokeWidth: 8,
                valueColor: AlwaysStoppedAnimation<Color>(Colors.green),
              ),
            )
          ],
        ),
      ),
    );
  }

  /// 美颜开关
  Widget _createBeautyButton() {
    return _createIconButton(
        left: 40,
        top: 15,
        imageAsset: _beautyEnable
            ? 'assets/beauty_enable.png'
            : 'assets/beauty_disable.png',
        onTap: () async {
          await _controller?.setBeautifyFilter(!_beautyEnable);
          setState(() {
            _beautyEnable = !_beautyEnable;
          });
        });
  }

  /// 返回开关
  Widget _createReturnButton() {
    return _createIconButton(
        left: 40,
        top: 15,
        imageAsset: 'assets/return.png',
        onTap: () {
          _return();
        });
  }

  /// 返回
  Future<void> _return() async {
    if (mounted) {
      setState(() {
        _recordingTimeMS = 0;
        _stage = 1;
      });
    }

    try {
      await _audioPlayer?.pause();
      await _audioPlayer?.seek(Duration.zero);
    }catch(e){}
    await _player?.dispose();
    await _controller?.resume();
    await _controller?.startEmojiGif();
  }

  /// 声音开关
  Widget _createAudioButton() {
    return _createIconButton(
        right: 40,
        top: 15,
        imageAsset: _audioEnable
            ? 'assets/sound_enable.png'
            : 'assets/sound_disable.png',
        onTap: () async {
          if (_stage >= 5) {
            await _player?.setVolume(_audioEnable ? 0.0 : 1.0);
          } else {
            await _audioPlayer?.setVolume(_audioEnable ? 0.0 : 1.0);
          }

          setState(() {
            _audioEnable = !_audioEnable;
          });
        });
  }

  /// 图标小开关
  Widget _createIconButton({
    double? top,
    double? bottom,
    double? left,
    double? right,
    required String imageAsset,
    VoidCallback? onTap,
  }) {
    return Positioned(
      top: top,
      bottom: bottom,
      left: left,
      right: right,
      child: GestureDetector(
        onTap: onTap,
        child: Container(
          width: 50,
          height: 50,
          alignment: Alignment.center,
          child: Image.asset(
            imageAsset,
            width: 30,
            height: 30,
          ),
        ),
      ),
    );
  }

  /// Flutter 纹理接入
  Widget _createTexture() {
    return Container(
      clipBehavior: Clip.hardEdge,
      decoration: BoxDecoration(),
      alignment: Alignment.topCenter,
      child: AspectRatio(
        aspectRatio: _controller!.cameraInfo.aspectRatio,
        child: Texture(
          textureId: _controller?.textureId ?? 0,
        ),
      ),
    );
  }

  /// 倒计时 3-2-1 动画
  Widget _createCountDownFlare() {
    return Container(
      alignment: Alignment.center,
      child: FlareActor(
        'assets/WooPlusCountdown.flr',
        animation: 'Countdown',
        callback: (name) async {
          // 先停止Emoji的Gif轮播
          await _controller?.stopEmojiGif();
          // 开始认证流程
          await _controller?.startVerify();

          await _controller?.startFaceDetectTakePhoto();
          // 进入阶段3，录制中
          setState(() {
            _stage = 3;
          });
        },
      ),
    );
  }

  /// 播放器View
  Widget _playerView({required Size size}) {
    if (_player == null) return Container();
    final double deviceRatio = size.width / size.height;
    return Container(
      clipBehavior: Clip.hardEdge,
      decoration: BoxDecoration(),
      alignment: Alignment.center,
      child: Transform.scale(
        scale: _player!.value.aspectRatio / deviceRatio,
        child: AspectRatio(
          aspectRatio: _player!.value.aspectRatio,
          child: VideoPlayer(_player!),
        ),
      ),
    );
  }
}
