
import 'dart:developer';

import 'package:ap_camera/ap_camera_plugin.dart';
import 'package:flutter/material.dart';

class MethodItem {
  
  final String title;
  final Function func;
  MethodItem({required this.title, required this.func});
}

class IOSDebugView extends StatefulWidget {
  
  @override
  State<StatefulWidget> createState() {
    return IOSDebugViewState();
  }
}

class IOSDebugViewState extends State<IOSDebugView> {
  
  bool initOk = false;
  late ApCameraController _controller;
  late List<MethodItem> items;
  
  @override
  void initState() {
    
    super.initState();
    _controller = ApCameraController.frontCamera();
    _methodInit();
  }
  
  void _methodInit() {
    
    items = [
      MethodItem(title: '创建摄像头', func: () async {
        await _controller.create();
        setState(() {
          initOk = true;
        });
        log('创建摄像头 ok');
      }),
      MethodItem(title: '释放摄像头', func: () async {
        await _controller.release();
        setState(() {
          initOk = false;
        });
        log('释放摄像头 ok');
      }),
      MethodItem(title: '开始视频流', func: () async {
        await _controller.start();
        log('开始视频流 ok');
      }),
      MethodItem(title: '暂停视频流', func: () async {
        await _controller.pause();
        log('暂停视频流 ok');
      }),
      MethodItem(title: '恢复视频流', func: () async {
        await _controller.resume();
        log('恢复视频流 ok');
      }),
      MethodItem(title: '开启美颜', func: () async {
        await _controller.setBeautifyFilter(true);
        log('开启美颜 ok');
      }),
      MethodItem(title: '关闭美颜', func: () async {
        await _controller.setBeautifyFilter(false);
        log('关闭美颜 ok');
      }),
      MethodItem(title: '显示Emoji', func: () async {
        await _controller.showEmoji(true);
        log('显示Emoji ok');
      }),
      MethodItem(title: '关闭Emoji', func: () async {
        await _controller.showEmoji(false);
        log('关闭Emoji ok');
      }),
      MethodItem(title: '开始Emoji Gif轮播', func: () async {
        await _controller.startEmojiGif();
        log('开始Emoji Gif轮播 ok');
      }),
      MethodItem(title: '关闭Emoji Gif轮播', func: () async {
        await _controller.stopEmojiGif();
        log('关闭Emoji Gif轮播 ok');
      }),
    ];
  }
  
  @override
  void dispose() {
    
    _controller.release();
    super.dispose();
  }
  
  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      appBar: AppBar(
        title: Text('IOS Debug'),
      ),
      body: Stack(
        children: [
          initOk ? _createTexture() : Center(child: Text('Loading...'),),
          Positioned(
            bottom: 0, left: 0, right: 0,
            child: Container(
              color: Colors.black54,
              height: MediaQuery.of(context).size.height / 2,
              child: ListView.builder(
                  itemCount: items.length,
                  itemBuilder: _itemBuilder
              ),
            ),
          )
        ],
      )
    );
  }

  Widget _createTexture() {
  
    final Size screenSize = MediaQuery.of(context).size;
    final double deviceRatio = screenSize.width / screenSize.height;
    final double cameraRatio = 480 / 640;
  
    return Container(
      alignment: Alignment.center,
      child: Transform.scale(
        scale: cameraRatio / deviceRatio,
        child: AspectRatio(
          aspectRatio: cameraRatio,
          child: Texture(
            textureId: _controller.textureId ?? 0,
          ),
        ),
      ),
    );
  }
  
  Widget _itemBuilder(context, index) {
    return GestureDetector(
      onTap: () {
        items[index].func.call();
      },
      child: Container(
        color: Colors.transparent,
        height: 50,
        alignment: Alignment.center,
        child: Text(
          items[index].title,
          style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700, color: Colors.green),
        ),
      ),
    );
  }
}