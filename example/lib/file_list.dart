
import 'package:ap_camera/ap_camera_plugin.dart';
import 'package:ap_camera_example/photo_view.dart';
import 'package:ap_camera_example/video_play_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FileList extends StatefulWidget {
  
  final bool isVideo;

  FileList({required this.isVideo});
  
  @override
  State<StatefulWidget> createState() {
    return FileListState();
  }
}

class FileListState extends State<FileList> {
  
  late List<ApCameraFile> files;
  
  @override
  void initState() {
    
    super.initState();
    getFiles();
  }
  
  Future<void> getFiles() async {
    
    if (widget.isVideo == true) {
      files = await ApCameraController.getVideoFiles();
    }
    else {
      files = await ApCameraController.getPhotoFiles();
    }
    setState(() {});
  }
  
  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.isVideo == true ?  'Video List' : 'Photo List'),
      ),
      body: _createContent(),
    );
  }
  
  Widget _createContent() {
   
    if (files == null) {
      return Center(child: Text('Loading...'),);
    }
    
    if (files.length == 0) {
      return Center(child: Text('No Files'),);
    }
    
    return ListView.builder(
      itemCount: files.length,
      itemBuilder: (context, index) {
        
        return GestureDetector(
          onTap: () {
            Navigator.of(context).push(CupertinoPageRoute(
                builder: (ctx) {
                  
                  if (widget.isVideo == true) {
                    return VideoPlayView(files[index].fullPath);
                  }
                  else {
                    return PhotoView(files[index].fullPath);
                  }
                }
            ));
          },
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            alignment: Alignment.centerLeft,
            color: Colors.white,
            child: Column(
              children: [
                _fileAttrItem(
                  title: 'Filename:',
                  value: files[index].filename,
                ),
                SizedBox(height: 10,),
                _fileAttrItem(
                  title: 'Size:',
                  value: '${(files[index].size / (1024)).toStringAsFixed(2)} KB'
                ),
                SizedBox(height: 10,),
                _fileAttrItem(
                  title: 'CreateAt',
                  value: files[index].createAt.toString()
                ),
                Container(
                  margin: EdgeInsets.only(top: 10),
                  height: 1,
                  color: Colors.grey,
                ),
              ],
            ),
          ),
        );
      },
    );
  }
  
  Widget _fileAttrItem({required String title,required String value}) {
    
    return Row(
      children: [
        Container(
          width: 80,
          child: Text(title),
        ),
        SizedBox(width: 10,),
        Flexible(child: Text(value))
      ],
    );
  }
}