
import 'dart:io';

import 'package:flutter/material.dart';

class PhotoView extends StatelessWidget {
  
  final String file;
  PhotoView(this.file);
  
  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      appBar: AppBar(
        title: Text('Photo'),
      ),
      body: Image.file(File(file)),
    );
  }
}