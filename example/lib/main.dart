
import 'package:ap_camera/ap_camera_plugin.dart';
import 'package:ap_camera_example/camera_display.dart';
import 'package:ap_camera_example/file_list.dart';
import 'package:ap_camera_example/ios_debug_view.dart';
import 'package:ap_camera_example/photo_detect_page.dart';
import 'package:ap_camera_example/verify_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  
  List<ApCameraPosition>? items;
  
  @override
  void initState() {
    
    super.initState();
    getDevices();
  }
  
  /// 异步获取设备列表
  Future<void> getDevices() async {
    
    items = await ApCameraController.getDevices();
    setState(() {});
  }
  
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
          actions: [
            Builder(builder: (context) {
              return IconButton(
                  icon: Icon(Icons.photo),
                  onPressed: () {
                    Navigator.of(context).push(CupertinoPageRoute(
                        builder: (ctx) {
                          //return FileList(isVideo: false);
                          return PhotoDetectPage();
                        }
                    ));
                  }
              );
            }),
            Builder(builder: (context) {
              return IconButton(
                  icon: Icon(Icons.videocam),
                  onPressed: () {
                    Navigator.of(context).push(CupertinoPageRoute(
                        builder: (ctx) {
                          return FileList(isVideo: true);
                        }
                    ));
                  }
              );
            })
          ],
        ),
        body: _createContent(),
      ),
    );
  }
  
  Widget _createContent() {
    
    if (items == null) {
      return Center(
        child: Text('Loading...'),
      );
    }
    
    if (items!.length == 0) {
      return Center(
        child: Text('can not find cameras'),
      );
    }
    
    return ListView.builder(
      itemCount: items!.length + 1,
      itemBuilder: (context, index) {

        
        if (index == items!.length) {
          return GestureDetector(
            onTap: () {
              Navigator.of(context).push(CupertinoPageRoute(
                  builder: (ctx) {
                    //return IOSDebugView();
                    return VerifyPage();
                  }
              ));
            },
            child: Container(
              height: 60,
              color: Colors.white,
              alignment: Alignment.center,
              child: Text(
                '认证例子流程'
              ),
            ),
          );
        }
        else {
          return GestureDetector(
            onTap: () {
              Navigator.of(context).push(CupertinoPageRoute(
                  builder: (ctx) {
                    return CameraDisplay(items![index]);
                  }
              ));
            },
            child: Container(
              color: Colors.white,
              height: 60,
              alignment: Alignment.center,
              child: Text(
                '${items![index]}',
              ),
            ),
          );
        }
      },
    );
  }
}
