
import 'dart:developer';

import 'package:ap_camera/ap_camera_plugin.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class PhotoDetectPage extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return PhotoDetectPageState();
  }
}

class PhotoDetectPageState extends State<PhotoDetectPage> {

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(title: Text('Photo Detect Page'),),
      body: Center(
        child: GestureDetector(
          onTap: () async {
            PickedFile file = await ImagePicker().getImage(source: ImageSource.gallery);
            int faceCount = await ApCameraController.detectFaceWithImageFile(file.path);
            log('识别到 $faceCount 张人脸');
          },
          child: Container(
            width: 100, height: 50,
            child: Text('选择图片图片'),
          ),
        ),
      ),
    );
  }
}