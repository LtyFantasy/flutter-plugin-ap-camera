
import 'package:ap_camera/ap_camera_plugin.dart';
import 'package:flutter/material.dart';

class OpenGLTriangle extends StatefulWidget {
 
  @override
  State<StatefulWidget> createState() {
    return _OpenGLTriangleState();
  }
}

class _OpenGLTriangleState extends State<OpenGLTriangle> {
  
  int textureId = 0;
  
  @override
  void initState() {
    
    super.initState();
    ApCameraPlugin().testOpenGLTriangle().then((value) => {
      setState(() {
        textureId = value;
      })
    });
  }
  
  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      appBar: AppBar(
        title: Text('动画三角形'),
      ),
      body: Container(
        child: Texture(textureId: textureId),
      ),
    );
  }
}