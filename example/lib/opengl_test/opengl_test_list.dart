
import 'package:ap_camera_example/opengl_test/opengl_triangle.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

typedef _TapCallback = void Function(BuildContext context);

class TestItem {
  
  final String title;
  final _TapCallback callback;

  TestItem({required this.title,required this.callback});
}

List<TestItem> items = [
  TestItem(
    title: '动画三角形',
    callback: (context) {
      Navigator.of(context).push(CupertinoPageRoute(
          builder: (ctx) {
            return OpenGLTriangle();
          }
      ));
    }
  )
];

class OpenGLTestList extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      appBar: AppBar(
        title: Text('OpenGL 测试'),
      ),
      body: ListView.builder(
        itemCount: items.length,
        itemBuilder: (context, index) {
          return GestureDetector(
            onTap: () {
              items[index].callback(context);
            },
            child: Container(
              margin: EdgeInsets.only(left: 20),
              height: 60,
              color: Colors.white,
              alignment: Alignment.centerLeft,
              child: Text(items[index].title),
            ),
          );
        },
      ),
    );
  }
}