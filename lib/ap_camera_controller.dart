
part of 'ap_camera_plugin.dart';

/// AP Camera应用控制类
///
/// Flutter与本SDK的所有操作都通过Controller进行
class ApCameraController {
  
  /// 事件流
  final StreamController<ApCameraEvent> eventStream = StreamController<ApCameraEvent>.broadcast();
  
  /// 相机信息
  late ApCameraInfo _cameraInfo;
  ApCameraInfo get cameraInfo => _cameraInfo;
  
  /// 摄像机纹理Id
  int? _textureId;
  int? get textureId => _textureId;

  /// ------------ 初始化 ------------
  
  /// 指定摄像头和分辨率
  ApCameraController.camera({required ApCameraPosition position,required ApResolution resolution}) {
    _cameraInfo = ApCameraInfo.newCamera(
      position: position,
      resolution: resolution,
    );
    
    ApCameraPlugin.setNativeMethodCallback(_nativeEventNotify);
  }

  /// 默认前置摄像头
  ///
  /// high 1280x720
  ApCameraController.frontCamera() {
    
    _cameraInfo = ApCameraInfo.newCamera(
      position: ApCameraPosition.Front,
      resolution: ApResolution.high,
    );

    ApCameraPlugin.setNativeMethodCallback(_nativeEventNotify);
  }

  /// 销毁
  void dispose() {
    eventStream.close();
    ApCameraPlugin.setNativeMethodCallback(null);
  }

  /// ------------ 原生事件监听 ------------
  
  /// 接收原生事件
  void _nativeEventNotify(ApCameraEvent event) {
    eventStream.add(event);
  }

  /// 注册监听者
  StreamSubscription<ApCameraEvent> addListener(void call(ApCameraEvent event)) {
    return eventStream.stream.listen(call);
  }
  
  /// ------------ 摄像头设置 ------------
  
  /// 获取设备列表
  static Future<List<ApCameraPosition>> getDevices() async {
    
    List<ApCameraPosition> devices = [];
    List<String> positions = await ApCameraPlugin.availableCameras();
    for (String value in positions) {
      devices.add(_positionFromString(value));
    }
    List<dynamic> getAllEmojiStickers = await ApCameraPlugin.getAllEmojiStickers();
    debugPrint("getAllEmojiStickers $getAllEmojiStickers ");
    return devices;
  }
  
  /// 摄像机创建
  ///
  /// 原生创建摄像机对象，并分配texture id
  ///
  /// 注意，因为可能会因为某些原因导致失败创建失败，原生会返回FlutterError异常，注意捕获处理
  Future<void> create() async {
    Map result = await ApCameraPlugin.createCamera(
        position: _positionToString(_cameraInfo.position),
        resolution: _resolutionToString(_cameraInfo.resolution)
    );
    _textureId = result["textureId"] ?? -1;
    _cameraInfo.previewSize = Size(result["width"] ?? 0, result["height"]??0);
  }
  
  /// 释放摄像头
  Future<void> release() async {
    await ApCameraPlugin.releaseCamera();
  }

  /// ------------- 缓存设置 --------------

  /// 清理缓存
  ///
  /// 会移除ApCamera生成的所有图片、视频文件
  static Future<void> cleanAllCache() async {
    await ApCameraPlugin.cleanAllCache();
  }
  
  /// ------------------ 视频、图片处理 -------------------
  
  /// 开始视频流
  Future<void> start() async {
    await ApCameraPlugin.startCamera();
  }

  /// 恢复视频流
  Future<void> resume() async {
    await ApCameraPlugin.resumeCamera();
  }
  
  /// 暂停视频流
  Future<void> pause() async {
    await ApCameraPlugin.pauseCamera();
  }
  
  /// 开始录像
  Future<bool> startRecord() async {
  
    if (_cameraInfo.isRecordingVideo == true) return true;
    
    _cameraInfo.isRecordingVideo = true;
    bool success = await ApCameraPlugin.startRecord();
    if (!success) {
      _cameraInfo.isRecordingVideo = false;
    }
    return success;
  }
  
  /// 停止录像
  ///
  /// 如果录制成功，返回文件path
  /// 如果录制失败，返回null
  Future<String?> stopRecord() async {
    
    if (_cameraInfo.isRecordingVideo == false) return null;
    
    String filepath = await ApCameraPlugin.stopRecord();
    _cameraInfo.isRecordingVideo = false;
    return filepath;
  }
  
  /// 取消录制
  ///
  /// 取消录制并不会生成录像文件
  Future<void> cancelRecord() async {
    
    if (_cameraInfo.isRecordingVideo == false) return null;
    
    await ApCameraPlugin.cancelRecord();
    _cameraInfo.isRecordingVideo = false;
  }
  
  /// 抓拍当前图片
  ///
  /// 如果抓拍成功，返回文件path
  /// 如果抓拍失败，返回null
  Future<String?> takePhoto() async {
    
    if (_cameraInfo.isTakingPicture == true) return null;
    
    _cameraInfo.isTakingPicture = true;
    String filepath = await ApCameraPlugin.takePhoto();
    _cameraInfo.isTakingPicture = false;
    return filepath;
  }
  
  /// 获取录像文件列表
  ///
  /// 文件列表按时间顺序排列，最新的在头部
  static Future<List<ApCameraFile>> getVideoFiles() async {
    
    List<ApCameraFile> files = [];
    List<Map> result = await ApCameraPlugin.getVideoFiles();
    for (Map map in result) {
      try {
        files.add(ApCameraFile.fromMap(map));
      }
      catch (e) {
        log('ApCamera get files exception ${e.toString()}');
      }
    }
    
    files.sort((a, b) {
      return b.createAt.compareTo(a.createAt);
    });
    
    return files;
  }

  /// 获取图片文件列表
  ///
  /// 文件列表按时间顺序排列，最新的在头部
  static Future<List<ApCameraFile>> getPhotoFiles() async {
  
    List<ApCameraFile> files = [];
    List<Map> result = await ApCameraPlugin.getPhotoFiles();
    for (Map map in result) {
      try {
        files.add(ApCameraFile.fromMap(map));
      }
      catch (e) {
        log('ApCamera get files exception ${e.toString()}');
      }
    }

    files.sort((a, b) {
      return b.createAt.compareTo(a.createAt);
    });
  
    return files;
  }

  /// ------------- 滤镜设置 --------------
  
  /// 开关 - 美颜滤镜
  ///
  /// 摄像头开始视频流后，该API才有调用效果
  Future<void> setBeautifyFilter(bool enable) async {
    await ApCameraPlugin.setBeautifyFilter(enable);
  }

  /// ------------- 贴纸设置 --------------
  
  /// 获取所有的Emoji
  ///
  /// eg返回值: [ "angry", "blink", ... ]
  Future<List<String>> getAllEmojis() async {
    List<dynamic> emojis = await ApCameraPlugin.getAllEmojiStickers();
    return emojis.cast<String>();
  }
  
  /// 使用指定的某组Emoji
  ///
  /// 注意，List只能为4个成员
  Future<void> useEmojis(List<String>? emojis) async {
    assert(emojis != null && emojis.length == 4, 'use emojis error: $emojis');
    await ApCameraPlugin.useEmojis(emojis);
  }

  /// 设置是否展示Emoji
  Future<void> showEmoji(bool show) async {
    await ApCameraPlugin.showEmoji(show);
  }
  
  /// 开始轮播Gif
  Future<void> startEmojiGif() async {
    await ApCameraPlugin.startEmojiGif();
  }
  
  /// 停止轮播Gif
  Future<void> stopEmojiGif() async {
    await ApCameraPlugin.stopEmojiGif();
  }
  
  /// 展示Emoji动画2
  Future<void> startEmojiAnimation2() async {
    await ApCameraPlugin.startEmojiAnimation2();
  }
  
  /// ------------- AI识别相关 --------------

  /// 识别单张图片人脸数据
  ///
  /// 提供给外部其他模块用
  ///
  /// 返回：人脸个数
  static Future<int> detectFaceWithImageFile(String filepath) {
    return ApCameraPlugin.detectFaceNumWithImageFile(filepath);
  }

  /// 开关 - 启动人脸识别
  ///
  /// 摄像头开始视频流后，该API才有调用效果
  /// 识别期间，会产生事件[ApCameraEventFaceDetect]
  Future<void> setFaceDetect(bool enable) async {
    await ApCameraPlugin.setFaceDetect(enable);
  }
  
  /// 开启人脸识别抓拍
  ///
  /// 当识别到产生了微笑、眨眼动作后，原生会进行视频抓拍，并保存成文件
  /// 抓拍成功后，会产生事件[ApCameraEventFaceDetectTakePhoto]
  ///
  /// 注意，微笑和眨眼只会各抓拍一次，抓拍成功后，则停止
  ///
  /// @param savePhoto 捕捉到对应表情后，是否抓拍成图片
  Future<void> startFaceDetectTakePhoto({bool savePhoto=true}) async {
    await ApCameraPlugin.startFaceDetectTakePhoto(savePhoto: savePhoto);
  }

  /// 开启、关闭人脸抓拍 - 二进制图片数据传输
  ///
  /// 调用[startFaceDetectTakePhoto]后，该API才有意义
  /// 该功能用作视频认证期间，抓拍用户真实长相
  Future<void> setFaceDetectPhotoDataEnable(bool enable) async {
    await ApCameraPlugin.setFaceDetectPhotoDataEnable(enable);
  }

  /// ------------- 场景玩法 --------------

  /// 开始认证流程
  ///
  /// 1，启动表情动画（缩放+依次Gif轮播展示）
  /// 2，同时启动视频录像
  /// 3，同时开启表情+眨眼抓拍识别
  /// 4，表情动画执行完毕后，停止录像，通知Flutter录制结束，事件[ApCameraEventVerifyRecordComplete]
  Future<void> startVerify({bool lowRecorder=false}) async {
    await ApCameraPlugin.startVerify(lowRecorder);
  }
  
  /// 停止认证流程
  Future<void> stopVerify() async {
    await ApCameraPlugin.stopVerify();
  }
  
  /// 开始进行视频BGM合成
  ///
  /// 原生进行合成期间，会通过事件[ApCameraEventVideoBGMCompose]回调通知Flutter进度
  Future<void> startVideoBGMCompose({required String videoFile}) async {
    await ApCameraPlugin.startVideoBGMCompose(videoFile: videoFile);
  }
}

extension IOSTest on ApCameraController {
  
  /// iOS 美颜滤镜参数调节测试
  Future<void> setBeautifyFilterParams({required double bright,required double beauty,required double tone}) async {
    await ApCameraPlugin().iosAdjustBeautyFilter(
      bright: bright,
      beauty: beauty,
      tone: tone
    );
  }
}

/// 设备位置参数转换 - Enum to String
String _positionToString(ApCameraPosition position) {
  switch(position) {
    case ApCameraPosition.Front:
      return 'front';
    case ApCameraPosition.Back:
      return 'back';
    default:
      return 'unknown';
  }
}

/// 设备位置参数转化 - String to Enum
ApCameraPosition _positionFromString(String position) {
  if (position == 'front') return ApCameraPosition.Front;
  if (position == 'back') return ApCameraPosition.Back;
  return ApCameraPosition.External;
}

/// 分辨率参数转换
String _resolutionToString(ApResolution resolution) {
  switch (resolution) {
    case ApResolution.max:
      return 'max';
    case ApResolution.ultraHigh:
      return 'ultraHigh';
    case ApResolution.veryHigh:
      return 'veryHigh';
    case ApResolution.high:
      return 'high';
    case ApResolution.medium:
      return 'medium';
    case ApResolution.low:
      return 'low';
    default:
      return 'medium';
  }
}