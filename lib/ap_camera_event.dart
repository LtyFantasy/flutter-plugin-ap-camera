part of 'ap_camera_plugin.dart';

/// 原生通知事件
enum ApCameraEventType {
  /// 未知事件（开发阶段的问题）
  Unknown,

  /// 参数数据格式有误（开发阶段的问题）
  ///
  /// 所有的事件参数都是Map，如果不是Map就会生成该错误事件
  InvalidFormat,

  /// 检测到人脸
  FaceDetect,

  /// 检测到人脸识别信息，并抓拍了图片
  ///
  /// 目前触发图片抓拍的信息有三种：微笑、左眼眨眼、右眼眨眼
  FaceDetectTakePhoto,

  /// 录制中，时间进度更新
  RecordingProgress,

  /// 认证录制结束
  ///
  /// 即Emoji表情动画执行完毕，视频录制完毕
  VerifyRecordComplete,

  /// 视频BGM合成进度通知
  VideoBGMCompose,
}

/// 原生通知Flutter层的事件
///
/// 原生传递给Flutter的参数必须是一个[Map]
/// 如果原生需要传递错误信息，则Map至少包含如下两个字段，
/// {
///   'errorCode': int,
///   'errorMsg' : String,
/// }
///
/// 除了[ApCameraEventType.Unknown]和[ApCameraEventType.InvalidParams]，这两个理论属于开发阶段问题
/// 其他的事件如果发生了对应的错误，会设置该事件对象的[errorCode]和[errorMsg]
class ApCameraEvent {
  /// 事件类型
  final ApCameraEventType type;

  /// 事件错误码
  final int errorCode;

  /// 事件错误描述
  final String errorMsg;

  /// 是否有错误
  bool get hasError => errorCode > 0;

  ApCameraEvent._internal({required this.type, Map? arguments})
      : errorCode = arguments?['errorCode'] ?? 0,
        errorMsg = arguments?['errorMsg'] ?? '';

  /// 工厂方法，解析原生通知数据
  ///
  /// 原生通知Flutter层的参数，必须是一个Map
  factory ApCameraEvent({required String method, dynamic arguments}) {
    try {
      if (arguments is! Map) {
        return ApCameraEventInvalidFormat(
            method: method, originParams: arguments);
      }

      if (method == ApCameraEventFaceDetect.Method) {
        return ApCameraEventFaceDetect(arguments);
      } else if (method == ApCameraEventFaceDetectTakePhoto.Method) {
        return ApCameraEventFaceDetectTakePhoto(arguments);
      } else if (method == ApCameraEventRecordingProgress.Method) {
        return ApCameraEventRecordingProgress(arguments);
      } else if (method == ApCameraEventVerifyRecordComplete.Method) {
        return ApCameraEventVerifyRecordComplete(arguments);
      } else if (method == ApCameraEventVideoBGMCompose.Method) {
        return ApCameraEventVideoBGMCompose(arguments);
      }
    } catch ( e, s) {
      return ApCameraEventUnknown(e: e, s: s);
    }
    return ApCameraEventUnknown(method: method);
  }

  @override
  String toString() {
    if (hasError) {
      return 'Event($type), Error($errorCode - $errorMsg)\n';
    } else {
      return 'Event($type)\n';
    }
  }
}

/// 未知 或 异常错误事件
class ApCameraEventUnknown extends ApCameraEvent {
  final String? method;
  final Object? e;
  final StackTrace? s;

  ApCameraEventUnknown({this.method, this.e, this.s})
      : super._internal(type: ApCameraEventType.Unknown);

  @override
  String toString() {
    if (method != null) {
      return '${super.toString()}unknown method : $method';
    } else {
      return '${super.toString()}exception : $e - $s';
    }
  }
}

/// 参数格式错误事件
class ApCameraEventInvalidFormat extends ApCameraEvent {
  final String method;
  final dynamic originParams;

  ApCameraEventInvalidFormat({required this.method,required this.originParams})
      : super._internal(type: ApCameraEventType.InvalidFormat);

  @override
  String toString() {
    return '${super.toString()}method($method) params($originParams)';
  }
}

/// 人脸数据
class ApCameraFaceData {
  /// 人脸坐标 - X
  double? _x;

  double get x => _x ?? 0;

  /// 人脸坐标 - Y
  double? _y;

  double get y => _y ?? 0;

  /// 人脸宽度 - Width
  double? _width;

  double get width => _width ?? 0;

  /// 人脸宽度 - Height
  double? _height;

  double get height => _height ?? 0;

  /// 摄像头总宽度
  double? _cameraWidth;

  double get cameraWidth => _cameraWidth ?? 0;

  /// 摄像头总高度
  double? _cameraHeight;

  double get cameraHeight => _cameraHeight ?? 0;

  /// 人脸追踪Id
  int? _trackId;

  int get trackId => _trackId ?? -1;

  /// 微笑概率
  double? _smile;

  double get smile => _smile ?? 0;

  /// 左眼睁开概率
  double? _leftEyeOpen;

  double get leftEyeOpen => _leftEyeOpen ?? 0;

  /// 右眼睁开概率
  double? _rightEyeOpen;

  double get rightEyeOpen => _rightEyeOpen ?? 0;

  ApCameraFaceData({
    required double x,
    required double y,
    required double width,
    required double height,
    required double cameraWidth,
    required double cameraHeight,
    required int trackId,
    required double smile,
    required double leftEyeOpen,
    required double rightEyeOpen,
  })  : _x = x,
        _y = y,
        _width = width,
        _height = height,
        _cameraWidth = cameraWidth,
        _cameraHeight = cameraHeight,
        _trackId = trackId,
        _smile = smile,
        _leftEyeOpen = leftEyeOpen,
        _rightEyeOpen = rightEyeOpen;

  @override
  String toString() {
    return 'frame($_x, $_y, $_width, $_height) trackId($_trackId), smile($_smile), leftEyeOpen($_leftEyeOpen), rightEyeOpen($_rightEyeOpen)';
  }
}

/// 人脸识别事件
///
/// {
///   'data': List<int>,
///   'list': [
///     {
///       'x': int,
///       'y': int,
///       'width': int,
///       'height': int,
///       'trackId': int,
///       'smile': double,
///       'leftEyeOpen': double,
///       'rightEyeOpen': double,
///     }
///   ]
/// }
class ApCameraEventFaceDetect extends ApCameraEvent {
  static const String Method = 'eventFaceDetect';

  /// 图片数据
  late Uint8List _data;

  Uint8List get data => _data;

  /// 人脸
  late List<ApCameraFaceData> _faces;

  List<ApCameraFaceData> get faces => _faces;

  ApCameraEventFaceDetect(Map argument)
      : super._internal(
            type: ApCameraEventType.FaceDetect, arguments: argument) {
    if (argument['data'] is Uint8List) {
      _data = argument['data'];
    } else {
      _data = Uint8List.fromList([]);
    }

    _faces = [];

    List listMap = argument['list'] ?? [];
    for (dynamic map in listMap) {
      if (map is Map) {
        _faces.add(ApCameraFaceData(
          x: map['x'],
          y: map['y'],
          width: map['width'],
          height: map['height'],
          cameraWidth: map['cameraWidth'],
          cameraHeight: map['cameraHeight'],
          trackId: map['trackId'],
          smile: map['smile'],
          leftEyeOpen: map['leftEyeOpen'],
          rightEyeOpen: map['rightEyeOpen'],
        ));
      }
    }
  }

  @override
  String toString() {
    return '${super.toString()}faces: $_faces';
  }
}

/// 检测到人脸相关指定信息，并抓拍了图片
///
/// {
///   'action': int,
///   'timestampMS': int,
///   'photoPath': String,
/// }
class ApCameraEventFaceDetectTakePhoto extends ApCameraEvent {
  static const String Method = 'eventFaceDetectTakePhoto';

  /// 所属动作
  ///
  /// 1 - 微笑(>= 70%)
  /// 2 - 左眼眨眼(<= 10%)
  /// 3 - 右眼眨眼(<= 10%)
  /// 4 - 双眼眨眼(都 <= 10%)
  late int? _action;

  int get action => _action ?? 0;

  /// 录像中的时间戳位置
  ///
  /// 开启录像后，会上报相对于视频开始时间的该事件产生时间
  /// 比如在视频的第 2.45 秒捕捉到了微笑事件
  ///
  /// 如果未开启录像，该值为0
  late int? _timestampMS;

  int get timestampMS => _timestampMS ?? 0;

  /// 抓拍的图片地址
  late String? _photoPath;

  String get photoPath => _photoPath ?? '';

  ApCameraEventFaceDetectTakePhoto(Map argument)
      : super._internal(
            type: ApCameraEventType.FaceDetectTakePhoto, arguments: argument) {
    _action = argument['action'];
    _timestampMS = argument['timestampMS'];
    _photoPath = argument['photoPath'];
  }

  @override
  String toString() {
    return '${super.toString()}action: $_action, timestamp: $_timestampMS ms, photoPath: $_photoPath';
  }
}

/// 录制中，时间进度通知
///
/// {
///   'time': double,
/// }
///
/// example: 视频已经录制了 7.2秒， 则 time = 7200
class ApCameraEventRecordingProgress extends ApCameraEvent {
  static const String Method = 'eventRecordingProgress';

  // 当前时间戳 ms
  late int _timeMS;

  int get timeMS => _timeMS;

  ApCameraEventRecordingProgress(Map argument)
      : super._internal(
            type: ApCameraEventType.RecordingProgress, arguments: argument) {
    _timeMS = argument['timeMS'];
  }
}

/// 认证录制流程结束
///
/// {
///   'videoFile': String,
/// }
class ApCameraEventVerifyRecordComplete extends ApCameraEvent {
  static const String Method = 'eventVerifyRecordComplete';

  // 视频文件路径
  late String? _videoFile;

  String get videoFile => _videoFile ?? '';

  ApCameraEventVerifyRecordComplete(Map argument)
      : super._internal(
            type: ApCameraEventType.VerifyRecordComplete, arguments: argument) {
    _videoFile = argument['videoFile'];
  }
}

/// 视频合成进度通知
///
/// {
///   'videoFile': String
/// }
class ApCameraEventVideoBGMCompose extends ApCameraEvent {
  static const String Method = 'eventVideoBGMCompose';

  /// 视频文件
  String? _videoFile;

  String get videoFile => _videoFile ?? "0";

  ApCameraEventVideoBGMCompose(Map argument)
      : super._internal(
            type: ApCameraEventType.VideoBGMCompose, arguments: argument) {
    _videoFile = argument['videoFile'];
  }
}
