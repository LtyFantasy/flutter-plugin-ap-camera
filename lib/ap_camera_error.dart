
part of 'ap_camera_plugin.dart';

/// 原生通知Flutter层的错误码集合定义
class ApCameraError {
  
  /// --------- 通用错误码 1xxx ---------
  
  /// 参数不正确
  static const int Common_InvalidParams = 1000;
  
  /// --------- 摄像头、音视频相关 2xxx ---------
  
  static const int AV_ComposeFailed = 2020;
  
  /// --------- 滤镜设置相关 3xxx ---------
  
  /// --------- AI人脸识别相关 4xxx ---------
  
  /// 人脸识别，MLKit返回error
  static const int AIFace_Detect = 4000;
  
  /// 人脸抓拍，保存图片文件失败
  static const int AIFace_SavePhotoFile = 4001;
}