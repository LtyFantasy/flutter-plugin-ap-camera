import 'dart:async';
import 'dart:developer';
import 'dart:typed_data';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

part 'ap_camera_controller.dart';

part 'ap_camera_info.dart';

part 'ap_camera_file.dart';

part 'ap_camera_event.dart';

part 'ap_camera_error.dart';

class ApCameraPlugin {
  /// 原生通信通道
  static const MethodChannel _channel =
      const MethodChannel('com.aplus.plugin.camera');

  /// 设置原生事件监听
  static setNativeMethodCallback(Function(ApCameraEvent event)? callback) {
    _channel.setMethodCallHandler((call) async{
      callback
          ?.call(ApCameraEvent(method: call.method, arguments: call.arguments));
      return;
    });
  }

  /// ------------ 摄像头设置 ------------

  /// 获取摄像头列表
  ///
  /// eg: ['front, 'back']
  static Future<List<String>> availableCameras() async {
    final List? resultData = await _channel.invokeMethod('availableCameras');
    List<String> positions = [];
    for (dynamic value in resultData ?? []) {
      if (value is String) {
        positions.add(value);
      }
    }
    return positions;
  }

  /// 设置当前摄像头
  ///
  /// 创建指定位置，指定分辨率的摄像头
  static Future<Map> createCamera({required String position,required String resolution}) async {
    final Map result = await _channel.invokeMethod(
        'createCamera', {'position': position, 'resolution': resolution});
    return result;
  }

  /// 释放摄像头
  static Future<void> releaseCamera() async {
    await _channel.invokeMethod('releaseCamera');
  }

  /// ------------- 缓存设置 --------------

  /// 清理缓存
  static Future<void> cleanAllCache() async {
    await _channel.invokeMethod('cleanAllCache');
  }

  /// ------------------ 视频、图片处理 -------------------

  /// 开始获取视频流
  static Future<void> startCamera() async {
    await _channel.invokeMethod('startCamera');
  }

  /// 恢复获取视频流
  static Future<void> resumeCamera() async {
    await _channel.invokeMethod('resumeCamera');
  }

  /// 暂停获取视频流
  static Future<void> pauseCamera() async {
    await _channel.invokeMethod('pauseCamera');
  }

  /// 开始录像
  ///
  /// 如果开启成功，返回原生返回1，否则返回0
  static Future<bool> startRecord() async {
    int result = await _channel.invokeMethod('startRecord');
    return result == 1;
  }

  /// 停止录像
  ///
  /// 如果录制成功，返回文件本地路径
  /// 如果录制失败，返回null
  static Future<String> stopRecord() async {
    String filepath = await _channel.invokeMethod('stopRecord');
    return filepath;
  }

  /// 取消录像
  static Future<void> cancelRecord() async {
    await _channel.invokeMethod('cancelRecord');
  }

  /// 获取视频文件列表
  static Future<List<Map>> getVideoFiles() async {
    List<Map> list = [];
    List? resultData = await _channel.invokeMethod('getVideoFiles');
    for (dynamic value in resultData ?? []) {
      if (value is Map) {
        list.add(value);
      }
    }
    return list;
  }

  /// 拍照
  ///
  /// 如果拍摄成功，返回文件本地路径
  /// 如果拍摄失败，返回null
  static Future<String> takePhoto() async {
    String filepath = await _channel.invokeMethod('takePhoto');
    return filepath;
  }

  /// 获取图片文件列表
  static Future<List<Map>> getPhotoFiles() async {
    List<Map> list = [];
    List? resultData = await _channel.invokeMethod('getPhotoFiles');
    for (dynamic value in resultData ?? []) {
      if (value is Map) {
        list.add(value);
      }
    }
    return list;
  }

  /// ------------- 滤镜设置 --------------

  /// 开关 - 美颜滤镜
  static Future<void> setBeautifyFilter(bool enable) async {
    int enableValue = enable == true ? 1 : 0;
    await _channel.invokeMethod('setBeautifyFilter', {'enable': enableValue});
  }

  /// ------------- 贴纸设置 --------------

  /// 获取所有Emoji贴纸
  ///
  /// 返回对应的 emoji类型数组
  static Future<List> getAllEmojiStickers() async {
    return await _channel.invokeMethod('getAllEmojiStickers');
  }

  /// 设置要使用的表情贴纸
  static Future<void> useEmojis(List<String>? emojis) async {
    await _channel.invokeMethod('useEmojis', {'emojis': emojis ?? []});
  }

  /// 开关 - 展示贴纸表情
  static Future<void> showEmoji(bool show) async {
    int showValue = show == true ? 1 : 0;
    await _channel.invokeMethod('showEmoji', {'enable': showValue});
  }

  /// 开始轮播 Emoji Gif
  static Future<void> startEmojiGif() async {
    await _channel.invokeMethod('startEmojiGif');
  }

  /// 停止轮播 Emoji Gif
  static Future<void> stopEmojiGif() async {
    await _channel.invokeMethod('stopEmojiGif');
  }

  /// 启动 Emoji 动画1 （波浪式循环缩放）
  static Future<void> startEmojiAnimation1() async {
    await _channel.invokeMethod('startEmojiAnimation1');
  }

  /// 关闭 Emoji 动画1
  static Future<void> stopEmojiAnimation1() async {
    await _channel.invokeMethod('stopEmojiAnimation1');
  }

  /// 启动 Emoji 动画2 （依次缩放+播放Gif）
  /// 播放完毕后自动停止
  static Future<void> startEmojiAnimation2() async {
    await _channel.invokeMethod('startEmojiAnimation2');
  }

  /// ------------- AI识别相关 --------------

  /// 通过图片识别人脸，返回人脸个数
  static Future<int> detectFaceNumWithImageFile(String filepath) async {
    dynamic value = await _channel.invokeMethod('detectFaceNumWithImageFile', {'file': filepath});
    return value ?? 0;
  }

  /// 开关 - 人脸识别
  static Future<void> setFaceDetect(bool enable) async {
    int enableValue = enable == true ? 1 : 0;
    await _channel.invokeMethod('setFaceDetect', {'enable': enableValue});
  }

  /// 开启人脸抓拍
  static Future<void> startFaceDetectTakePhoto({bool savePhoto=true}) async {
    int value = savePhoto == true ? 1 : 0;
    await _channel.invokeMethod('startFaceDetectTakePhoto', {'savePhoto': value});
  }
  
  /// 开启、关闭人脸抓拍 - 二进制图片数据传输
  static Future<void> setFaceDetectPhotoDataEnable(bool enable) async {
    int value = enable == true ? 1 : 0;
    await _channel.invokeMethod('setFaceDetectPhotoDataEnable', {'enable': value});
  }

  /// ------------- 玩法 --------------

  /// 开启认证
  static Future<void> startVerify(bool lowRecorder) async {
    await _channel
        .invokeMethod('startVerify', {'lowRecorder': lowRecorder ? 1 : 0});
  }

  /// 停止认证
  static Future<void> stopVerify() async {
    await _channel.invokeMethod('stopVerify');
  }

  /// 开始进行视频+BGM合成
  static Future<void> startVideoBGMCompose({required String? videoFile}) async {
    await _channel
        .invokeMethod('startVideoBGMCompose', {'videoFile': videoFile ?? ''});
  }
}

/// iOS 滤镜参数调试
extension IOSFilter on ApCameraPlugin {
  /// 设置美颜滤镜参数
  Future<void> iosAdjustBeautyFilter(
      {required double? bright,required double? beauty,required double? tone}) async {
    await ApCameraPlugin._channel.invokeMethod('setBeautifyFilterParams',
        {'bright': bright ?? 0, 'beauty': beauty ?? 0, 'tone': tone ?? 0});
  }
}

/// OpenGL测试
extension OpenGL on ApCameraPlugin {
  /// 三角形旋转动画
  Future<int> testOpenGLTriangle() async {
    final int textureId =
        await ApCameraPlugin._channel.invokeMethod('testOpenGLTriangle');
    return textureId;
  }
}
