
part of 'ap_camera_plugin.dart';

enum ApCameraPosition {
  /// 前置摄像头
  Front,
  /// 后置摄像头
  Back,
  /// 外置摄像头
  External,
}

/// 相机分辨率
enum ApResolution {
  /// 352x288 on iOS, 240p (320x240) on Android
  low,
  
  /// 480p (640x480 on iOS, 720x480 on Android)
  medium,
  
  /// 720p (1280x720)
  high,
  
  /// 1080p (1920x1080)
  veryHigh,
  
  /// 2160p (3840x2160)
  ultraHigh,
  
  /// The highest resolution available.
  max,
}

/// 摄像头显示相关信息
class ApCameraInfo {
  
  /// 位置
  late final ApCameraPosition position;
  
  /// 分辨率
  late final ApResolution resolution;
  
  /// 是否已经初始化设置完毕
  bool isInitialized;
  
  /// 是否正在截图中
  bool isTakingPicture;
  
  /// 是否正在录制中
  bool isRecordingVideo;
  
  /// 视频的理想分辨率大小
  Size previewSize;
  
  /// 显示比例
  double get aspectRatio => previewSize.width / previewSize.height;
  
  ApCameraInfo._init({
    required this.position,
    required this.resolution,
    required this.isInitialized,
    required this.isTakingPicture,
    required this.isRecordingVideo,
    required this.previewSize
  });

  /// 初始化默认信息
  ApCameraInfo.newCamera({
   required ApCameraPosition position,
    required  ApResolution resolution
  }) : this._init(
    position: position,
    resolution: resolution,
    isInitialized: false,
    isTakingPicture: false,
    isRecordingVideo: false,
    previewSize: Size(1, 1),
  );
}