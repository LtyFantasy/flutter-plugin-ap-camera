part of 'ap_camera_plugin.dart';

/// 摄像头相关文件
///
/// 图片 or 视频
class ApCameraFile {
  /// 视频文件 - true ，图片文件 - false
  final bool isVideo;

  /// 文件名
  ///
  /// eg: test1.mp4
  final String filename;

  /// 文件全路径
  ///
  /// eg: /tmp/test1.mp4
  final String fullPath;

  /// 大小
  final int size;

  /// 创建时间 ms
  final DateTime createAt;

  ApCameraFile({
    required this.isVideo,
    required this.filename,
    required this.fullPath,
    required this.size,
    required this.createAt,
  });

  /// 原生Map转Flutter模型
  static ApCameraFile fromMap(Map map) {

    bool isVideo = map['isVideo'] ?? false;
    String filename = map['filename'] ?? '';
    String fullPath = map['fullPath'] ?? '';
    int size = map['size'] ?? 0;
    int createAtValue = map['createAt'] ?? 0;
    DateTime createAt = DateTime.fromMillisecondsSinceEpoch(createAtValue);

    return ApCameraFile(
        isVideo: isVideo,
        filename: filename,
        fullPath: fullPath,
        size: size,
        createAt: createAt);
  }
}
