package com.example.android.camera2basic.watermark

import android.content.Context
import android.graphics.Bitmap
import android.graphics.RectF
import android.opengl.GLES20
import android.opengl.Matrix
import android.util.Log
import com.example.ap_camera.egl.EGLUtil
import com.example.ap_camera.egl.EGLUtil.loadTexture
import com.example.ap_camera.filter.VertexArray
import com.example.ap_camera.gif.GifDecoder
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList

class EmojiRender(context: Context) {

    companion object {
        private const val TAG = "WaterMarkRender"

        private const val VERTEX_COMPONENT_COUNT = 2
        private const val COORDINATE_COMPONENT_COUNT = 2
        private const val STRIDE =
                16
    }

    private var vertexData = floatArrayOf(
            // x, y, s, t
            -1f, -1f, 0f, 1f,
            1f, -1f, 1f, 1f,
            -1f, 1f, 0f, 0f,
            1f, 1f, 1f, 0f
    )

    private val vertexArray = VertexArray(vertexData)
    private val emojiShaderProgram = EmojiShaderProgram(context)
    var textureIds: HashMap<Int, Int>? = HashMap()
    var bitmapMap: HashMap<String, List<Bitmap>>? = HashMap()
    var frame: Int = 0

    private var mvpMatrix = Array(16) { 0.0f }.toFloatArray()
    private var modelMatrix = Array(16) { 0.0f }.toFloatArray()
    private var viewMatrix = Array(16) { 0.0f }.toFloatArray()
    private var projectionMatrix = Array(16) { 0.0f }.toFloatArray()
    private val tmpMatrix = Array(16) { 0.0f }.toFloatArray()

    private var renderWith = -1
    private var renderHeight = -1

    private var gifAnim: Boolean = true

    private var scaleAnim: Boolean = false

    public val scaleTime: Long = 2400L

    private var scaleStartTime: Long = 0L


    //    private var renderRect = RectF(left, top, left + 72.5f, top + 37.5f)
    private lateinit var renderRect: RectF

    private lateinit var frameBuffer: IntArray

    private lateinit var frameBufferTexture: IntArray

    private var name: String = ""

    private var show: Boolean = false

//    private var name: String = "angry.gif"
//
//    private var show: Boolean = true


    private var gifStartTime: Long = 0L

    init {
        setupGif()
    }

    private fun clamp(amount: Float, low: Float, high: Float): Float {
        return if (amount < low) low else if (amount > high) high else amount
    }

    fun setEmojiName(name: String) {
        this.name = name
        setupGif()
    }

    fun setShow(show: Boolean) {
        this.show = show
        if(!show){
            scaleAnim=false
        }
    }


    private fun setupGif() {
        try {
            val `is` = EGLUtil.context.assets.open(name)
            val gifDecoder = GifDecoder()
            val code = gifDecoder.read(`is`)
            if (code == GifDecoder.STATUS_OK) { //解码成功
                val frameList = gifDecoder.frames
                val bitmaps: MutableList<Bitmap> = ArrayList()
                for (i in frameList.indices) {
                    bitmaps.add(frameList[i].image)
                }
                bitmapMap!![name] = bitmaps
            } else if (code == GifDecoder.STATUS_FORMAT_ERROR) { //图片格式不是GIF
            } else { //图片读取失败
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }


    /**
     * @param width: preview width
     * @param height: preview height
     */
    fun onSizeChanged(width: Int, height: Int, top: Float, left: Float, size: Float) {

        renderRect = RectF(left, top, left + size, top + size)

        renderWith = height
        renderHeight = width
        frameBuffer = IntArray(1)
        frameBufferTexture = IntArray(1)

//         create fbo

//         create fbo
        GLES20.glGenFramebuffers(1, frameBuffer, 0)
        GLES20.glGenTextures(1, frameBufferTexture, 0)
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, frameBufferTexture[0])
        GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGBA, renderWith, renderHeight,
                0, GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, null)
        GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR.toFloat())
        GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR.toFloat())
        GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE.toFloat())
        GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE.toFloat())

        GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, frameBuffer[0])
        GLES20.glFramebufferTexture2D(GLES20.GL_FRAMEBUFFER,
                GLES20.GL_COLOR_ATTACHMENT0, GLES20.GL_TEXTURE_2D, frameBufferTexture[0], 0)
        GLES20.glCheckFramebufferStatus(GLES20.GL_FRAMEBUFFER)

        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0)
        GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0)

        Matrix.setIdentityM(mvpMatrix, 0)
        val xStep = 2f / renderWith
        val yStep = 2f / renderHeight
        val rTop = xStep * renderRect.top
        val rLeft = yStep * renderRect.left
        val h = clamp(xStep * renderRect.height(), 0f, 1f)
        val w = clamp(yStep * renderRect.width(), 0f, 1f)


        vertexData = floatArrayOf(
                // x, y, s, t
                -1f + rLeft, 1f - rTop - h, 0f, 1f,
                -1f + rLeft + w, 1f - rTop - h, 1f, 1f,
                -1f + rLeft, 1f - rTop, 0f, 0f,
                -1f + rLeft + w, 1f - rTop, 1f, 0f
        )
        vertexArray.updateBuffer(vertexData, 0, vertexData.size)


    }

    ///Gif动画
    fun startOrStopGif(start: Boolean) {
        gifAnim = start
        if (!start) {
            frame = 0
        }
    }

    ///放大缩小动画
    fun startOrStopScale(start: Boolean) {
        scaleAnim = start
        if (start) {
            scaleStartTime = 0
        }
    }
    //
    fun drawSelf(id: Int): Int {
        if (name == "" || !show) {
            return id
        }
        if(textureIds?.size==0){
            //先把纹理加载到缓存，避免录制视频表情有闪烁
            for (i in 0 until bitmapMap?.get(name)?.size!!) {
                var textureId= loadTexture( bitmapMap?.get(name)?.get(i))
                textureIds?.put(i, textureId)
            }
            Log.d(TAG, "drawSelf: 完成 "+ textureIds!!.size)
        }


        // blend
        GLES20.glEnable(GLES20.GL_BLEND)
        GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA)
        GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, frameBuffer[0])
        emojiShaderProgram.useProgram()


        GLES20.glActiveTexture(GLES20.GL_TEXTURE1)
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, id)
        GLES20.glFramebufferTexture2D(GLES20.GL_FRAMEBUFFER,
                GLES20.GL_COLOR_ATTACHMENT0, GLES20.GL_TEXTURE_2D, id, 0)

        var textureId = textureIds?.get(frame)
        if (textureId == null) {
            textureId = loadTexture(bitmapMap?.get(name)?.get(frame))
            textureIds?.put(frame, textureId)
        }
        emojiShaderProgram.setUniform(mvpMatrix, textureId)
        val positionLoc = emojiShaderProgram.getPositionAttributeLoc()
        val coordinateLoc = emojiShaderProgram.getTextureCoordinateAttributeLoc()

        val xStep = 2f / renderWith
        val yStep = 2f / renderHeight
        val top = xStep * renderRect.top
        val left = yStep * renderRect.left
        val h = clamp(xStep * renderRect.height(), 0f, 1f)
        val w = clamp(yStep * renderRect.width(), 0f, 1f)

        if (scaleAnim) {
            val maxW = w * 1.4f
            val maxH = h * 1.4f
            val now = System.currentTimeMillis()
            if (scaleStartTime == 0L) {
                scaleStartTime = now
            }
            var offsetTime = now - scaleStartTime
            if (offsetTime > scaleTime || offsetTime < 0L) {
                scaleStartTime = 0L
                scaleAnim = false
                vertexData = floatArrayOf(
                        // x, y, s, t
                        -1f + left, 1f - top - h, 0f, 1f,
                        -1f + left + w, 1f - top - h, 1f, 1f,
                        -1f + left, 1f - top, 0f, 0f,
                        -1f + left + w, 1f - top, 1f, 0f
                )
            } else {

                if (offsetTime <= 200L) {
                    gifAnim=false
                    val offsetW = (maxW - w) / (200L) * offsetTime
                    val offsetH = (maxH - h) / (200L) * offsetTime
                    vertexData = floatArrayOf(
                            // x, y, s, t
                            -1f + left - offsetW, 1f - top - h - offsetH, 0f, 1f,
                            -1f + left + w + offsetW, 1f - top - h - offsetH, 1f, 1f,
                            -1f + left - offsetW, 1f - top + offsetH, 0f, 0f,
                            -1f + left + w + offsetW, 1f - top + offsetH, 1f, 0f
                    )
                } else if (offsetTime >= 2200L) {
                    gifAnim=false
                    val tempTime = scaleTime - offsetTime
                    val offsetW = (maxW - w) / (200L) * tempTime
                    val offsetH = (maxH - h) / (200L) * tempTime
                    vertexData = floatArrayOf(
                            // x, y, s, t
                            -1f + left - offsetW, 1f - top - h - offsetH, 0f, 1f,
                            -1f + left + w + offsetW, 1f - top - h - offsetH, 1f, 1f,
                            -1f + left - offsetW, 1f - top + offsetH, 0f, 0f,
                            -1f + left + w + offsetW, 1f - top + offsetH, 1f, 0f
                    )
                }else{
                    gifAnim=true
                    val offsetW = (maxW - w)
                    val offsetH = (maxH - h)
                    vertexData = floatArrayOf(
                            // x, y, s, t
                            -1f + left - offsetW, 1f - top - h - offsetH, 0f, 1f,
                            -1f + left + w + offsetW, 1f - top - h - offsetH, 1f, 1f,
                            -1f + left - offsetW, 1f - top + offsetH, 0f, 0f,
                            -1f + left + w + offsetW, 1f - top + offsetH, 1f, 0f
                    )
                }
            }
        } else {
            vertexData = floatArrayOf(
                    // x, y, s, t
                    -1f + left, 1f - top - h, 0f, 1f,
                    -1f + left + w, 1f - top - h, 1f, 1f,
                    -1f + left, 1f - top, 0f, 0f,
                    -1f + left + w, 1f - top, 1f, 0f
            )
        }


        vertexArray.updateBuffer(vertexData, 0, vertexData.size)

        vertexArray.setVertexAttributePointer(
                0, positionLoc, VERTEX_COMPONENT_COUNT, STRIDE
        )
        vertexArray.setVertexAttributePointer(
                2, coordinateLoc, COORDINATE_COMPONENT_COUNT, STRIDE
        )

        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 4)

        GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0)
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0)
        GLES20.glDisableVertexAttribArray(positionLoc)
        GLES20.glDisableVertexAttribArray(coordinateLoc)
        GLES20.glDisable(GLES20.GL_BLEND)
        if (gifAnim) {

            frame= ((System.currentTimeMillis()-gifStartTime)/2000.0f*60).toInt()
            if (frame >= 60) {
                frame = 0
                gifStartTime=System.currentTimeMillis();
            }
        }
        return id
    }


}