package com.example.ap_camera.filter

import android.content.Context
import android.opengl.GLES20
import com.example.ap_camera.egl.EGLUtil

open class ShaderProgram(context: Context,
                         vertexShaderResId: Int,
                         fragmentShaderResId: Int) {

    var programId = -1
    init {
        // Compile the shader and link the program
        programId = ShaderHelper.buildProgram(
                EGLUtil.loadShaderSource(vertexShaderResId),
                EGLUtil.loadShaderSource( fragmentShaderResId)
        )
    }

    fun useProgram() {
        // Set the current OpenGL shader program to this program
        GLES20.glUseProgram(programId)
    }
}