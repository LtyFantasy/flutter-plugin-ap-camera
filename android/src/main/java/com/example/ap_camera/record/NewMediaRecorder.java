package com.example.ap_camera.record;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaCodecList;
import android.media.MediaFormat;
import android.media.MediaMuxer;
import android.opengl.EGLContext;
import android.opengl.GLES20;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.util.Log;
import android.view.Surface;

import com.example.ap_camera.egl.EGLUtil;
import com.hw.videoprocessor.VideoProcessor;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import androidx.annotation.RequiresApi;

import static android.media.MediaCodecList.REGULAR_CODECS;

public class NewMediaRecorder {
    private static final String TAG = "NewMediaRecorder";

    private final int mWidth;
    private final int mHeight;
    private final String mPath;
    private final Context mContext;

    private MediaCodec mMediaCodec;
    private Surface mSurface;
    private EGLContext mGlContext;
    private MediaMuxer mMuxer;
    private Handler mHandler;
    private boolean isStart;
    private int track;
    private float mSpeed;
    private long mLastTimeStamp;
    private RecorderEGLEnv eglEnv;
    List<MediaCodec.BufferInfo> cacheInfo;
    ///针对Android低端机型，使用低帧率模式
    ///
    private boolean mAndroidLowRecorder = false;

    public NewMediaRecorder(Context context, String path, EGLContext glContext, int width, int
            height, boolean androidLowRecorder) {
        mContext = context.getApplicationContext();
        mPath = path;
        mWidth = width;
        mHeight = height;
        mGlContext = glContext;
        cacheInfo = new ArrayList<>();
        mAndroidLowRecorder = androidLowRecorder;
    }


    public void start() throws IOException {
        mSpeed = 1.0f;
        MediaFormat format = MediaFormat.createVideoFormat(MediaFormat.MIMETYPE_VIDEO_AVC,
                mWidth, mHeight);
        //颜色空间 从 surface当中获得
        format.setInteger(MediaFormat.KEY_COLOR_FORMAT, MediaCodecInfo.CodecCapabilities
                .COLOR_FormatSurface);
        //码率
        if (mAndroidLowRecorder) {
            format.setInteger(MediaFormat.KEY_BIT_RATE, mWidth * mHeight * 4);
        } else {
            format.setInteger(MediaFormat.KEY_BIT_RATE, mWidth * mHeight * 3);
        }


        //帧率
        if (mAndroidLowRecorder) {
            format.setInteger(MediaFormat.KEY_FRAME_RATE, 10);
        } else {
            format.setInteger(MediaFormat.KEY_FRAME_RATE, 30);
        }

        //关键帧间隔
        format.setInteger(MediaFormat.KEY_I_FRAME_INTERVAL, 30);

//        MediaCodecList list = new MediaCodecList(REGULAR_CODECS);
//        String encoderForFormat = list.findEncoderForFormat(format);
//        Log.d(TAG, "startaaaaaa: " + encoderForFormat);
//        MediaCodecInfo[] infos = list.getCodecInfos();
//        for (int i = 0; i < infos.length; i++) {
//            MediaCodecInfo info = infos[i];
//            if (info.isEncoder()) {
//                Log.d(TAG, "start:dddd " + info.getName());
//            }
//        }
        //创建编码器
        mMediaCodec = MediaCodec.createEncoderByType(MediaFormat.MIMETYPE_VIDEO_AVC);
//        mMediaCodec = MediaCodec.createByCodecName(encoderForFormat);
//        mMediaCodec = MediaCodec.createByCodecName("OMX.google.h264.encoder");


        //配置编码器
        mMediaCodec.configure(format, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);
        //这个surface显示的内容就是要编码的画面
        mSurface = mMediaCodec.createInputSurface();


        //創建OpenGL 的 環境
        HandlerThread handlerThread = new HandlerThread("codec-gl");
        handlerThread.start();
        mHandler = new Handler(handlerThread.getLooper());
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                // 创建EGL环境
                eglEnv = new RecorderEGLEnv(mContext, mGlContext, mSurface, mWidth, mHeight);
                isStart = true;
            }
        });
        //混合器 (复用器) 将编码的h.264封装为mp4
        mMuxer = new MediaMuxer(mPath,
                MediaMuxer.OutputFormat.MUXER_OUTPUT_MPEG_4);
        mMediaCodec.start();

    }

    boolean drawing = false;

    public void fireFrame(final int textureId, final long timestamp) {
        if (!isStart) {
            return;
        }
        if (drawing) {
            return;
        }
        drawing = true;


        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                drawing = false;
            }
        }, mAndroidLowRecorder ? 1000 / 10 : 1000 / 30);
        //录制用的opengl已经和handler的线程绑定了 ，所以需要在这个线程中使用录制的opengl
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (eglEnv == null)
                    return;

//                if(!drawing){
//                    drawing=true;
                //画画
                eglEnv.draw(textureId, timestamp);
                codec(false);
//                    drawing=false;
//                }

            }
        });


    }

//    boolean codeStart = false;
//    Timer timer;
//    boolean mEndOfStream = false;
//
//    boolean stopCode = false;

//    private void codec(boolean endOfStream) {
//        //给个结束信号
//        if (endOfStream) {
//            mMediaCodec.signalEndOfInputStream();
//        }
////        Log.d(TAG, "encoderStatus : start");
//        //获得输出缓冲区 (编码后的数据从输出缓冲区获得)
//        MediaCodec.BufferInfo bufferInfo = new MediaCodec.BufferInfo();
//        if (!stopCode) {
//            cacheInfo.add(bufferInfo);
//        }
//        if (!codeStart) {
//            codeStart = true;
//            timer = new Timer();
//            TimerTask timerTask = new TimerTask() {
//                @Override
//                public void run() {
//                    if (cacheInfo.size() == 0) {
//                        timer.cancel();
//                        codeStop();
//                        return;
//                    }
//                    MediaCodec.BufferInfo buffer = cacheInfo.remove(0);
//
//                    while (true) {
//                        int encoderStatus = mMediaCodec.dequeueOutputBuffer(buffer, 10000);
//                        //需要更多数据
//                        if (encoderStatus == MediaCodec.INFO_TRY_AGAIN_LATER) {
//                            //如果是结束那直接退出，否则继续循环
//                            if (!mEndOfStream) {
//                                break;
//                            }
//                        } else if (encoderStatus == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
//                            //输出格式发生改变  第一次总会调用所以在这里开启混合器
//                            MediaFormat newFormat = mMediaCodec.getOutputFormat();
//                            track = mMuxer.addTrack(newFormat);
//                            mMuxer.start();
//                        } else if (encoderStatus == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {
//                            //可以忽略
//                        } else {
//                            //调整时间戳
//                            buffer.presentationTimeUs = (long) (buffer.presentationTimeUs / mSpeed);
////                Log.d(TAG, " bufferInfo.presentationTimeUs  " + bufferInfo.presentationTimeUs);
//                            //有时候会出现异常 ： timestampUs xxx < lastTimestampUs yyy for Video track
////                if (bufferInfo.presentationTimeUs <= mLastTimeStamp) {
////                    Log.d(TAG, " timestampUs xxx < lastTimestampUs yyy ");
////                    bufferInfo.presentationTimeUs = (long) (mLastTimeStamp + 1_000_000 / 25 / mSpeed);
////                }
////                mLastTimeStamp = bufferInfo.presentationTimeUs;
//
//                            //正常则 encoderStatus 获得缓冲区下标
//                            ByteBuffer encodedData = mMediaCodec.getOutputBuffer(encoderStatus);
//                            //如果当前的buffer是配置信息，不管它 不用写出去
//                            if ((buffer.flags & MediaCodec.BUFFER_FLAG_CODEC_CONFIG) != 0) {
//                                buffer.size = 0;
//                            }
//                            if (buffer.size != 0) {
//                                //设置从哪里开始读数据(读出来就是编码后的数据)
//                                encodedData.position(buffer.offset);
//                                //设置能读数据的总长度
//                                encodedData.limit(buffer.offset + buffer.size);
//                                //写出为mp4
//                                mMuxer.writeSampleData(track, encodedData, buffer);
//                            }
//                            // 释放这个缓冲区，后续可以存放新的编码后的数据啦
//                            mMediaCodec.releaseOutputBuffer(encoderStatus, false);
//                            // 如果给了结束信号 signalEndOfInputStream
//                            if ((buffer.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0) {
//                                break;
//                            }
//                        }
//                    }
//
//                }
//            };
//            timer.schedule(timerTask, 0, 100);
//        }
//
//
//    }

//    public void codeStop() {
//        // 释放
//        isStart = false;
//        mEndOfStream = true;
//        Log.d(TAG, "文件地址   " + mPath);
//        Log.d(TAG, "codeStop: "+System.currentTimeMillis());
//        mHandler.post(new Runnable() {
//            @Override
//            public void run() {
//                codec(true);
//                mMediaCodec.stop();
//                mMediaCodec.release();
//                mMediaCodec = null;
//                mMuxer.stop();
//                mMuxer.release();
//                mMuxer = null;
//                eglEnv.release();
//                eglEnv = null;
//                mSurface = null;
//                mHandler.getLooper().quitSafely();
//                mHandler = null;
//            }
//        });
//
//    }

    private void codec(boolean endOfStream) {
        //给个结束信号
        if (endOfStream) {
            mMediaCodec.signalEndOfInputStream();
        }
//        Log.d(TAG, "encoderStatus : start");
        //获得输出缓冲区 (编码后的数据从输出缓冲区获得)
        MediaCodec.BufferInfo buffer = new MediaCodec.BufferInfo();

        while (true) {
            int encoderStatus = mMediaCodec.dequeueOutputBuffer(buffer, 0);
            //需要更多数据
            if (encoderStatus == MediaCodec.INFO_TRY_AGAIN_LATER) {
                //如果是结束那直接退出，否则继续循环
                if (!endOfStream) {
                    break;
                }
            } else if (encoderStatus == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
                //输出格式发生改变  第一次总会调用所以在这里开启混合器
                MediaFormat newFormat = mMediaCodec.getOutputFormat();
                track = mMuxer.addTrack(newFormat);
                mMuxer.start();
            } else if (encoderStatus == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {
                //可以忽略
            } else {
                //调整时间戳
                buffer.presentationTimeUs = (long) (buffer.presentationTimeUs / mSpeed);

//                Log.d(TAG, " bufferInfo.presentationTimeUs  " + bufferInfo.presentationTimeUs);
                //有时候会出现异常 ： timestampUs xxx < lastTimestampUs yyy for Video track
//                if (bufferInfo.presentationTimeUs <= mLastTimeStamp) {
//                    Log.d(TAG, " timestampUs xxx < lastTimestampUs yyy ");
//                    bufferInfo.presentationTimeUs = (long) (mLastTimeStamp + 1_000_000 / 25 / mSpeed);
//                }
//                mLastTimeStamp = bufferInfo.presentationTimeUs;

                //正常则 encoderStatus 获得缓冲区下标
                ByteBuffer encodedData = mMediaCodec.getOutputBuffer(encoderStatus);
                //如果当前的buffer是配置信息，不管它 不用写出去
                if ((buffer.flags & MediaCodec.BUFFER_FLAG_CODEC_CONFIG) != 0) {
                    buffer.size = 0;
                }
                if (buffer.size != 0) {
                    //设置从哪里开始读数据(读出来就是编码后的数据)
                    encodedData.position(buffer.offset);
                    //设置能读数据的总长度
                    encodedData.limit(buffer.offset + buffer.size);
                    //写出为mp4
                    mMuxer.writeSampleData(track, encodedData, buffer);
                }
                // 释放这个缓冲区，后续可以存放新的编码后的数据啦
                mMediaCodec.releaseOutputBuffer(encoderStatus, false);
                // 如果给了结束信号 signalEndOfInputStream
                if ((buffer.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0) {
                    break;
                }
            }
        }
    }

    public void stop() {
//        stopCode=true;
//        // 释放
        isStart = false;
        Log.d(TAG, "文件地址   " + mAndroidLowRecorder + "   " + mPath);
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                codec(true);
                mMediaCodec.stop();
                mMediaCodec.release();
                mMediaCodec = null;
                mMuxer.stop();
                mMuxer.release();
                mMuxer = null;
                eglEnv.release();
                eglEnv = null;
                mSurface = null;
                mHandler.getLooper().quitSafely();
                mHandler = null;
            }
        });

    }


}
