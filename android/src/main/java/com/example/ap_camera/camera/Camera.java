package com.example.ap_camera.camera;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureFailure;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.OutputConfiguration;
import android.hardware.camera2.params.SessionConfiguration;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.CamcorderProfile;
import android.media.Image;
import android.media.ImageReader;
import android.opengl.EGL14;
import android.opengl.GLSurfaceView;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.util.Log;
import android.util.Size;
import android.view.Surface;

import com.example.ap_camera.ApCameraPlugin;
import com.example.ap_camera.egl.EGLTextureRender;
import com.example.ap_camera.egl.EGLUtil;
import com.example.ap_camera.gif.GifDecoder;
import com.example.ap_camera.record.MediaRecorder;
import com.example.ap_camera.record.NewMediaRecorder;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.mlkit.vision.common.InputImage;
import com.google.mlkit.vision.face.Face;
import com.google.mlkit.vision.face.FaceDetection;
import com.google.mlkit.vision.face.FaceDetector;
import com.google.mlkit.vision.face.FaceDetectorOptions;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import io.flutter.embedding.engine.systemchannels.PlatformChannel;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.view.TextureRegistry;

/**
 * Macx
 * 2021/4/29
 *
 * @Description:
 */
public class Camera {

    public static int Camera_Height = 1280;
    public static int Camera_Width = 720;
    ///每次人脸时间间隔（ms）
    final static int Detector_Time = 500;
    private static final String TAG = "Camera";
    private final CameraManager cameraManager;
    private String cameraId;
    private EGLTextureRender render;
    private Size previewSize;
    private CameraDevice cameraDevice;
    private Context context;
    private Activity activity;
    //    private MediaRecorder mediaRecorder;
    private NewMediaRecorder newMediaRecorder;
    private CameraCaptureSession cameraCaptureSession;
    ///用户截图和人脸检测
    private ImageReader imageStreamReader;
    ///人脸识别
    FaceDetector detector;
    ///用于人脸检测时间间隔
    long detectorTime = 0;
    private ScopedExecutor executor;
    boolean saveFirstPersonPhoto = false;
    boolean saveSmilePersonPhoto = false;
    ///录像文件
    private File videoRecordingFile;
    ///正在录制中
    private boolean recordingVideo = false;

    private FaceDetectCallback faceDetectCallback;

    int firstPhotoFaceFrame = 0;

    public EGLTextureRender recordRender;

    boolean beautifyFilter = false;

    HandlerThread handlerThread = new HandlerThread("detector");

    Handler handler;
    YuvToRgbConverter yuvToRgbConverter;

    public Camera(Context context, Activity activity, EGLTextureRender render, FaceDetectCallback faceDetectCallback) {

        this.faceDetectCallback = faceDetectCallback;
        this.context = context;
        yuvToRgbConverter = new YuvToRgbConverter(context);
        this.activity = activity;
        this.cameraManager = (CameraManager) context.getSystemService(Context.CAMERA_SERVICE);
        this.render = render;
//        deviceOrientationListener =
//                new DeviceOrientationManager(context, dartMessenger, isFrontFacing, sensorOrientation);
//        deviceOrientationListener.start();
        setupMLKit();
        executor = new ScopedExecutor(TaskExecutors.MAIN_THREAD);
        handlerThread.start();
        handler = new Handler(handlerThread.getLooper());
    }

    public void setRecordRender(EGLTextureRender recordRender) {
        this.recordRender = recordRender;
    }

    ///初始化人脸识别模块
    public void setupMLKit() {
        // High-accuracy landmark detection and face classification
//        FirebaseVisionFaceDetectorOptions highAccuracyOpts =
//                new FirebaseVisionFaceDetectorOptions.Builder()
//                        .setPerformanceMode(FirebaseVisionFaceDetectorOptions.ACCURATE)
//                        .setLandmarkMode(FirebaseVisionFaceDetectorOptions.ALL_LANDMARKS)
//                        .setClassificationMode(FirebaseVisionFaceDetectorOptions.ALL_CLASSIFICATIONS)
//                        .setMinFaceSize(1.0f)
//                        .build();

        FaceDetectorOptions highAccuracyOpts =
                new FaceDetectorOptions.Builder()
                        .setPerformanceMode(FaceDetectorOptions.PERFORMANCE_MODE_ACCURATE)
                        .setLandmarkMode(FaceDetectorOptions.LANDMARK_MODE_ALL)
                        .setClassificationMode(FaceDetectorOptions.CLASSIFICATION_MODE_ALL)
                        .setMinFaceSize(1.0f)
                        .build();
        detector = FaceDetection.getClient(highAccuracyOpts);
    }


    ///初始化相机
    public void setupCamera() {
        configCamera();
        openCamera();
    }


    ///配置相机
    ///预览模式，摄像头id
    public void configCamera() {
        try {
            for (String cid : cameraManager.getCameraIdList()) {
                //获取相机配置
                CameraCharacteristics characteristics = cameraManager.getCameraCharacteristics(cid);
                //使用前置相机
                int facing = characteristics.get(CameraCharacteristics.LENS_FACING);//获取相机朝向
                if (facing != CameraCharacteristics.LENS_FACING_FRONT) {
                    continue;
                }
                cameraId = cid;
                Log.d(TAG, "configCamera: " + cameraId);
                //获取相机输出格式/尺寸参数
                StreamConfigurationMap configs = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
                Size[] sizes = configs.getOutputSizes(ImageFormat.JPEG);
                for (int i = 0; i < sizes.length; i++) {
                    Log.d(TAG, "sizes: getWidth " + sizes[i].getWidth() + "   getHeight" + sizes[i].getHeight());
                }
                //设定最佳预览尺寸
                previewSize = setOptimalPreviewSize();
                imageStreamReader =
                        ImageReader.newInstance(previewSize.getWidth(), previewSize.getHeight(), ImageFormat.YUV_420_888, 2);
                imageStreamReader.setOnImageAvailableListener(
                        new ImageReader.OnImageAvailableListener() {
                            @Override
                            public void onImageAvailable(ImageReader imageReader) {
                                try {
                                    long now = System.currentTimeMillis();
                                    final Image img = imageReader.acquireNextImage();
                                    if (detectorTime == 0 || now - detectorTime > Detector_Time) {
                                        detectorTime = now;
                                        try {
//                                        final FirebaseVisionImage image = FirebaseVisionImage.fromMediaImage(img, CameraUtils.getRotationCompensation(cameraId, activity, context));
                                            final InputImage inputImage = InputImage.fromMediaImage(img, CameraUtils.getRotationCompensation(cameraId, activity, true));
                                            Task<List<Face>> result =
                                                    detector.process(inputImage)
                                                            .addOnSuccessListener(
                                                                    new OnSuccessListener<List<Face>>() {
                                                                        @Override
                                                                        public void onSuccess(List<Face> faces) {
                                                                            try {
                                                                                // Task completed successfully
                                                                                // ...
                                                                                if (faceDetectCallback == null) {
                                                                                    return;
                                                                                }

                                                                                Log.d(TAG, "onSuccess: " + faces.size());
                                                                                Bitmap bitmap = Bitmap.createBitmap(img.getWidth(), img.getHeight(), Bitmap.Config.ARGB_8888);

                                                                                yuvToRgbConverter.yuvToRgb(img, bitmap);

                                                                                Matrix m = new Matrix();
                                                                                m.setRotate(-90, (float) bitmap.getWidth() / 2, (float) bitmap.getHeight() / 2);

                                                                                bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), m, true);


                                                                                faceDetectCallback.onDetectFace(faces, bitmap);
                                                                                if (faces.size() > 0) {


                                                                                    if (!saveFirstPersonPhoto && firstPhotoFaceFrame > 10) {
                                                                                        saveFirstPersonPhoto = true;
                                                                                        CameraUtils.savePic(bitmap, 0, context);
                                                                                        Log.d(TAG, "saveFirst ok: ");
                                                                                    } else {
                                                                                        firstPhotoFaceFrame++;
                                                                                    }
                                                                                    Face face = faces.get(0);
                                                                                    if (face.getSmilingProbability() != null) {
                                                                                        float smileProb = face.getSmilingProbability();
                                                                                        Log.d(TAG, "smileProb: " + smileProb);
                                                                                        if (smileProb > 0.7 && !saveSmilePersonPhoto) {
                                                                                            saveSmilePersonPhoto = true;
                                                                                            CameraUtils.savePic(bitmap, 1, context);
                                                                                            Log.d(TAG, "saveSmile ok: ");
                                                                                        }
                                                                                    }
                                                                                    if (face.getRightEyeOpenProbability() != null) {
                                                                                        float rightEyeOpenProb = face.getRightEyeOpenProbability();
                                                                                        Log.d(TAG, "rightEyeOpenProb: " + rightEyeOpenProb);
                                                                                    }
                                                                                    if (face.getLeftEyeOpenProbability() != null) {
                                                                                        float leftEyeOpenProb = face.getLeftEyeOpenProbability();
                                                                                        Log.d(TAG, "leftEyeOpenProb: " + leftEyeOpenProb);
                                                                                    }
                                                                                }
                                                                            } catch (Exception e) {
                                                                                e.printStackTrace();
                                                                            }
                                                                        }
                                                                    })
                                                            .addOnFailureListener(
                                                                    new OnFailureListener() {
                                                                        @Override
                                                                        public void onFailure(@NonNull Exception e) {
                                                                            // Task failed with an exception
                                                                            // ...
                                                                            Log.e(TAG, "onFailure: ", e);
                                                                        }
                                                                    })
                                                            .addOnCompleteListener(new OnCompleteListener<List<Face>>() {
                                                                @Override
                                                                public void onComplete(@NonNull Task<List<Face>> task) {
                                                                    img.close();
                                                                }
                                                            });


                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                    } else {
                                        img.close();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        handler);

                //打印最佳预览尺寸
                Log.d(TAG, "最佳预览尺寸（w-h）：" + previewSize.getWidth() + "-" + previewSize.getHeight());

                break;
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    ///打开相机
    public void openCamera() {
        try {

            //打开相机
            cameraManager.openCamera(cameraId,
                    new CameraDevice.StateCallback() {
                        @Override
                        public void onOpened(CameraDevice camera) {
                            cameraDevice = camera;
                            //创建相机预览 session
                            createPreviewSession(CameraDevice.TEMPLATE_PREVIEW);

                            Handler handler = new Handler(Looper.getMainLooper());
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    createPreviewSession(CameraDevice.TEMPLATE_PREVIEW);
                                }
                            }, 300);
                        }

                        @Override
                        public void onDisconnected(CameraDevice camera) {
                            cameraDevice = camera;
                            //释放相机资源
                            close();
                        }

                        @Override
                        public void onError(CameraDevice camera, int error) {
                            cameraDevice = camera;
                            //释放相机资源
                            close();
                        }
                    },
                    null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void createPreviewSession(final int templateType) {
        //根据TextureView 和 选定的 previewSize 创建用于显示预览数据的Surface
        render.setSize(previewSize);//设置SurfaceTexture缓冲区大小

        final Surface previewSurface = new Surface(render.inputSurfaceTexture);

        try {
            //创建预览session
            ArrayList<Surface> list = new ArrayList<>();
            list.add(previewSurface);
            list.add(imageStreamReader.getSurface());
            cameraDevice.createCaptureSession(list,
                    new CameraCaptureSession.StateCallback() {
                        @Override
                        public void onConfigured(CameraCaptureSession session) {
                            try {
                                cameraCaptureSession = session;
                                //构建预览捕获请求
                                CaptureRequest.Builder builder = cameraDevice.createCaptureRequest(templateType);
//                                if (finalRecordSurface != null) {
//                                    builder.addTarget(finalRecordSurface);
//                                }
                                builder.addTarget(previewSurface);//设置 pre
                                builder.addTarget(imageStreamReader.getSurface());// viewSurface 作为预览数据的显示界面
                                CaptureRequest captureRequest = builder.build();
                                //设置重复请求，以获取连续预览数据
                                session.setRepeatingRequest(captureRequest, new CameraCaptureSession.CaptureCallback() {
                                            @Override
                                            public void onCaptureProgressed(CameraCaptureSession session, CaptureRequest request, CaptureResult partialResult) {
                                                super.onCaptureProgressed(session, request, partialResult);

                                            }

                                            @Override
                                            public void onCaptureCompleted(CameraCaptureSession session, CaptureRequest request, TotalCaptureResult result) {
                                                super.onCaptureCompleted(session, request, result);

                                            }
                                        },
                                        null);
                            } catch (CameraAccessException e) {
                                e.printStackTrace();
                            }
//                            if (templateType == CameraDevice.TEMPLATE_RECORD) {
//                                mediaRecorder.start();
//                            }
                            if (templateType == CameraDevice.TEMPLATE_RECORD) {
                                try {
                                    newMediaRecorder.start();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }

                        }

                        @Override
                        public void onConfigureFailed(CameraCaptureSession session) {
                            cameraCaptureSession = session;
                        }
                    },
                    null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }

    }

//    public void startPreview() throws CameraAccessException {
//        createCaptureSession(CameraDevice.TEMPLATE_PREVIEW, pictureImageReader.getSurface(), imageStreamReader.getSurface());
//    }

    ///启动摄像
    public void startVideoRecording(MethodCall call, MethodChannel.Result result) {
        ///针对Android低端机型，使用低帧率模式
        boolean androidLowRecorder = true;
        if (call.argument("lowRecorder") != null) {
            androidLowRecorder = (Integer) call.argument("lowRecorder") == 1;
        }


        final File outputDir = context.getCacheDir();
        try {
            videoRecordingFile = File.createTempFile("REC", ".mp4", outputDir);
        } catch (IOException | SecurityException e) {
            return;
        }
//        newMediaRecorder = new NewMediaRecorder(EGLUtil.context, videoRecordingFile.getPath()
//                , render.mEGLContext, 2076, 2768);
        newMediaRecorder = new NewMediaRecorder(EGLUtil.context, videoRecordingFile.getPath()
                , render.mEGLContext, ApCameraPlugin.cameraWidth, ApCameraPlugin.cameraHeight
                , androidLowRecorder);
        render.setNewMediaRecorder(newMediaRecorder);

        recordingVideo = true;
        createPreviewSession(CameraDevice.TEMPLATE_RECORD);
        result.success(1);


    }

    ///停止摄像
    public void stopVideoRecording(@NonNull final MethodChannel.Result result) {
        if (!recordingVideo) {
            result.success(null);
            return;
        }

        try {
            recordingVideo = false;

            try {
                cameraCaptureSession.abortCaptures();
//                mediaRecorder.stop();
                newMediaRecorder.stop();
                newMediaRecorder = null;
            } catch (CameraAccessException | IllegalStateException e) {
                // Ignore exceptions and try to continue (changes are camera session already aborted capture)
            }
            createPreviewSession(CameraDevice.TEMPLATE_PREVIEW);
            result.success(videoRecordingFile.getAbsolutePath());
            videoRecordingFile = null;
        } catch (IllegalStateException e) {
            result.error("videoRecordingFailed", e.getMessage(), null);
        }
    }

    ///停止摄像
    public String stopVideoRecording() {
        if (!recordingVideo) {
            return null;
        }

        try {
            recordingVideo = false;

            try {
                cameraCaptureSession.abortCaptures();
                newMediaRecorder.stop();
                newMediaRecorder = null;
            } catch (CameraAccessException | IllegalStateException e) {
                // Ignore exceptions and try to continue (changes are camera session already aborted capture)
            }
            createPreviewSession(CameraDevice.TEMPLATE_PREVIEW);
            String path = videoRecordingFile.getPath();
            videoRecordingFile = null;
            return path;
        } catch (IllegalStateException e) {

        }
        return null;
    }

    public void setBeautifyFilter(boolean open) {
        beautifyFilter = open;
        if (render != null) {
            render.setBeautifyFilter(beautifyFilter);
        }
    }

//    private void createCaptureSession(int templateType, Surface... surfaces)
//            throws CameraAccessException {
//        createCaptureSession(templateType, null, surfaces);
//    }

//    private void createCaptureSession(
//            int templateType, final Runnable onSuccessCallback, Surface... surfaces)
//            throws CameraAccessException {
//        // Close any existing capture session.
//        closeCaptureSession();
//
//        // Create a new capture builder.
//        captureRequestBuilder = cameraDevice.createCaptureRequest(templateType);
//
//        // Build Flutter surface to render to
//
//        render.setSize(previewSize);
//        Surface flutterSurface = new Surface(render.inputSurfaceTexture);
//        captureRequestBuilder.addTarget(flutterSurface);
//
//        List<Surface> remainingSurfaces = Arrays.asList(surfaces);
//        if (templateType != CameraDevice.TEMPLATE_PREVIEW) {
//            // If it is not preview mode, add all surfaces as targets.
//            for (Surface surface : remainingSurfaces) {
//                captureRequestBuilder.addTarget(surface);
//            }
//        }
//
//
//        // Prepare  the callback
//        CameraCaptureSession.StateCallback callback =
//                new CameraCaptureSession.StateCallback() {
//                    @Override
//                    public void onConfigured(@NonNull CameraCaptureSession session) {
//
//                        cameraCaptureSession = session;
//
//                        refreshPreviewCaptureSession(
//                                onSuccessCallback);
//                    }
//
//                    @Override
//                    public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession) {
//
//                    }
//
//                };
//
//        // Start the session
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
//            // Collect all surfaces we want to render to.
//            List<OutputConfiguration> configs = new ArrayList<>();
//            configs.add(new OutputConfiguration(flutterSurface));
//            for (Surface surface : remainingSurfaces) {
//                configs.add(new OutputConfiguration(surface));
//            }
//            createCaptureSessionWithSessionConfig(configs, callback);
//        } else {
//            // Collect all surfaces we want to render to.
//            List<Surface> surfaceList = new ArrayList<>();
//            surfaceList.add(flutterSurface);
//            surfaceList.addAll(remainingSurfaces);
//            createCaptureSession(surfaceList, callback);
//        }
//    }

//    @TargetApi(Build.VERSION_CODES.P)
//    private void createCaptureSessionWithSessionConfig(
//            List<OutputConfiguration> outputConfigs, CameraCaptureSession.StateCallback callback)
//            throws CameraAccessException {
//        cameraDevice.createCaptureSession(
//                new SessionConfiguration(
//                        SessionConfiguration.SESSION_REGULAR,
//                        outputConfigs,
//                        Executors.newSingleThreadExecutor(),
//                        callback));
//    }
//
//    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
//    @SuppressWarnings("deprecation")
//    private void createCaptureSession(
//            List<Surface> surfaces, CameraCaptureSession.StateCallback callback)
//            throws CameraAccessException {
//        cameraDevice.createCaptureSession(surfaces, callback, null);
//    }

//    private void refreshPreviewCaptureSession(
//            @Nullable Runnable onSuccessCallback) {
//        if (cameraCaptureSession == null) {
//            return;
//        }
//
//        try {
//            cameraCaptureSession.setRepeatingRequest(
//                    captureRequestBuilder.build(),
//                    new CameraCaptureSession.CaptureCallback() {
//                        @Override
//                        public void onCaptureStarted(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, long timestamp, long frameNumber) {
//                            super.onCaptureStarted(session, request, timestamp, frameNumber);
//                        }
//
//                        @Override
//                        public void onCaptureProgressed(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, @NonNull CaptureResult partialResult) {
//                            super.onCaptureProgressed(session, request, partialResult);
//                        }
//
//                        @Override
//                        public void onCaptureCompleted(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, @NonNull TotalCaptureResult result) {
//                            super.onCaptureCompleted(session, request, result);
//                        }
//
//                        @Override
//                        public void onCaptureFailed(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, @NonNull CaptureFailure failure) {
//                            super.onCaptureFailed(session, request, failure);
//                        }
//                    },
//                    new Handler(Looper.getMainLooper()));
//
//            if (onSuccessCallback != null) {
//                onSuccessCallback.run();
//            }
//        } catch (CameraAccessException | IllegalStateException | IllegalArgumentException e) {
//
//        }
//    }


    private void closeCaptureSession() {
        if (cameraCaptureSession != null) {
            cameraCaptureSession.close();
            cameraCaptureSession = null;
        }
    }

    public void close() {
        closeCaptureSession();

        if (cameraDevice != null) {
            cameraDevice.close();
            cameraDevice = null;
        }
        if (imageStreamReader != null) {
            imageStreamReader.close();
            imageStreamReader = null;
        }
        if (newMediaRecorder != null) {
            newMediaRecorder.stop();
            newMediaRecorder = null;
        }
    }

    private Size setOptimalPreviewSize() {
        return new Size(Camera_Height, Camera_Width);
//        List<Size> bigEnoughSizes = new ArrayList<>();
//        List<Size> notBigEnoughSizes = new ArrayList<>();
//
//        for (Size size : sizes) {
//            if (size.getWidth() >= previewViewWidth && size.getHeight() >= previewViewHeight) {
//                bigEnoughSizes.add(size);
//            } else {
//                notBigEnoughSizes.add(size);
//            }
//        }
//
//        if (bigEnoughSizes.size() > 0) {
//            return Collections.min(bigEnoughSizes, new CameraUtils.CompareSizesByArea());
//        } else if (notBigEnoughSizes.size() > 0) {
//            return Collections.max(notBigEnoughSizes, new CameraUtils.CompareSizesByArea());
//        } else {
//            Log.d(TAG, "未找到合适的预览尺寸");
//            return sizes[0];
//        }
    }


}
