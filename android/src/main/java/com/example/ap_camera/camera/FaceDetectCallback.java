package com.example.ap_camera.camera;

import android.graphics.Bitmap;

import com.google.mlkit.vision.face.Face;

import java.util.List;

/**
 * Macx
 * 2021/5/19
 *
 * @Description: 人脸检测回调
 */
public interface FaceDetectCallback {
   void onDetectFace(List<Face> faces, Bitmap bitmap);

   void onNoFace();
}
