package com.example.ap_camera;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.SurfaceTexture;
import android.net.Uri;
import android.opengl.GLES20;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.Surface;

import com.example.ap_camera.camera.Camera;
import com.example.ap_camera.camera.CameraUtils;
import com.example.ap_camera.camera.FaceDetectCallback;
import com.example.ap_camera.egl.EGLTextureRender;
import com.example.ap_camera.egl.EGLUtil;
import com.example.ap_camera.egl.InitCompleteCallBack;
import com.example.ap_camera.gif.GifDecoder;
import com.example.ap_camera.record.MediaUtil;
import com.example.ap_camera.test.TestActivity;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.mlkit.vision.common.InputImage;
import com.google.mlkit.vision.face.Face;
import com.google.mlkit.vision.face.FaceDetection;
import com.google.mlkit.vision.face.FaceDetector;
import com.google.mlkit.vision.face.FaceDetectorOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import androidx.annotation.NonNull;

import androidx.annotation.Nullable;
import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.embedding.engine.plugins.activity.ActivityAware;
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry.Registrar;
import io.flutter.view.TextureRegistry;

import static com.example.ap_camera.camera.Camera.Camera_Height;
import static com.example.ap_camera.camera.Camera.Camera_Width;
import static java.lang.Math.max;
import static java.lang.Math.min;

/**
 * ApCameraPlugin
 */
public class ApCameraPlugin implements FlutterPlugin, MethodCallHandler, ActivityAware, FaceDetectCallback {
    private static final String TAG = "ApCameraPlugin";
    /// The MethodChannel that will the communication between Flutter and native Android
    ///
    /// This local reference serves to register the plugin with the Flutter Engine and unregister it
    /// when the Flutter Engine is detached from the Activity
    private MethodChannel channel;

    private Camera camera;

    TextureRegistry textureRegistry;

    Context context;

    Activity activity;

    EGLTextureRender render;

    Handler handler;

    public static int cameraWidth = 576;

    public static int cameraHeight = 1024;

    ///视频时长 秒
    final int videoDuration = 11;
    ///当前录制时长 *50为秒
    int seconds = 0;

    Timer timer;

    ///是否已经上传过微笑事件
    boolean takeSmiling = false;

    ///是否已经上传过眨眼事件
    boolean takeEyeOpen = false;

    ///是否开启人脸抓拍
    boolean openStartFaceDetectTakePhoto = false;

    ///人脸抓拍是否保存图片返回path
    // 暂时无效
    boolean faceDetectTakePhotoSavePhoto = false;

    ///人脸抓拍 - 二进制图片数据传输
    boolean faceDetectPhotoDataEnable = false;

    HashMap<String, String> emojisName2File;

    @Override
    public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
        channel = new MethodChannel(flutterPluginBinding.getBinaryMessenger(), "com.aplus.plugin.camera");
        channel.setMethodCallHandler(this);
        textureRegistry = flutterPluginBinding.getTextureRegistry();
        context = flutterPluginBinding.getApplicationContext();
        EGLUtil.init(context);
        handler = new Handler(Looper.getMainLooper());
    }

    @Override
    public void onMethodCall(@NonNull MethodCall call, @NonNull Result result) {
        if (call.method.equals("getPlatformVersion")) {
            result.success("Android " + android.os.Build.VERSION.RELEASE);
        } else if (call.method.equals("getAllEmojiStickers")) {
            emojisName2File = new HashMap<>();
            List<String> emojis = new ArrayList();
            JSONObject jsonObject = getJSONObject("sticker.json", context);
            try {

                JSONArray jsonArray = jsonObject.getJSONArray("stickers");
                for (int i = 0; i < jsonArray.length(); i++) {
                    String name = (String) ((JSONObject) jsonArray.get(i)).get("name");
                    String fileName = (String) ((JSONObject) jsonArray.get(i)).get("file");
                    emojis.add(name);
                    emojisName2File.put(name, fileName);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            result.success(emojis);
        } else if (call.method.equals("useEmojis")) {
            List list = (List) call.argument("emojis");

            List listFileName = new ArrayList();
            for (Object name : list) {
                if (name instanceof String) {
                    listFileName.add(emojisName2File.get(name));
                }
            }
            render.setEmojis(listFileName);
            result.success(null);
        } else if (call.method.equals("startFaceDetectTakePhoto")) {
            openStartFaceDetectTakePhoto = true;
            faceDetectTakePhotoSavePhoto = (Integer) call.argument("savePhoto") == 1;
            result.success(null);
        } else if (call.method.equals("setFaceDetect")) {
            boolean isOpenDetect = (Integer) call.argument("enable") == 1;
            result.success(null);
        } else if (call.method.equals("showEmoji")) {
            boolean isShow = (Integer) call.argument("enable") == 1;
            render.showEmoji(isShow);
            result.success(null);
        } else if (call.method.equals("setFaceDetectPhotoDataEnable")) {
            faceDetectPhotoDataEnable = (Integer) call.argument("enable") == 1;

            result.success(null);
        } else if (call.method.equals("startEmojiGif")) {
            render.startOrStopEmojiGif(true);
            result.success(null);
        } else if (call.method.equals("stopVerify")) {
            if (timer != null) {
                timer.cancel();
                timer = null;
            }
            camera.close();
            result.success(null);
        } else if (call.method.equals("stopEmojiGif")) {

            render.startOrStopEmojiGif(false);
            result.success(null);
        } else if (call.method.equals("availableCameras")) {
            try {
//                Intent intent=new Intent(activity,TestActivity.class);
//                activity.startActivity(intent);
                result.success(CameraUtils.getAvailableCameras(context));
            } catch (Exception e) {
                Log.e(TAG, "onMethodCall: ", e);
            }
        } else if (call.method.equals("createCamera")) {
            createCamera(call, result);
        } else if (call.method.equals("detectFaceNumWithImageFile")) {
            ///解析图片
            detectFaceNumWithImageFile(call, result);
        } else if (call.method.equals("releaseCamera")) {
            if (camera != null) {
                camera.close();
                camera = null;
            }
        } else if (call.method.equals("resumeCamera")) {
            resumeCamera(call, result);
        } else if (call.method.equals("startCamera")) {
            startCamera(call, result);
        } else if (call.method.equals("takePhoto")) {
            takePhoto(call, result);
        } else if (call.method.equals("startVerify")) {
            startVerify(call, result);
        } else if (call.method.equals("startVideoBGMCompose")) {
            final String path = call.argument("videoFile");

            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    ///延时1秒进行合成，避免视频为完全录制结束
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    final File outputDir = EGLUtil.context.getCacheDir();
                    try {
                        File videoRecordingFile = File.createTempFile("REC", ".mp4", outputDir);
                        Log.d(TAG, "videoFile: 输入" + path + "\n 输出" + videoRecordingFile.getPath());
                        MediaUtil.startComposeTrack(context, path, videoRecordingFile.getPath());
                        final HashMap<String, Object> map = new HashMap<>();
                        map.put("videoFile", videoRecordingFile.getPath());
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                channel.invokeMethod("eventVideoBGMCompose", map);

                            }
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
            thread.start();
        } else if (call.method.equals("pauseCamera")) {
            camera.close();
            result.success(null);
        } else if (call.method.equals("startRecord")) {
            camera.startVideoRecording(call, result);

        } else if (call.method.equals("stopRecord")) {
            camera.stopVideoRecording(result);
        } else if (call.method.equals("setBeautifyFilter")) {
            camera.setBeautifyFilter(((Integer) call.argument("enable") == 1));
            result.success(null);
        } else if (call.method.equals("cleanAllCache")) {
            cleanAllCache();
            result.success(null);
        } else {
            result.notImplemented();
        }
    }

    ///解析人脸返回人脸个数
    private void detectFaceNumWithImageFile(@NonNull MethodCall call, @NonNull final Result result) {
        InputImage image;
        try {
            FaceDetectorOptions highAccuracyOpts =
                    new FaceDetectorOptions.Builder()
                            .setPerformanceMode(FaceDetectorOptions.PERFORMANCE_MODE_ACCURATE)
                            .setLandmarkMode(FaceDetectorOptions.LANDMARK_MODE_ALL)
                            .setClassificationMode(FaceDetectorOptions.CLASSIFICATION_MODE_ALL)
                            .setMinFaceSize(1.0f)
                            .build();
            FaceDetector detector = FaceDetection.getClient(highAccuracyOpts);
            image = InputImage.fromFilePath(context, Uri.fromFile(new File((String) call.argument("file"))));
            Task<List<Face>> faceTask =
                    detector.process(image)
                            .addOnSuccessListener(
                                    new OnSuccessListener<List<Face>>() {
                                        @Override
                                        public void onSuccess(final List<Face> faces) {
                                            // Task completed successfully
                                            // ...
                                            handler.post(new Runnable() {
                                                @Override
                                                public void run() {
                                                    result.success(faces.size());
                                                }
                                            });

                                        }
                                    })
                            .addOnFailureListener(
                                    new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            // Task failed with an exception
                                            // ...
                                            handler.post(new Runnable() {
                                                @Override
                                                public void run() {
                                                    result.success(0);
                                                }
                                            });
                                        }
                                    });

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void cleanAllCache() {
        final File outputDir = context.getCacheDir();
        //目录存在，且为文件夹
        if (outputDir.exists() && outputDir.isDirectory()) {
            //找出下面所有的文件或者文件夹名称
            String[] list = outputDir.list();
            for (String name : list) {
                if (name.length() >= 3 && name.substring(0, 3).equals("REC")) {
                    File video = new File(outputDir + File.separator + name);
                    video.delete();
                }
            }
        }
    }


    private void startVerify(MethodCall call, final MethodChannel.Result result) {
        seconds = 0;
        takeEyeOpen = false;
        takeSmiling = false;
        timer = new Timer();
        //开始录制
        camera.startVideoRecording(call, result);

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //播放动画
                render.startEmojiAnim(new EGLTextureRender.EmojiAnimCompleteCallBack() {
                    @Override
                    public void onComplete() {
                        //动画播放完成
                    }
                });
            }
        }, 800);


        TimerTask task = new TimerTask() {

            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        seconds++;
                        HashMap<String, Object> map = new HashMap<>();
                        map.put("timeMS", seconds * 50);
                        channel.invokeMethod("eventRecordingProgress", map);

                        if (seconds * 50 == videoDuration * 1000) {
                            final String path = camera.stopVideoRecording();

                            if (path != null) {

                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        HashMap<String, String> map = new HashMap<>();
                                        map.put("videoFile", path);
                                        channel.invokeMethod("eventVerifyRecordComplete", map);
                                    }
                                });

                            }
                            timer.cancel();
                            timer = null;
                        }
                    }
                });

            }
        };
        timer.schedule(task, 0, 50);
    }

    @Override
    public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
        channel.setMethodCallHandler(null);

    }


    private void createCamera(@NonNull MethodCall call, @NonNull final Result result) {
//        String position = call.argument("position");
//        String resolution = call.argument("resolution");
//        if (camera != null) {
//            camera.close();
//        }


        final TextureRegistry.SurfaceTextureEntry flutterSurfaceTexture =
                textureRegistry.createSurfaceTexture();
        SurfaceTexture surfaceTexture = flutterSurfaceTexture.surfaceTexture();
        float wScale = EGLUtil.displayWidth / (float) cameraWidth;
        float hScale = EGLUtil.displayHeight / (float) cameraHeight;
        float scale = min(wScale, hScale);
        int renderW = (int) (cameraWidth * scale);
        int renderH = (int) (cameraHeight * scale);
        surfaceTexture.setDefaultBufferSize(renderW, renderH);
        render = new EGLTextureRender(surfaceTexture, new InitCompleteCallBack() {
            @Override
            public void onInitComplete() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        HashMap<String, Object> map = new HashMap<>();
                        map.put("textureId", (int) (flutterSurfaceTexture.id()));
                        map.put("width", Double.valueOf(cameraWidth));
                        map.put("height", Double.valueOf(cameraHeight));
                        result.success(map);
                    }
                });
            }
        });
        camera = new Camera(context, activity, render, this);

    }


    private void startCamera(@NonNull MethodCall call, @NonNull Result result) {


        if (camera != null) {
            try {
                camera.setupCamera();
                result.success(null);
            } catch (Exception e) {
                Log.d(TAG, "startCamera: ");
                Log.e(TAG, "startCamera: ", e);
            }
        } else {
            result.success(null);
        }

    }

    private void takePhoto(@NonNull MethodCall call, @NonNull final Result result) {
        render.takePhoto(new TakePhotoCallBack() {
            @Override
            public void CompleteTakePhoto(String path) {
                result.success(path);
            }
        });

    }

    private void resumeCamera(@NonNull MethodCall call, @NonNull Result result) {
        if (camera != null) {
            try {
                camera.setupCamera();
                result.success(null);
            } catch (Exception e) {

            }
        } else {
            result.success(null);
        }

    }

    @Override
    public void onAttachedToActivity(@NonNull ActivityPluginBinding binding) {
        activity = binding.getActivity();
    }

    @Override
    public void onDetachedFromActivityForConfigChanges() {

    }

    @Override
    public void onReattachedToActivityForConfigChanges(@NonNull ActivityPluginBinding binding) {

    }

    @Override
    public void onDetachedFromActivity() {

    }

    @Override
    public void onDetectFace(List<Face> faces, final Bitmap bitmap) {
        final HashMap<String, Object> map = new HashMap<>();
        final ArrayList<HashMap<String, Object>> list = new ArrayList<>();

        for (Face face : faces) {
            HashMap<String, Object> data = new HashMap<>();
            data.put("trackId", face.getTrackingId());
            data.put("smile", face.getSmilingProbability());
            data.put("leftEyeOpen", face.getLeftEyeOpenProbability());
            data.put("rightEyeOpen", face.getRightEyeOpenProbability());
            Rect rect = face.getBoundingBox();
            data.put("x", (double) rect.left);
            data.put("y", (double) rect.top);
            data.put("width", (double) rect.width());
            data.put("height", (double) rect.height());
            data.put("cameraHeight", (double) Camera_Height);
            data.put("cameraWidth", (double) Camera_Width);
            Log.d(TAG, "onDetectFace: " + rect.left + "  " + rect.top + "  " + rect.right);
            if (openStartFaceDetectTakePhoto) {
                if (face.getSmilingProbability() > 0.7 && !takeSmiling) {
                    takeSmiling = true;
                    HashMap<String, Object> smilingMap = new HashMap<>();
                    smilingMap.put("action", 1);
                    smilingMap.put("timestampMS", seconds * 50);
                    channel.invokeMethod("eventFaceDetectTakePhoto", smilingMap);
                }
                if ((face.getLeftEyeOpenProbability() < 0.1 || face.getRightEyeOpenProbability() < 0.1) && !takeEyeOpen) {
                    takeEyeOpen = true;
                    HashMap<String, Object> takeEyeOpenMap = new HashMap<>();
                    if (face.getLeftEyeOpenProbability() < 0.1) {
                        takeEyeOpenMap.put("action", 2);
                    } else if (face.getRightEyeOpenProbability() < 0.1) {
                        takeEyeOpenMap.put("action", 3);
                    } else {
                        takeEyeOpenMap.put("action", 4);
                    }

                    takeEyeOpenMap.put("timestampMS", seconds * 50);
                    channel.invokeMethod("eventFaceDetectTakePhoto", takeEyeOpenMap);

                }
            }

            list.add(data);
        }
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                if (faceDetectPhotoDataEnable&&bitmap!=null) {
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 20, baos);
                    byte[] dataBytes = baos.toByteArray();
                    map.put("data", dataBytes);
                }
                map.put("list", list);
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        channel.invokeMethod("eventFaceDetect", map);
                    }
                });

            }
        });
        thread.start();


    }

    @Override
    public void onNoFace() {

    }

    public static JSONObject getJSONObject(String fileName, Context context) {
        try {
            return new JSONObject(getJson(fileName, context));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String getJson(String fileName, Context context) {
        StringBuilder stringBuilder = new StringBuilder();
        try {
            //获取assets资源管理器
            AssetManager assetManager = context.getAssets();
            //通过管理器打开文件并读取
            BufferedReader bf = new BufferedReader(new InputStreamReader(
                    assetManager.open(fileName)));
            String line;
            while ((line = bf.readLine()) != null) {
                stringBuilder.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringBuilder.toString();
    }
}

