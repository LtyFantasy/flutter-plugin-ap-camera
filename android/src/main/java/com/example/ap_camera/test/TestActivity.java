package com.example.ap_camera.test;

import android.app.Activity;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.util.Log;
import android.util.Size;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.View;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.VideoView;

import com.example.ap_camera.ApCameraPlugin;
import com.example.ap_camera.R;
import com.example.ap_camera.camera.Camera;
import com.example.ap_camera.egl.EGLTextureRender;
import com.example.ap_camera.egl.InitCompleteCallBack;
import com.example.ap_camera.record.NewMediaRecorder;
import com.example.ap_camera.record.RecorderEGLEnv;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Macx
 * 2021/5/13
 *
 * @Description:
 */
public class TestActivity extends Activity {
    TextureView textureView;
    TextureView textureViewMini;
    EGLTextureRender render;
    private Camera camera;
    private static final String TAG = "TestActivity";
    public static RecorderEGLEnv eglEnv;
    public static Handler mHandler;
    public static Handler mMainHandler;
    boolean beautify = false;

    boolean startRecorder = false;
    public static NewMediaRecorder newMediaRecorder;
    File videoRecordingFile;
    VideoView videoView;
    Surface miniSurface;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_activity);
        //創建OpenGL 的 環境
        HandlerThread handlerThread = new HandlerThread("codec-gl");
        handlerThread.start();
        mHandler = new Handler(handlerThread.getLooper());
        mMainHandler = new Handler(getMainLooper());
        final Button button = findViewById(R.id.btn);
        videoView = findViewById(R.id.videoView);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                beautify = !beautify;
//                camera.setBeautifyFilter(beautify);
                if (!startRecorder) {
                    mMainHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            button.setText("录制中");
                        }
                    });

                    startRecorder = true;
                    final File outputDir = TestActivity.this.getCacheDir();
                    try {
                        videoRecordingFile = File.createTempFile("REC", ".mp4", outputDir);


                        newMediaRecorder = new NewMediaRecorder(TestActivity.this,
                                videoRecordingFile.getPath(), render.mEGLContext, ApCameraPlugin.cameraWidth, ApCameraPlugin.cameraHeight
                        ,false);
                        newMediaRecorder.start();
                        mHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                newMediaRecorder.stop();
                                mMainHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        button.setText("播放");
                                    }
                                });
                            }
                        }, 10000);
                    } catch (IOException | SecurityException e) {
                        return;
                    }
                } else {
                    //播放
                    videoView.setVideoPath(videoRecordingFile.getPath());
                    Log.d(TAG, "onClick: " + videoRecordingFile.getPath());
                    //创建MediaController对象
                    MediaController mediaController = new MediaController(TestActivity.this);

                    //VideoView与MediaController建立关联
                    videoView.setMediaController(mediaController);

                    //让VideoView获取焦点
                    videoView.requestFocus();
                    mediaController.show();
                    videoView.start();
                    videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                        @Override
                        public void onCompletion(MediaPlayer mPlayer) {
                            // TODO Auto-generated method stub
                            mPlayer.start();
                            mPlayer.setLooping(true);
                        }
                    });
                }

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        textureViewMini = findViewById(R.id.textureViewMini);
        textureViewMini.setSurfaceTextureListener(new TextureView.SurfaceTextureListener() {
            @Override
            public void onSurfaceTextureAvailable(@NonNull final SurfaceTexture surfaceTexture, final int i, final int i1) {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        // 创建EGL环境
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        miniSurface = new Surface(surfaceTexture);
                        eglEnv = new RecorderEGLEnv(TestActivity.this, render.mEGLContext, miniSurface
                                , i, i1);

                    }
                });

            }

            @Override
            public void onSurfaceTextureSizeChanged(@NonNull SurfaceTexture surfaceTexture, int i, int i1) {


            }

            @Override
            public boolean onSurfaceTextureDestroyed(@NonNull SurfaceTexture surfaceTexture) {
                return false;
            }

            @Override
            public void onSurfaceTextureUpdated(@NonNull SurfaceTexture surfaceTexture) {

            }
        });
        textureView = findViewById(R.id.textureView);
        textureView.setSurfaceTextureListener(new TextureView.SurfaceTextureListener() {
            @Override
            public void onSurfaceTextureAvailable(@NonNull SurfaceTexture surfaceTexture, int width, int height) {
                render = new EGLTextureRender(surfaceTexture, new InitCompleteCallBack() {
                    @Override
                    public void onInitComplete() {

                    }
                });

                camera = new Camera(TestActivity.this, TestActivity.this, render, null);
                camera.setupCamera();
                Log.d(TAG, "onSurfaceTextureAvailable: " + width + "  " + height);

            }

            @Override
            public void onSurfaceTextureSizeChanged(@NonNull SurfaceTexture surfaceTexture, int width, int height) {

            }

            @Override
            public boolean onSurfaceTextureDestroyed(@NonNull SurfaceTexture surfaceTexture) {
                return false;
            }

            @Override
            public void onSurfaceTextureUpdated(@NonNull SurfaceTexture surfaceTexture) {

            }
        });
//        surfaceView.getHolder().addCallback(new SurfaceHolder.Callback() {
//            @Override
//            public void surfaceCreated(SurfaceHolder holder) {
//
//            }
//
//            @Override
//            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
//
//            }
//
//            @Override
//            public void surfaceDestroyed(SurfaceHolder holder) {
//
//            }
//        });
    }

    public static void draw(final int id, final long timestamp) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                //画画
                eglEnv.draw(id, timestamp);
                if (newMediaRecorder != null)
                    newMediaRecorder.fireFrame(id, timestamp);
            }
        });
    }
}
