package com.example.ap_camera;

public interface TakePhotoCallBack{
    public void CompleteTakePhoto(String path);
}
