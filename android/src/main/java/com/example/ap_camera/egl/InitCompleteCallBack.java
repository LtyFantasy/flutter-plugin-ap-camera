package com.example.ap_camera.egl;

public interface InitCompleteCallBack {
   public void  onInitComplete();
}
