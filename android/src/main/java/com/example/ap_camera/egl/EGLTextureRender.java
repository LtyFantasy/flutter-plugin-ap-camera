package com.example.ap_camera.egl;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.SurfaceTexture;
import android.opengl.EGL14;
import android.opengl.EGLConfig;
import android.opengl.EGLContext;
import android.opengl.EGLDisplay;
import android.opengl.EGLSurface;
import android.opengl.GLES11Ext;
import android.opengl.GLES20;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.util.Size;

import com.example.android.camera2basic.watermark.EmojiRender;
import com.example.ap_camera.ApCameraPlugin;
import com.example.ap_camera.R;
import com.example.ap_camera.TakePhotoCallBack;
import com.example.ap_camera.filter.BaseFilter;
import com.example.ap_camera.filter.BeautyFilter;
import com.example.ap_camera.record.MediaRecorder;
import com.example.ap_camera.record.NewMediaRecorder;
import com.example.ap_camera.record.RecorderFilter;
import com.example.ap_camera.test.TestActivity;


import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;

import javax.microedition.khronos.opengles.GL10;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import static android.opengl.GLES20.glVertexAttribPointer;
import static java.lang.Math.max;
import static java.lang.Math.min;

/**
 * Created by wangyt on 2019/5/10
 */
public class EGLTextureRender implements SurfaceTexture.OnFrameAvailableListener {
    private static final String TAG = "EGLTextureRender";
    private EGLConfig[] mEGLConfig = new EGLConfig[1];
    private EGLDisplay mEGLDisplay;
    public EGLContext mEGLContext;
    EGLSurface mEglSurface;

    HandlerThread handlerThread;
    Handler handler;
    Object outputSurfaceTexture;
    public SurfaceTexture inputSurfaceTexture;
    private CameraFilter mCameraFilter;
    private BaseFilter baseFilter;
    private BeautyFilter beautyFilter;
    private EmojiRender emojiRender1;
    private EmojiRender emojiRender2;
    private EmojiRender emojiRender3;
    private EmojiRender emojiRender4;
    //    private MediaRecorder mediaRecorder;
    boolean beautifyFilter = false;
    private NewMediaRecorder newMediaRecorder;
    int mOESTextureId;
    private float[] transformMatrix = new float[16];

    TakePhotoCallBack takePhotoCallBack;
    boolean takePhoto = false;

    int renderW;
    int renderH;

    //    private FilterChain filterChain;
    InitCompleteCallBack mInitCompleteCallBack;

    public EGLTextureRender(Object outputSurfaceTexture, InitCompleteCallBack initCompleteCallBack) {
        this.outputSurfaceTexture = outputSurfaceTexture;
        mInitCompleteCallBack = initCompleteCallBack;
        handlerThread = new HandlerThread("gl_thread");
        handlerThread.start();
        handler = new Handler(handlerThread.getLooper()) {
            @Override
            public void handleMessage(@NonNull Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case 0:
                        //init
                        init();
                        if (mInitCompleteCallBack != null) {
                            mInitCompleteCallBack.onInitComplete();
                        }
                        break;
                    case 1:
                        //drawFrame
                        drawFrame();
                        break;
                    case 2:
                        //setSize
                        onSurfaceChanged(msg.arg1, msg.arg2);
                        break;
                }
            }
        };
        handler.sendEmptyMessage(0);


    }

    public void setNewMediaRecorder(NewMediaRecorder newMediaRecorder) {
        this.newMediaRecorder = newMediaRecorder;
    }

//    public void setMediaRecorder(MediaRecorder mediaRecorder) {
//        this.mediaRecorder = mediaRecorder;
//    }

    private void init() {

        createEGL();
        mOESTextureId = createOESTextureObject();
        baseFilter = new BaseFilter();
        emojiRender1 = new EmojiRender(EGLUtil.context);
        emojiRender2 = new EmojiRender(EGLUtil.context);
        emojiRender3 = new EmojiRender(EGLUtil.context);
        emojiRender4 = new EmojiRender(EGLUtil.context);

    }

    public void setSize(Size previewSize) {
        if (inputSurfaceTexture == null) {
            inputSurfaceTexture = new SurfaceTexture(mOESTextureId);
            inputSurfaceTexture.setOnFrameAvailableListener(this);
        }
        handler.sendMessage(handler.obtainMessage(2, previewSize.getWidth(),
                previewSize.getHeight()));

    }

    public void setEmojis(List<String> names) {
        if (names.size() >= 4) {
            emojiRender1.setEmojiName(names.get(0));
            emojiRender2.setEmojiName(names.get(1));
            emojiRender3.setEmojiName(names.get(2));
            emojiRender4.setEmojiName(names.get(3));
        }
    }

    public void showEmoji(boolean show) {
        emojiRender1.setShow(show);
        emojiRender2.setShow(show);
        emojiRender3.setShow(show);
        emojiRender4.setShow(show);

    }

    public void startOrStopEmojiGif(boolean start) {
        emojiRender1.startOrStopGif(start);
        emojiRender2.startOrStopGif(start);
        emojiRender3.startOrStopGif(start);
        emojiRender4.startOrStopGif(start);
    }


    ///启动录制中动画
    public void startEmojiAnim(final EmojiAnimCompleteCallBack emojiAnimCompleteCallBack) {
        startOrStopEmojiGif(false);
        emojiRender1.startOrStopScale(true);
        emojiRender1.startOrStopGif(true);
        ///第二个表情播放
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                emojiRender1.startOrStopGif(false);
                emojiRender2.startOrStopScale(true);
                emojiRender2.startOrStopGif(true);
            }
        }, emojiRender1.getScaleTime());


        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                emojiRender2.startOrStopGif(false);
                emojiRender3.startOrStopScale(true);
                emojiRender3.startOrStopGif(true);
            }
        }, emojiRender1.getScaleTime() * 2);

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                emojiRender3.startOrStopGif(false);
                emojiRender4.startOrStopScale(true);
                emojiRender4.startOrStopGif(true);

            }
        }, emojiRender1.getScaleTime() * 3);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                emojiRender4.startOrStopGif(false);
                emojiAnimCompleteCallBack.onComplete();
            }
        }, emojiRender1.getScaleTime() * 4);
    }

    private void onSurfaceChanged(int width, int height) {

        inputSurfaceTexture.setDefaultBufferSize(width, height);

        float wScale = EGLUtil.displayWidth / (float) height;
        float hScale = EGLUtil.displayHeight / (float) width;
        float scale = min(wScale, hScale);
        renderW = (int) (height * scale);
        renderH = (int) (width * scale);

//        filterChain.setSize(renderW, renderH);
        mCameraFilter = new CameraFilter(mOESTextureId, renderW, renderH);
        beautyFilter = new BeautyFilter(renderW, renderH);
        GLES20.glViewport(0, 0, renderW, renderH);

        float size;
        float top = renderH / 10f;
        float left;
        float offset;
        float topOffset;
        size = renderW / 9f;
        left = renderW / 6f;
        topOffset = renderH / 20f;

        offset = (renderW - (2 * left) - (4 * size)) / 3f;

        emojiRender1.onSizeChanged(renderW, renderH, top + topOffset, left, size);
        emojiRender2.onSizeChanged(renderW, renderH, top, left + (1 * (size + offset)), size);
        emojiRender3.onSizeChanged(renderW, renderH, top, left + (2 * (size + offset)), size);
        emojiRender4.onSizeChanged(renderW, renderH, top + topOffset, left + (3 * (size + offset)), size);

    }


    private void createEGL() {

        //获取显示设备
        mEGLDisplay = EGL14.eglGetDisplay(EGL14.EGL_DEFAULT_DISPLAY);
        if (mEGLDisplay == EGL14.EGL_NO_DISPLAY) {
            throw new RuntimeException("eglGetDisplay failed! " + EGL14.eglGetError());
        }

        //version中存放EGL版本号
        int[] version = new int[2];

        //初始化EGL
        if (!EGL14.eglInitialize(mEGLDisplay, version, 0, version, 1)) {
            throw new RuntimeException("eglInitialize failed");
        }

        //构造需要的配置列表
        int[] configAttribs = {
                EGL14.EGL_RED_SIZE, 8, //颜色缓冲区中红色位数
                EGL14.EGL_GREEN_SIZE, 8,//颜色缓冲区中绿色位数
                EGL14.EGL_BLUE_SIZE, 8, //
                EGL14.EGL_ALPHA_SIZE, 8,//
                EGL14.EGL_RENDERABLE_TYPE, EGL14.EGL_OPENGL_ES2_BIT, //opengl es 2.0
                EGL14.EGL_NONE
        };
        int[] configsNum = new int[1];

        //EGL选择配置
        if (!EGL14.eglChooseConfig(mEGLDisplay, configAttribs, 0, mEGLConfig, 0, mEGLConfig.length,
                configsNum, 0)) {
            throw new RuntimeException("EGL error " + EGL14.eglGetError());
        }

        /**
         * EGL上下文
         */
        int[] context_attrib_list = {
                EGL14.EGL_CONTEXT_CLIENT_VERSION, 2,
                EGL14.EGL_NONE
        };

        mEGLContext = EGL14.eglCreateContext(mEGLDisplay, mEGLConfig[0], EGL14.EGL_NO_CONTEXT, context_attrib_list, 0);


        /**
         * 创建EGLSurface
         */
        int[] surface_attrib_list = {
                EGL14.EGL_NONE
        };
        mEglSurface = EGL14.eglCreateWindowSurface(mEGLDisplay, mEGLConfig[0], outputSurfaceTexture, surface_attrib_list, 0);
        // mEglSurface == null
        if (mEglSurface == null) {
            throw new RuntimeException("EGL error " + EGL14.eglGetError());
        }

        /**
         * 绑定当前线程的显示器display
         */
        if (!EGL14.eglMakeCurrent(mEGLDisplay, mEglSurface, mEglSurface, mEGLContext)) {
            throw new RuntimeException("EGL error " + EGL14.eglGetError());
        }

//        //创建EGL显示窗口
//        mEglSurface = mEgl.eglCreateWindowSurface(mEGLDisplay, mEGLConfig[0], outputSurfaceTexture, null);
//
//        //创建上下文
//        int[] contextAttribs = {
//                EGL14.EGL_CONTEXT_CLIENT_VERSION, 2,
//                EGL10.EGL_NONE
//        };
//        mEGLContext = mEgl.eglCreateContext(mEGLDisplay, mEGLConfig[0], EGL10.EGL_NO_CONTEXT, contextAttribs);
//
//        if (mEGLDisplay == EGL10.EGL_NO_DISPLAY || mEGLContext == EGL10.EGL_NO_CONTEXT) {
//            throw new RuntimeException("eglCreateContext fail failed! " + mEgl.eglGetError());
//        }
//
//        if (!mEgl.eglMakeCurrent(mEGLDisplay, mEglSurface, mEglSurface, mEGLContext)) {
//            throw new RuntimeException("eglMakeCurrent failed! " + mEgl.eglGetError());
//        }
    }

    private void destroyEGL() {

    }


    public void drawFrame() {
        EGL14.eglMakeCurrent(mEGLDisplay, mEglSurface, mEglSurface, mEGLContext);
        inputSurfaceTexture.updateTexImage();
        inputSurfaceTexture.getTransformMatrix(transformMatrix);
        int id = mCameraFilter.drawTexture(transformMatrix);
        if (beautifyFilter) {
            id = beautyFilter.drawTexture(id, 1.0f);
        } else {
            id = beautyFilter.drawTexture(id, 0.01f);
        }
        id = emojiRender1.drawSelf(id);
        id = emojiRender2.drawSelf(id);
        id = emojiRender3.drawSelf(id);
        id = emojiRender4.drawSelf(id);
        baseFilter.onDraw(id);

        if (takePhoto) {
            takePhoto = false;
            File outputDir = EGLUtil.context.getCacheDir();
            final File filePic;
            try {
                filePic = File.createTempFile("Photo", ".jpg", outputDir);
                saveFrame(filePic, renderW, renderH);
                if (takePhotoCallBack != null) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            takePhotoCallBack.CompleteTakePhoto(filePic.getPath());
                            takePhotoCallBack = null;
                        }
                    });
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        EGL14.eglSwapBuffers(mEGLDisplay, mEglSurface);

        EGL14.eglWaitClient();
        EGL14.eglWaitGL();
//        EGL14.eglWaitNative(EGL14.EGL_CORE_NATIVE_ENGINE);

        if (newMediaRecorder != null) {
            newMediaRecorder.fireFrame(id,inputSurfaceTexture.getTimestamp());
        }

        if (TestActivity.eglEnv != null) {
            TestActivity.draw(id,inputSurfaceTexture.getTimestamp());
        }

    }

    public void takePhoto(TakePhotoCallBack takePhotoCallBack) {
        takePhoto = true;
        this.takePhotoCallBack = takePhotoCallBack;

    }

    private void saveFrame(File file, int width, int height) throws IOException {
        String filename = file.toString();
        ByteBuffer buf = ByteBuffer.allocateDirect(width * height * 4);
        buf.order(ByteOrder.BIG_ENDIAN);
        GLES20.glReadPixels(0, 0, width, height, GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, buf);
        buf.rewind();
        BufferedOutputStream bos = null;

        try {
            bos = new BufferedOutputStream(new FileOutputStream(filename));
            Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            bmp.copyPixelsFromBuffer(buf);
            int w = bmp.getWidth();
            int h = bmp.getHeight();
            float sx = ApCameraPlugin.cameraWidth / 2 / (float) width;
            Matrix m = new Matrix();
            m.postScale(-1, 1); //镜像
            m.postRotate(180); //旋转
            m.postScale(sx, sx);
            bmp = Bitmap.createBitmap(bmp, 0, 0, w, h, m, true);
            bmp.compress(Bitmap.CompressFormat.PNG, 60, bos);
            bmp.recycle();
        } finally {
            if (bos != null) {
                bos.close();
            }
        }
    }


    public void setBeautifyFilter(boolean open) {
        beautifyFilter = open;
    }

    public int createOESTextureObject() {
        int[] tex = new int[1];
        GLES20.glGenTextures(1, tex, 0);


        GLES20.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, tex[0]);
        GLES20.glTexParameterf(GLES11Ext.GL_TEXTURE_EXTERNAL_OES,
                GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_NEAREST);
        GLES20.glTexParameterf(GLES11Ext.GL_TEXTURE_EXTERNAL_OES,
                GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);
        GLES20.glTexParameterf(GLES11Ext.GL_TEXTURE_EXTERNAL_OES,
                GL10.GL_TEXTURE_WRAP_S, GL10.GL_CLAMP_TO_EDGE);
        GLES20.glTexParameterf(GLES11Ext.GL_TEXTURE_EXTERNAL_OES,
                GL10.GL_TEXTURE_WRAP_T, GL10.GL_CLAMP_TO_EDGE);
        GLES20.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, 0);

        return tex[0];
    }


//    public void render(Surface surface, int width, int height) {
//        //创建屏幕上渲染区域：EGL窗口
//        int[] surfaceAttribList = {EGL_NONE};
//        EGLSurface eglSurface = eglCreateWindowSurface(eglDisplay, eglConfig, surface, surfaceAttribList, 0);
//        //指定当前上下文
//        eglMakeCurrent(eglDisplay, eglSurface, eglSurface, eglContext);
//        //获取着色器
//        int texVertexShader = loadShader(GL_VERTEX_SHADER, loadShaderSource(R.raw.egl_texture_vertex_shader));
//        int texFragmentShader = loadShader(GL_FRAGMENT_SHADER, loadShaderSource(R.raw.egl_texture_fragtment_shader));
//        //创建并连接程序
//        int program = createAndLinkProgram(texVertexShader, texFragmentShader);
//        //设置清除渲染时的颜色
//        glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
//        //设置视口
//        glViewport(0, 0, width, height);
//        //获取顶点、纹理坐标数据
//        FloatBuffer vertexBuffer = getVertextBuffer();
//        FloatBuffer texCoordBuffer = getTextureCoordBuffer();
//        //擦除屏幕
//        glClear(GL_COLOR_BUFFER_BIT);
//        //使用程序
//        glUseProgram(program);
//
//        //绑定顶点、纹理坐标到指定属性位置
//        int aPosition = glGetAttribLocation(program, "a_Position");
//        int aTexCoord = glGetAttribLocation(program, "a_texCoord");
//        glVertexAttribPointer(aPosition, 3, GL_FLOAT, false, 0, vertexBuffer);
//        glVertexAttribPointer(aTexCoord, 2, GL_FLOAT, false, 0, texCoordBuffer);
//        glEnableVertexAttribArray(aPosition);
//        glEnableVertexAttribArray(aTexCoord);
//        //绑定纹理
//        glActiveTexture(GL_TEXTURE0);
//        glBindTexture(GL_TEXTURE_2D, loadTexture(R.drawable.texture));
//        //Set the sampler texture unit to 0
//        glUniform1i(glGetUniformLocation(program, "s_texture"), 0);
//        //绘制
//        glDrawArrays(GL_TRIANGLES, 0, 3);
//        //交换 surface 和显示器缓存
//        eglSwapBuffers(eglDisplay, eglSurface);
//        //释放
//        eglDestroySurface(eglDisplay, eglSurface);
//    }

    @Override
    public void onFrameAvailable(SurfaceTexture surfaceTexture) {
        handler.sendEmptyMessage(1);
    }

    public interface EmojiAnimCompleteCallBack {
        void onComplete();
    }
}

