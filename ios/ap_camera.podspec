#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html.
# Run `pod lib lint ap_camera.podspec` to validate before publishing.
#
Pod::Spec.new do |s|
  s.name             = 'ap_camera'
  s.version          = '1.0.0'
  s.summary          = 'Flutter video sdk for aplus'
  s.description      = <<-DESC
A new flutter plugin project.
                       DESC
  s.homepage         = 'http://example.com'
  s.license          = { :file => '../LICENSE' }
  s.author           = { 'APlus' => 'loki.aplus@gmail.com' }
  s.source           = { :path => '.' }
  s.source_files     = 'Classes/**/*.{m,h}'
  s.preserve_paths   = 'Resources'
  s.public_header_files = 'Classes/**/*.h'
  s.dependency 'Flutter'
  s.dependency 'GoogleMLKit/FaceDetection'
  #s.dependency 'Firebase/MLVision'
  #s.dependency 'Firebase/MLVisionFaceModel'
  s.platform         = :ios, '11.0'
  s.static_framework = true
  s.resource_bundle  = {
      'APSticker' => [
        'Resources/Stickers/*.gif',
        'Resources/Stickers/*.json'
      ],
      'APMedia' => [
        'Resources/Media/*.mp3',
      ]
  }

  # Flutter.framework does not contain a i386 slice.
  s.pod_target_xcconfig = { 'DEFINES_MODULE' => 'YES', 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'i386' }
  s.xcconfig = {
    # 预编译宏，屏蔽OpenGL Deprecation 相关Warning
   "GCC_PREPROCESSOR_DEFINITIONS" => 'GLES_SILENCE_DEPRECATION=1'
  }
end
