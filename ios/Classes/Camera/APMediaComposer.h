//
//  APMediaComposer.h
//  ap_camera
//
//  Created by wooplus on 2021/5/20.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^ExportEvent) (AVAssetExportSessionStatus status, NSError  * _Nullable error);

@interface APMediaComposer : NSObject

+ (NSString*)audioFile;

+ (void)editVideoSynthesizeVieoPath:(NSURL *)assetURL
                            BGMPath:(NSURL *)BGMPath
                         outputPath:(NSURL *)outputPath
                  needOriginalVoice:(BOOL)needOriginalVoice
                        videoVolume:(CGFloat)videoVolume
                          BGMVolume:(CGFloat)BGMVolume
                         eventBlock:(ExportEvent)block;

@end

NS_ASSUME_NONNULL_END
