//
//  CameraFile.h
//  ap_camera
//
//  Created by wooplus on 2021/4/30.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CameraFile : NSObject

// 是否是视频文件
@property (nonatomic, assign) BOOL isVideo;

// 文件名
@property (nonatomic, copy) NSString *filename;

// 文件全路径
@property (nonatomic, copy) NSString *fullPath;

// 大小
@property (nonatomic, assign) NSUInteger size;

// 创建时间
@property (nonatomic, assign) NSUInteger createAt;

- (NSDictionary*)toMap;

@end

NS_ASSUME_NONNULL_END
