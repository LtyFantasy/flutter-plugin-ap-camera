//
//  RPRandomStickers.h
//  ap_camera
//
//  Created by wooplus on 2021/5/13.
//

#import <Foundation/Foundation.h>
#import "APGLStickerFilter.h"
#import "APGLStickerGifFilter.h"

NS_ASSUME_NONNULL_BEGIN

@interface RPRandomStickers : NSObject

@property (nonatomic, readonly) NSArray<APGLStickerFilter*> *filters;

// 初始化
- (instancetype)initWithViewportSize:(CGSize)size
                              emojis:(NSArray<NSString*>*)emojis;

// 在RandomStickers的串行线程中执行任务
- (void)excute:(void(^)(void))block;

// 资源加载初始化，同步
- (void)setup;
// 资源加载设置初始化，异步
- (void)setupWithComplete:(void(^)(void))block;

// 开始 Gif 轮播
- (void)emojiGifStart;

// 停止 Gif 轮播
- (void)emojiGifStop;

// 开启动画2
- (void)startAnimation2WithComplete:(void(^)(void))block;

// 停止动画2
- (void)stopAnimation2;

@end

NS_ASSUME_NONNULL_END
