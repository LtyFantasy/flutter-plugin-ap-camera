//
//  Camera.h
//  ap_camera
//
//  Created by wooplus on 2021/4/21.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <Flutter/Flutter.h>
#import "APGLFilter.h"
#import "APGLStickerFilter.h"

@class MLFaceDetectResult;

NS_ASSUME_NONNULL_BEGIN

/// 相机清晰度
typedef NS_ENUM(NSUInteger, CameraResolution) {
    
    CameraResolutionVeryLow = 0,
    CameraResolutionLow,
    CameraResolutionMedium,
    CameraResolutionHigh,
    CameraResolutionVeryHigh,
    CameraResolutionUltraHigh,
    CameraResolutionMax
};

// Flutter Position 转 VCaptureDevicePosition
AVCaptureDevicePosition cameraPositionForString(NSString * _Nullable position);

// VCaptureDevicePosition 转 Flutter Position
NSString* cameraPositionStringForPosition(AVCaptureDevicePosition position);

// Flutter Resolution 转 CameraResolution
CameraResolution cameraResolutionForString(NSString * _Nullable resolution);

/// 闪光模式
typedef NS_ENUM(NSUInteger, CameraFlashMode) {
    
    CameraFlashModeOff,
    CameraFlashModeAuto,
    CameraFlashModeAlways,
    CameraFlashModeTorch,
};

/// 曝光模式
typedef NS_ENUM(NSUInteger, CameraExposureMode) {
    CameraExposureModeAuto,
    CameraExposureModeLocked,
};

/// 聚焦模式
typedef NS_ENUM(NSUInteger, CameraFocusMode) {
    CameraFocusModeAuto,
    CameraFocusModeLocked,
};

@protocol CameraDelegate <NSObject>

- (void)didOutput:(CVPixelBufferRef)buffer sampleBuffer:(CMSampleBufferRef)sampleBuffer;

@end

@interface Camera : NSObject<FlutterTexture>

// 代理
@property (nonatomic, weak) id<CameraDelegate> delegate;

// 摄像头对应的Flutter Texture Id
@property (nonatomic, assign) NSInteger textureId;

// 回调 - Texture需要更新
@property (nonatomic, copy) void (^textureNeedUpdate) (NSInteger textureId);

// 摄像头图像大小
@property (nonatomic, assign, readonly) CGSize previewSize;

/**
 OpenGL视口大小
 
 注意，OpenGL视口大小必须贴近手机实际分辨率
 因为需要在视口中添加贴纸滤镜，如果视口是按照摄像头实际分辨率，比如640 x 480，则贴纸纹理会出现锯齿
 具体原因不多解释，自己推理就会明白
 */
@property (nonatomic, assign, readonly) CGSize viewPortSize;

/**
 创建摄像头对象
 */
- (instancetype)initWithDevice:(AVCaptureDevice*)device
                    resolution:(CameraResolution)resolution
                   enableAudio:(BOOL)enableAudio
                   orientation:(UIDeviceOrientation)orientation
                         error:(NSError**)error;

// 关闭
- (void)close;

// 开始显示
- (void)start;

// 停止显示
- (void)stop;

// 开始录像
- (BOOL)startRecord:(NSURL*)file
     progressUpdate:(void(^)(NSUInteger timeMS))block;

// 停止录像
- (void)stopRecordWithCompletionBlock:(void( ^ _Nullable)(NSString * _Nullable filepath, BOOL success))block;

// 取消录像
- (void)cancelRecord;

#pragma mark - 滤镜 & 贴纸

// 添加滤镜
- (void)addFilter:(APGLFilter*)filter;

// 插入滤镜
- (void)insertFilter:(APGLFilter *)filter atIndex:(NSUInteger)index;

// 移除滤镜
- (void)removeFilter:(APGLFilter*)filter;

// 移除所有滤镜
- (void)cleanFilter;

- (void)addSticker:(APGLStickerFilter*)sticker;
- (void)insertSticker:(APGLStickerFilter*)sticker atIndex:(NSUInteger)index;
- (void)removeSticker:(APGLStickerFilter*)sticker;
- (void)cleanSticker;

#pragma mark - AI 人脸识别

// 设置开启人脸识别
- (void)setFaceDetect:(BOOL)enable
      completionBlock:(void(^ _Nullable)(MLFaceDetectResult *result, CVPixelBufferRef buffer, CMTime time))block;

// 开启、关闭 人脸识别，二进制数据传输
- (void)setFaceDetectDataTransition:(BOOL)enable;

@end

NS_ASSUME_NONNULL_END
