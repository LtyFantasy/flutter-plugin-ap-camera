//
//  RPRandomStickers.m
//  ap_camera
//
//  Created by wooplus on 2021/5/13.
//

#import "RPRandomStickers.h"
#import "APGLStickerManager.h"
#import "APGLScaleAnimator.h"

@interface RPRandomStickers()

@property (nonatomic, assign) BOOL loadSuccess;

@property (nonatomic, strong) dispatch_queue_t queue;
@property (nonatomic, assign) CGSize viewportSize;
@property (nonatomic, strong) NSArray<APGLSticker*> *stickers;
@property (nonatomic, strong) NSArray<APGLStickerGifFilter*> *filters;

@end

@implementation RPRandomStickers

#pragma mark - Init

- (instancetype)initWithViewportSize:(CGSize)size emojis:(NSArray<NSString*>*)emojis {
    
    if (self = [super init]) {
        
        _queue = dispatch_queue_create("com.aplus.plugin.camera.sticker", DISPATCH_QUEUE_SERIAL);
        _viewportSize = size;
        // 生成贴纸组
        [self generateSticker:emojis];
        // 生成贴纸滤镜
        [self generateFilters];
    }
    return self;
}

- (void)generateSticker:(NSArray<NSString*>*)emojis {
    
    NSMutableArray *stickers = [NSMutableArray array];
    for (NSString *name in emojis) {
        for (APGLSticker *s in APGLStickerManager.instance.stickers) {
            if ([s.name isEqualToString:name]) {
                [stickers addObject:s];
            }
        }
    }
    _stickers = stickers;
}

- (void)generateFilters {
    
    CGFloat scale = _viewportSize.width / UIScreen.mainScreen.bounds.size.width;
    
    CGFloat marginT1 = 78 * scale;
    CGFloat marginT2 = 60 * scale;
    CGFloat marginLR = 54 * scale;
    CGFloat stickerSize = 40 * scale;
    CGFloat stickerInterval = (_viewportSize.width - (marginLR * 2) - (stickerSize * _stickers.count)) / (_stickers.count - 1);
    CGFloat startX = marginLR;
    
    NSMutableArray *filters = [NSMutableArray arrayWithCapacity:_stickers.count];
    for (int i = 0; i < _stickers.count; i++) {
        
        APGLSticker *sticker = _stickers[i];
        APGLStickerGifFilter *filter = [APGLStickerGifFilter filterWithSticker:sticker];
        
        if (i == 1 || i == 2) {
            filter.stickerFrame = CGRectMake(startX, marginT2, stickerSize, stickerSize);
        }
        else {
            filter.stickerFrame = CGRectMake(startX, marginT1, stickerSize, stickerSize);
        }
        [filters addObject:filter];
        startX += stickerSize + stickerInterval;
    }
    _filters = filters;
}

#pragma mark - Resource

- (void)setup {
 
    if (_loadSuccess) {
        return;
    }
   
    for (APGLSticker *s in _stickers) {
        [s loadResource];
    }
    _loadSuccess = YES;
}

- (void)setupWithComplete:(void (^)(void))block {
    
    __weak typeof(self) weakSelf = self;
    dispatch_async(_queue, ^{
        
        if (weakSelf.loadSuccess) {
            if (block) block();
            return;
        }
       
        __weak typeof(self) strongSelf = weakSelf;
        for (APGLSticker *s in strongSelf.stickers) {
            [s loadResource];
        }
        
        weakSelf.loadSuccess = YES;
        
        if (block) {
            block();
        }
    });
}

#pragma mark - Excute

- (void)excute:(void(^)(void))block {
    
    dispatch_async(_queue, ^{
        if (block) {
            block();
        }
    });
}

#pragma mark - Animation

- (void)emojiGifStart {
    
    __weak typeof(self) weakSelf = self;
    dispatch_async(_queue, ^{
       
        for (APGLStickerGifFilter *f in weakSelf.filters) {
            [f startWithRepeat:YES complete:nil];
        }
    });
}

- (void)emojiGifStop {
    
    __weak typeof(self) weakSelf = self;
    dispatch_async(_queue, ^{
       
        for (APGLStickerGifFilter *f in weakSelf.filters) {
            [f stop];
        }
    });
}

- (void)startAnimation2WithComplete:(void(^)(void))block {

    [self startAnimation2WithIndex:0 complete:block];
}

- (void)startAnimation2WithIndex:(NSUInteger)index complete:(void(^)(void))block {
    
    if (index >= _filters.count) {
        if (block) {
            block();
        }
        return;
    }
    
    __weak typeof(self) weakSelf = self;
    APGLStickerGifFilter *filter = _filters[index];
    
    // 放大
    filter.animator = [APGLScaleAnimator animatorWithDurationMS:200 scale:1.75];
    filter.animator.curve = APGLAnimationCurveEasyInOut;
    [filter.animator startAnimationWithCompleteBlock:^{
       // 播放Gif
        [filter startWithRepeat:NO complete:^{
            // 缩小
            [filter.animator reverseAnimationWithCompleteBlock:^{
                [weakSelf startAnimation2WithIndex:index+1 complete:block];
            }];
        }];
    }];
}

- (void)stopAnimation2 {
    
    __weak typeof(self) weakSelf = self;
    dispatch_async(_queue, ^{
       
        for (APGLStickerGifFilter *f in weakSelf.filters) {
            [f.animator cancelAnimation];
            [f stop];
        }
    });
}

@end
