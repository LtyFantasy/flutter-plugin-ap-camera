//
//  CameraManager.m
//  ap_camera
//
//  Created by wooplus on 2021/4/21.
//

#import "CameraManager.h"
#import "ApCameraDefines.h"
#import "CameraFile.h"

#import "APGLBeautyFilter.h"
#import "RPRandomStickers.h"
#import "MLFaceDetect.h"
#import "APMediaComposer.h"
#import "APImageDataTools.h"

#import <Accelerate/Accelerate.h>
#import <CoreMotion/CoreMotion.h>
#import <libkern/OSAtomic.h>
#import <uuid/uuid.h>

@implementation MLFaceDetectTakePhotoResult

- (NSDictionary *)toMap {
    
    NSMutableDictionary *map = [NSMutableDictionary dictionary];
    if (_errorCode) {
        map[@"errorCode"] = @(_errorCode);
        map[@"errorMsg"] = _errorMsg ?: @"";
    }
    
    if (_action) {
        map[@"action"] = @(_action);
    }
    
    map[@"timestampMS"] = @(_timestampMS);
    
    if (_photoPath) {
        map[@"photoPath"] = _photoPath;
    }
    
    return map;
}

@end

#pragma mark - 摄像头功能管理器

@interface CameraManager () <CameraDelegate>

// 当前激活的摄像头
@property (nonatomic, strong) Camera *camera;

/// 拍照回调Block
@property (nonatomic, copy) void(^takePhotoCallback) (NSString*, BOOL);

// 美颜滤镜开关
@property (nonatomic, assign) BOOL beautyEnable;

// 美颜滤镜
@property (nonatomic, strong) APGLBeautyFilter *beautyFilter;

// 随机表情组
@property (nonatomic, strong) RPRandomStickers *randomStickers;

// 人脸抓拍，是否保存成图片
@property (nonatomic, assign) BOOL faceCaptureSavePhoto;


// 人脸识别抓拍回调
@property (nonatomic, copy) void (^faceDetectTakePhotoBlock)(MLFaceDetectTakePhotoResult *);

// 抓拍标记 - 微笑
@property (nonatomic, assign) BOOL faceCaptureSmile;

// 抓拍标记 - 闭眼
@property (nonatomic, assign) BOOL faceCaptureEyeClose;

@end

@implementation CameraManager

#pragma mark - Init

+ (instancetype)instance {
    
    static CameraManager *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [CameraManager new];
    });
    
    return instance;
}

- (instancetype)init {
    
    if (self = [super init]) {
        
    }
    return self;
}

#pragma mark - 摄像头

// 获取设备位置列表
+ (NSArray<NSString*>*)getPositionDevices {
    
    NSMutableArray *array = [NSMutableArray array];
    NSArray<AVCaptureDevice*> *devices = [self getDevices];
    for (AVCaptureDevice *device in devices) {
        NSString *value = cameraPositionStringForPosition(device.position);
        [array addObject:value];
    }
    return array;
}

/**
 获取系统相机设备列表
 */
+ (NSArray<AVCaptureDevice*>*)getDevices {
    
    AVCaptureDeviceDiscoverySession *discoverySession = [AVCaptureDeviceDiscoverySession discoverySessionWithDeviceTypes:@[AVCaptureDeviceTypeBuiltInWideAngleCamera] mediaType:AVMediaTypeVideo position:AVCaptureDevicePositionUnspecified];
    return discoverySession.devices;
}

- (Camera *)createCameraWithPosition:(AVCaptureDevicePosition)position
                         enableAudio:(BOOL)enableAudio
                          resolution:(CameraResolution)resolution
                               error:(NSError *__autoreleasing *)error {
    
    AVCaptureDevice *target;
    NSArray<AVCaptureDevice*> *devices = [CameraManager getDevices];
    for (AVCaptureDevice *device in devices) {
        if (device.position == position) {
            target = device;
        }
    }
    
    if (!target) {
        *error = [NSError errorWithDomain:@"Can not find camera" code:-1 userInfo:nil];
        return nil;
    }
    
    _camera = [[Camera alloc] initWithDevice:target
                                  resolution:resolution
                                 enableAudio:enableAudio
                                 orientation:[UIDevice currentDevice].orientation
                                       error:error];
    _camera.delegate = self;
    return _camera;
}

- (void)startCameraWithTextureUpdate:(void (^)(NSInteger))block {
    
    [_camera start];
    _camera.textureNeedUpdate = block;
    _beautyFilter = [APGLBeautyFilter filter];
}

- (void)resumeCamera {
    [_camera start];
}

- (void)pauseCamera {
    [_camera stop];
}

- (void)releaseCamera {

    [_camera close];
    _camera = nil;
    _beautyEnable = NO;
    _beautyFilter = nil;
    _randomStickers = nil;
}

#pragma mark - 视频 & 图片

- (void)takePhotoWithCompletionBlock:(void (^)(NSString *, BOOL))block {
    
    _takePhotoCallback = block;
}

- (BOOL)startVideoRecordWithProgressUpdate:(void(^)(NSUInteger timeMS))block {
    
    NSString *file = [self getTempVideoFile];
    if (!file) {
        return NO;
    }
    return [_camera startRecord:[NSURL fileURLWithPath:file] progressUpdate:block];
}

- (void)stopVideoRecordWithCompletionBlock:(void (^)(NSString *, BOOL))block {
    
    [_camera stopRecordWithCompletionBlock:block];
}

- (void)cancelVideoRecord {
    
    [_camera cancelRecord];
}

- (void)startComposeVideo:(NSString*)videoFile
                    audio:(NSString*)audioFile
            completeBlock:(void(^)(BOOL success, NSString *outputFile, NSError *error))block {
    
    NSString *tempFile = [self getTempVideoFile];
    [APMediaComposer editVideoSynthesizeVieoPath:[NSURL fileURLWithPath:videoFile]
                                         BGMPath:[NSURL fileURLWithPath:audioFile]
                                      outputPath:[NSURL fileURLWithPath:tempFile]
                               needOriginalVoice:NO
                                     videoVolume:0
                                       BGMVolume:1
                                      eventBlock:^ (AVAssetExportSessionStatus status, NSError  * _Nullable error) {
        if (block) {
            block(status == AVAssetExportSessionStatusCompleted, tempFile, error);
        }
    }];
}

#pragma mark - 文件

- (NSString*)getPhotoCachePath {
 
    NSString *docDir = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *fileDir = [[docDir stringByAppendingPathComponent:@"ap_camera"] stringByAppendingPathComponent:@"photo"];
    return fileDir;
}

- (NSString*)getTempPhotoFile {
    
    NSString *dir = [self getPhotoCachePath];
    NSString *fileName = [@"pic_" stringByAppendingString:[[NSUUID UUID] UUIDString]];
    NSString *file = [[dir stringByAppendingPathComponent:fileName] stringByAppendingPathExtension:@"jpeg"];
    
    NSFileManager *fm = [NSFileManager defaultManager];
    // 目录不存在则创建
    if (![fm fileExistsAtPath:dir]) {
        
        NSError *error;
        [[NSFileManager defaultManager] createDirectoryAtPath:dir
                                  withIntermediateDirectories:true
                                                   attributes:nil
                                                        error:&error];
        if (error) {
            return nil;
        }
    }
    return file;
}

- (NSArray<NSDictionary *> *)getPhotoFiles {
    
    NSString *dir = [self getPhotoCachePath];
    NSFileManager *fm = [NSFileManager defaultManager];
    if (![fm fileExistsAtPath:dir]) {
        return @[];
    }
    
    // 获取目录下所有文件
    NSError *error;
    NSArray<NSString*> *contentArray = [fm contentsOfDirectoryAtPath:dir error:&error];
    if (error || !contentArray) {
        return @[];
    }
    
    NSMutableArray *array = [NSMutableArray array];
    for (NSString *subfile in contentArray) {
        
        BOOL isDir, isExist;
        NSString *fullPath = [dir stringByAppendingPathComponent:subfile];
        isExist = [fm fileExistsAtPath:fullPath isDirectory:&isDir];
        // 存在，且是文件
        if (isExist && !isDir) {
            
            NSError *error;
            // 获取文件属性
            NSDictionary *attr = [fm attributesOfItemAtPath:fullPath error:&error];
            if (error) {
                continue;
            }
            
            NSDate *time = attr[NSFileCreationDate];
            NSInteger size = [attr[NSFileSize] integerValue];
            
            CameraFile *file = [CameraFile new];
            file.isVideo = NO;
            file.filename = subfile;
            file.fullPath = fullPath;
            file.size = size;
            file.createAt = time.timeIntervalSince1970 * 1000;
            [array addObject:[file toMap]];
        }
    }
    return array;
}

- (NSString*)getVideoCachePath {
 
    NSString *docDir = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *fileDir = [[docDir stringByAppendingPathComponent:@"ap_camera"] stringByAppendingPathComponent:@"video"];
    return fileDir;
}

- (NSString*)getTempVideoFile {
    
    NSString *dir = [self getVideoCachePath];
    NSString *fileName = [@"video_" stringByAppendingString:[[NSUUID UUID] UUIDString]];
    NSString *file = [[dir stringByAppendingPathComponent:fileName] stringByAppendingPathExtension:@"mp4"];
    
    NSFileManager *fm = [NSFileManager defaultManager];
    // 目录不存在则创建
    if (![fm fileExistsAtPath:dir]) {
        
        NSError *error;
        [[NSFileManager defaultManager] createDirectoryAtPath:dir
                                  withIntermediateDirectories:true
                                                   attributes:nil
                                                        error:&error];
        if (error) {
            return nil;
        }
    }
    return file;
}

- (NSArray<NSDictionary *> *)getVideoFiles {
    
    NSString *dir = [self getVideoCachePath];
    NSFileManager *fm = [NSFileManager defaultManager];
    if (![fm fileExistsAtPath:dir]) {
        return @[];
    }
    
    // 获取目录下所有文件
    NSError *error;
    NSArray<NSString*> *contentArray = [fm contentsOfDirectoryAtPath:dir error:&error];
    if (error || !contentArray) {
        return @[];
    }
    
    NSMutableArray *array = [NSMutableArray array];
    for (NSString *subfile in contentArray) {
        
        BOOL isDir, isExist;
        NSString *fullPath = [dir stringByAppendingPathComponent:subfile];
        isExist = [fm fileExistsAtPath:fullPath isDirectory:&isDir];
        // 存在，且是文件
        if (isExist && !isDir) {
            
            NSError *error;
            // 获取文件属性
            NSDictionary *attr = [fm attributesOfItemAtPath:fullPath error:&error];
            if (error) {
                continue;
            }
            
            NSDate *time = attr[NSFileCreationDate];
            NSInteger size = [attr[NSFileSize] integerValue];
            
            CameraFile *file = [CameraFile new];
            file.isVideo = YES;
            file.filename = subfile;
            file.fullPath = fullPath;
            file.size = size;
            file.createAt = time.timeIntervalSince1970 * 1000;
            [array addObject:[file toMap]];
        }
    }
    return array;
}

#pragma mark - 缓存设置

- (void)cleanAllCache {
    
    @try {
        NSFileManager *manager = [NSFileManager defaultManager];
        NSString *photoDir = [self getPhotoCachePath];
        NSDirectoryEnumerator *enumerator = [manager enumeratorAtPath:photoDir];
        for (NSString *fileName in enumerator) {
            [manager removeItemAtPath:[photoDir stringByAppendingPathComponent:fileName] error:nil];
        }
        
        NSString *videoDir = [self getVideoCachePath];
        enumerator = [manager enumeratorAtPath:videoDir];
        for (NSString *fileName in enumerator) {
            [manager removeItemAtPath:[videoDir stringByAppendingPathComponent:fileName] error:nil];
        }
    } @catch (NSException *exception) {
        NSLog(@"[CameraManager] clean all cache error: %@", exception);
    }
}

#pragma mark - 滤镜设置

- (void)setEnableBeautyFilter:(BOOL)enable {
    
    if (_beautyEnable == enable) return;
    _beautyEnable = enable;
    if (enable) {
        [_camera addFilter:_beautyFilter];
    }
    else {
        [_camera removeFilter:_beautyFilter];
    }
}

- (void)adjustBright:(CGFloat)bright beauty:(CGFloat)beauty tone:(CGFloat)tone {
    [_beautyFilter adjustBright:bright beauty:beauty tone:tone];
}

#pragma mark - 贴纸设置

- (void)useEmojis:(NSArray<NSString *> *)emojis {
    
    _randomStickers = [[RPRandomStickers alloc] initWithViewportSize:_camera.viewPortSize
                                                              emojis:emojis];
    [_randomStickers setup];
}

- (void)setEmojiShow:(BOOL)enable {
    
    if (enable) {
        for (APGLStickerFilter *filter in _randomStickers.filters) {
            [_camera addSticker:filter];
        }
    }
    else {
        for (APGLStickerFilter *filter in _randomStickers.filters) {
            [_camera removeSticker:filter];
        }
    }
}

- (void)emojiGifStart {
    
    [_randomStickers emojiGifStart];
}

- (void)emojiGifStop {
    
    [_randomStickers emojiGifStop];
}

- (void)startEmojiAnimation2WithComplete:(void(^)(void))block {
 
    [_randomStickers startAnimation2WithComplete:block];
}

- (void)stopEmojiAnimation2 {
    
    [_randomStickers stopAnimation2];
}

#pragma mark - AI 识别

- (void)startFaceDetect:(void(^)(MLFaceDetectResult *))block {
    
    __weak typeof(self) weakSelf = self;
    [_camera setFaceDetect:YES completionBlock:^(MLFaceDetectResult * _Nonnull result, CVPixelBufferRef  _Nonnull buffer, CMTime time) {
        
        // 通知Flutter人脸识别结果
        if (block) {
            block(result);
        }
        
        __strong typeof(self) strongSelf = weakSelf;
        // 进行抓拍判断
        // 目前多张人脸只判断第一个
        if (!result.error && result.faces.count > 0 && strongSelf.faceDetectTakePhotoBlock) {
            
            MLFaceObject *face = result.faces.firstObject;
            
            // 微笑 >= 0.7
            if (!strongSelf.faceCaptureSmile && face.smile >= 0.7) {
                
                strongSelf.faceCaptureSmile = YES;
                
                NSError *error;
                NSString *file;
                
                if (strongSelf.faceCaptureSavePhoto) {
                    file = [strongSelf saveFacePhotoToTempFile:buffer error:&error];
                }
                
                MLFaceDetectTakePhotoResult *result = [MLFaceDetectTakePhotoResult new];
                result.action = 1;
                if (error) {
                    result.errorCode = ApCameraError_AIFace_SavePhotoFile;
                    result.errorMsg = error.description;
                }
                else {
                    result.photoPath = file;
                }
                
                if (CMTIME_IS_VALID(time)) {
                    result.timestampMS = time.value * 1000.f  / time.timescale;
                }
                
                strongSelf.faceDetectTakePhotoBlock(result);
            }
            
            // 左右眼任意一只眼睛闭眼 <= 0.1
            if (!strongSelf.faceCaptureEyeClose
                     && (face.leftEyeOpen <= 0.1 || face.rightEyeOpen <= 0.1)) {
                
                strongSelf.faceCaptureEyeClose = YES;
                
                NSError *error;
                NSString *file;
                
                if (strongSelf.faceCaptureSavePhoto) {
                    file = [strongSelf saveFacePhotoToTempFile:buffer error:&error];
                }
                
                MLFaceDetectTakePhotoResult *result = [MLFaceDetectTakePhotoResult new];
                if (face.leftEyeOpen <= 0.1 && face.rightEyeOpen > 0.1) {
                    result.action = 2;
                }
                else if (face.rightEyeOpen <= 0.1 && face.leftEyeOpen > 0.1) {
                    result.action = 3;
                }
                else {
                    result.action = 4;
                }
                
                if (error) {
                    result.errorCode = ApCameraError_AIFace_SavePhotoFile;
                    result.errorMsg = error.description;
                }
                else {
                    result.photoPath = file;
                }
                
                if (CMTIME_IS_VALID(time)) {
                    result.timestampMS = time.value * 1000.f / time.timescale;
                }
                
                strongSelf.faceDetectTakePhotoBlock(result);
            }
            
            // 微笑和眨眼都抓拍过了，就停止
            if (strongSelf.faceCaptureSmile && strongSelf.faceCaptureEyeClose) {
                [strongSelf stopFaceDetectTakePhoto];
            }
        }
    }];
}

- (void)stopFaceDetect {
    
    [self stopFaceDetectTakePhoto];
    [_camera setFaceDetect:NO completionBlock:nil];
}

- (void)startFaceDetectTakePhoto:(BOOL)savePhoto block:(void (^)(MLFaceDetectTakePhotoResult *))block {
    _faceCaptureSavePhoto = savePhoto;
    _faceDetectTakePhotoBlock = block;
    _faceCaptureSmile = NO;
    _faceCaptureEyeClose = NO;
}

- (void)stopFaceDetectTakePhoto {
    _faceCaptureSavePhoto = NO;
    _faceDetectTakePhotoBlock = nil;
    _faceCaptureSmile = NO;
    _faceCaptureEyeClose = NO;
}

- (void)enableFaceDetectDataTransition:(BOOL)enable {
    [_camera setFaceDetectDataTransition:enable];
}

- (NSString*)saveFacePhotoToTempFile:(CVPixelBufferRef)buffer error:(NSError**)error {
    
    NSString *file = [self getTempPhotoFile];
    UIImage *image = [APImageDataTools imageWithPixelBuffer:buffer];
    NSData *data = UIImageJPEGRepresentation(image, 1);
    BOOL success = [data writeToFile:file options:NSDataWritingAtomic error:error];
    if (!success) {
        return nil;
    }
    return file;
}

#pragma mark - CameraDelegate

- (void)didOutput:(CVPixelBufferRef)buffer sampleBuffer:(CMSampleBufferRef)sampleBuffer {
 
    // 拍照截图
    if (_takePhotoCallback) {
        NSError *error;
        NSString *file = [self saveFacePhotoToTempFile:buffer error:&error];
        _takePhotoCallback(file, error == nil);
        _takePhotoCallback = nil;
    }
}

@end

