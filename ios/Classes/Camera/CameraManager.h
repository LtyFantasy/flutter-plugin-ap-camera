//
//  CameraManager.h
//  ap_camera
//
//  Created by wooplus on 2021/4/21.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

#import "Camera.h"

typedef NS_ENUM(NSUInteger, MLFaceAction) {
    MLFaceActionSmile = 1,
    MLFaceActionLeftEyeClose = 2,
    MLFaceActionRightEyeClose = 3,
    MLFaceActionBothEyeClose = 4,
};

@interface MLFaceDetectTakePhotoResult : NSObject

@property (nonatomic, assign) NSUInteger errorCode;
@property (nonatomic, copy) NSString *errorMsg;
@property (nonatomic, assign) MLFaceAction action;
@property (nonatomic, assign) NSUInteger timestampMS;
@property (nonatomic, copy) NSString *photoPath;

- (NSDictionary*)toMap;

@end

@interface CameraManager : NSObject

// 当前激活的摄像头
@property (nonatomic, readonly) Camera *camera;

// 单例
+ (instancetype)instance;

#pragma mark - 摄像头

// 获取设备位置列表，用来告知Flutter有哪些位置的设备
+ (NSArray<NSString*>*)getPositionDevices;

// 创建并激活 指定摄像头
- (Camera*)createCameraWithPosition:(AVCaptureDevicePosition)position
                        enableAudio:(BOOL)enableAudio
                         resolution:(CameraResolution)resolution
                              error:(NSError**)error;

// 释放摄像头
- (void)releaseCamera;

// 开始视频流
- (void)startCameraWithTextureUpdate:(void(^)(NSInteger textureId))block;

// 恢复视频流
- (void)resumeCamera;

// 暂停视频流
- (void)pauseCamera;

#pragma mark - 视频 & 图片

// 抓拍照片
- (void)takePhotoWithCompletionBlock:(void(^)(NSString *filepath, BOOL success))block;

// 开始录制视频
- (BOOL)startVideoRecordWithProgressUpdate:(void(^)(NSUInteger timeMS))block;

// 停止视频录制
- (void)stopVideoRecordWithCompletionBlock:(void(^)(NSString *filepath, BOOL success))block;

// 取消视频录制
- (void)cancelVideoRecord;

// 视频合成
- (void)startComposeVideo:(NSString*)videoFile
                    audio:(NSString*)audioFile
            completeBlock:(void(^)(BOOL success, NSString *outputFile, NSError *error))block;

#pragma mark - 文件

// 获取图片文件列表
- (NSArray<NSDictionary *> *)getPhotoFiles;

// 获取视频文件列表
- (NSArray<NSString*>*)getVideoFiles;

#pragma mark - 缓存

- (void)cleanAllCache;

#pragma mark - 滤镜设置

// 设置美颜滤镜开关
- (void)setEnableBeautyFilter:(BOOL)enable;

// 美颜参数
- (void)adjustBright:(CGFloat)bright beauty:(CGFloat)beauty tone:(CGFloat)tone;

#pragma mark - 贴纸设置

// 使用对应的Emoji(4个)
- (void)useEmojis:(NSArray<NSString*>*)emojis;

// 设置是否展示Emoji
- (void)setEmojiShow:(BOOL)enable;

// 展示Emoji Gif动画
- (void)emojiGifStart;

// 停止Emoji Gif动画
- (void)emojiGifStop;

// 开始Emoji认证轮播动画
- (void)startEmojiAnimation2WithComplete:(void(^)(void))block;

// 终止Emoji认证轮播动画
- (void)stopEmojiAnimation2;

#pragma mark - AI 识别

// 开启人脸识别
- (void)startFaceDetect:(void(^)(MLFaceDetectResult *result))block;

// 关闭人脸识别
- (void)stopFaceDetect;

// 开启人脸识别抓拍
- (void)startFaceDetectTakePhoto:(BOOL)savePhoto
                           block:(void (^)(MLFaceDetectTakePhotoResult *))block;

// 开关、关闭 人脸识别 图片二进制数据传输
- (void)enableFaceDetectDataTransition:(BOOL)enable;

@end
