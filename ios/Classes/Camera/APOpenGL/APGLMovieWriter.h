//
//  APGLMovieWriter.h
//  ap_camera-Shader
//
//  Created by wooplus on 2021/4/28.
//

#import <UIKit/UIKit.h>
#import <CoreVideo/CoreVideo.h>
#import <AVFoundation/AVFoundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol APGLMovieWriterDelegate <NSObject>

- (void)movieWriterFailedWithError:(NSError*)error;

@end

@interface APGLMovieWriter : NSObject

// 录制的起始时间
@property (nonatomic, readonly) CMTime startTime;

// 录制时间进度通知
@property (nonatomic, copy) void (^progressUpdate) (NSUInteger timeMS);

+ (instancetype)writerWithURL:(NSURL*)url
                         size:(CGSize)size
                     delegate:(id<APGLMovieWriterDelegate>)delegate;

- (void)handlePixelBuffer:(CVPixelBufferRef)buffer frameTime:(CMTime)frameTime;
- (void)stopWithCompletion:(void(^)(NSString *filepath, BOOL success))completionBlock;
- (void)cancel;

@end

NS_ASSUME_NONNULL_END
