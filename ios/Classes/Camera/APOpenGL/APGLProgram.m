//
//  APGLProgram.m
//  ap_camera-Shader
//
//  Created by wooplus on 2021/4/28.
//

#import "APGLProgram.h"
#import <OpenGLES/ES2/gl.h>

@interface APGLProgram ()

@property (nonatomic, assign) GLuint program;

@end

@implementation APGLProgram

#pragma mark - Init

+ (instancetype)programWithVertexShader:(NSString *)vertex fragmentShader:(NSString *)fragment {
    
    APGLProgram *obj = [[APGLProgram alloc] initWithVertex:vertex fragment:fragment];
    return obj;
}

- (instancetype)initWithVertex:(NSString*)vertext fragment:(NSString*)fragment {
    
    if (self = [super init]) {
        
        do {
            
            GLuint vertShader, fragShader;
            if (![self compileShader:&vertShader type:GL_VERTEX_SHADER string:vertext]) {
                NSLog(@"compile vertex shader failed");
                break;
            }
            
            if (![self compileShader:&fragShader type:GL_FRAGMENT_SHADER string:fragment]) {
                NSLog(@"compile fragment shader failed");
                break;
            }
            
            _program = glCreateProgram();
            glAttachShader(_program, vertShader);
            glAttachShader(_program, fragShader);
            
            // 连接
            if (![self linkProgram:_program]) {
                NSLog(@"link program failed");
                glDeleteShader(vertShader);
                glDeleteShader(fragShader);
                glDeleteProgram(_program);
                _program = 0;
                break;
            }
            
            // 连接完后就可以移除
            if (vertShader) {
                glDetachShader(_program, vertShader);
                glDeleteShader(vertShader);
                vertShader = 0;
            }
            
            if (fragShader) {
                glDetachShader(_program, fragShader);
                glDeleteShader(fragShader);
                fragShader = 0;
            }
            
            /*
            // 验证
            if (![self validateProgram:_program]) {
                NSLog(@"validate program failed");
                glDeleteProgram(_program);
                _program = 0;
            }*/
            
        } while (0);
    }
    return self;
}

- (BOOL)compileShader:(GLuint *)shader type:(GLenum)type string:(NSString *)string {
    
    GLint status;
    const GLchar *source;
    source = (GLchar *)[string UTF8String];
    if (!source) {
        return NO;
    }
    
    *shader = glCreateShader(type);
    glShaderSource(*shader, 1, &source, NULL);
    glCompileShader(*shader);
    
#ifdef DEBUG
    GLint logLength;
    glGetShaderiv(*shader, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0) {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetShaderInfoLog(*shader, logLength, &logLength, log);
        NSLog(@"Shader compile log:\n%s", log);
        free(log);
    }
#endif
    
    glGetShaderiv(*shader, GL_COMPILE_STATUS, &status);
    if (status == GL_FALSE) {
       glDeleteShader(*shader);
       return NO;
    }
    return YES;
}

- (BOOL)linkProgram:(GLuint)prog {
    
    GLint status;
    glLinkProgram(prog);
    glGetProgramiv(prog, GL_LINK_STATUS, &status);
    if (status == GL_FALSE) {
       return NO;
    }
    return YES;
}

- (BOOL)validateProgram:(GLuint)prog {
    
    GLint status;
    glValidateProgram(prog);
    
#ifdef DEBUG
    GLint logLength;
    glGetProgramiv(prog, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0) {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetProgramInfoLog(prog, logLength, &logLength, log);
        NSLog(@"program validate log : \n%s", log);
        free(log);
    }
#endif
    
    glGetProgramiv(prog, GL_VALIDATE_STATUS, &status);
    if (status == GL_FALSE) {
        return NO;
    }
    return YES;
}

#pragma mark - OpenGL Operation

- (GLuint)uniformIndex:(NSString *)uniName {
    return glGetUniformLocation(_program, [uniName UTF8String]);
}

- (GLuint)attributeIndex:(NSString *)attrName {
    return glGetAttribLocation(_program, [attrName UTF8String]);
}

- (void)useProgram {
    glUseProgram(_program);
}

- (void)clean {
    glDeleteProgram(_program);
    _program = 0;
}

@end
