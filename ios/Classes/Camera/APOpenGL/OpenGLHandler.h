//
//  OpenGLHandler.h
//  ap_camera
//
//  Created by wooplus on 2021/4/27.
//

#import <Foundation/Foundation.h>
#import <OpenGLES/EAGL.h>
#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/ES2/glext.h>
#import <AVFoundation/AVFoundation.h>

#import "APGLFilter.h"
#import "APGLStickerFilter.h"

//NS_ASSUME_NONNULL_BEGIN

@protocol OpenGLHandlerDelegate <NSObject>

- (void)ouputVideoPixelBuffer:(CVPixelBufferRef)buffer sampleBuffer:(CMSampleBufferRef)sampleBuffer;

@end

@interface OpenGLHandler : NSObject

@property (nonatomic, weak) id<OpenGLHandlerDelegate> delegate;

@property (nonatomic, readonly) NSArray<APGLFilter*> *filters;
@property (nonatomic, readonly) NSArray<APGLStickerFilter*> *stickers;

- (instancetype)initWithVideoSize:(CGSize)size;
- (void)handleSampleBuffer:(CMSampleBufferRef)sampleBuffer;
- (void)close;

- (void)addFilter:(APGLFilter*)filter;
- (void)insertFilter:(APGLFilter*)filter atIndex:(NSUInteger)index;
- (void)removeFilter:(APGLFilter*)filter;
- (void)cleanFilter;

- (void)addSticker:(APGLStickerFilter*)sticker;
- (void)insertSticker:(APGLStickerFilter*)sticker atIndex:(NSUInteger)index;
- (void)removeSticker:(APGLStickerFilter*)sticker;
- (void)cleanSticker;

@end

//NS_ASSUME_NONNULL_END
