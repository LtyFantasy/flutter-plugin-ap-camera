//
//  APGLMovieWriter.m
//  ap_camera-Shader
//
//  Created by wooplus on 2021/4/28.
//

#import "APGLMovieWriter.h"
#import <OpenGLES/EAGL.h>
#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/ES2/glext.h>

@interface APGLMovieWriter ()

@property (nonatomic, weak) id<APGLMovieWriterDelegate> delegate;
@property (nonatomic, strong) NSURL *url;
@property (nonatomic, assign) CGSize videoSize;

@property (nonatomic, strong) AVAssetWriter *writer;
@property (nonatomic, strong) AVAssetWriterInput *writerInput;
@property (nonatomic, strong) AVAssetWriterInputPixelBufferAdaptor *writerInputPixelBufferAdaptor;

@property (nonatomic, assign) CMTime startTime;
@property (nonatomic, assign) CMTime previousFrameTime;

@end

@implementation APGLMovieWriter

#pragma mark - Init

+ (instancetype)writerWithURL:(NSURL *)url size:(CGSize)size delegate:(id<APGLMovieWriterDelegate>)delegate {
    APGLMovieWriter *obj = [[APGLMovieWriter alloc] initWithURL:url size:size delegate:delegate];
    return obj;
}

- (instancetype)initWithURL:(NSURL *)url size:(CGSize)size delegate:(id<APGLMovieWriterDelegate>)delegate {
    if (self = [super init]) {
        _delegate = delegate;
        _url = url;
        _videoSize = size;
        [self setupAssetWriter:url];
    }
    return self;
}

- (BOOL)setupAssetWriter:(NSURL*)url {
    
    NSError *error;
    _writer = [[AVAssetWriter alloc] initWithURL:url fileType:AVFileTypeMPEG4 error:&error];
    if (error) {
        if (_delegate && [_delegate respondsToSelector:@selector(movieWriterFailedWithError:)]) {
            [_delegate movieWriterFailedWithError:error];
        }
        return NO;
    }
    
    _writer.movieFragmentInterval = kCMTimeInvalid;
    _writerInput = [[AVAssetWriterInput alloc] initWithMediaType:AVMediaTypeVideo
                                                  outputSettings:[self videoSetting]];
    _writerInput.expectsMediaDataInRealTime = YES;
    
    // 输入源是BGRA格式
    NSDictionary *sourceAttr = @{
        (__bridge NSString*)kCVPixelBufferPixelFormatTypeKey : @(kCVPixelFormatType_32BGRA),
        (__bridge NSString*)kCVPixelBufferWidthKey: @(_videoSize.width),
        (__bridge NSString*)kCVPixelBufferHeightKey: @(_videoSize.height)
    };
    _writerInputPixelBufferAdaptor = [[AVAssetWriterInputPixelBufferAdaptor alloc] initWithAssetWriterInput:_writerInput
                                                                                sourcePixelBufferAttributes:sourceAttr];
    
    [_writer addInput:_writerInput];
    return YES;
}

- (NSDictionary*)videoSetting {
    
    //写入视频大小
    NSInteger numPixels = _videoSize.width * _videoSize.height;
    //每像素比特
    CGFloat bitsPerPixel = 3.0;
    NSInteger bitsPerSecond = numPixels * bitsPerPixel;
    
    NSDictionary *compressionProperties = @{ AVVideoAverageBitRateKey : @(bitsPerSecond),
                                             AVVideoExpectedSourceFrameRateKey : @(30),
                                             AVVideoMaxKeyFrameIntervalKey : @(90),
                                             AVVideoProfileLevelKey : AVVideoProfileLevelH264BaselineAutoLevel };
    
    NSDictionary *videoCompressionSettings = @{ AVVideoCodecKey : AVVideoCodecTypeH264,
                                                AVVideoScalingModeKey : AVVideoScalingModeResizeAspectFill,
                                                AVVideoWidthKey : @(_videoSize.width),
                                                AVVideoHeightKey : @(_videoSize.height),
                                                AVVideoCompressionPropertiesKey : compressionProperties };
    
    return videoCompressionSettings;
}

#pragma mark - Operation

- (void)stopWithCompletion:(void(^)(NSString *filepath, BOOL success))completionBlock {
    NSThread *current = [NSThread currentThread];
    __weak typeof(self) weakSelf = self;
    if (weakSelf.writer.status == AVAssetWriterStatusUnknown
        || weakSelf.writer.status == AVAssetWriterStatusCancelled
        || weakSelf.writer.status == AVAssetWriterStatusCompleted) {
        if (completionBlock) {
            completionBlock(NULL, NO);
        }
        return;
    }
    
    if (weakSelf.writer.status == AVAssetWriterStatusWriting) {
        [weakSelf.writerInput markAsFinished];
    }
    
    [weakSelf.writer finishWritingWithCompletionHandler:^{
        if (completionBlock) {
            NSDictionary *params = @{@"success": @(YES), @"file": weakSelf.url.path, @"completion":completionBlock};
            [weakSelf performSelector:@selector(performStopCompletion:) onThread:current withObject:params waitUntilDone:NO];
        }
    }];
}



- (void)performStopCompletion:(NSDictionary *)params{
    void (^callback)(NSString *filepath, BOOL success)  = params[@"completion"];
    bool success = [params[@"success"] boolValue];
    NSString *filePath = params[@"file"];
    callback(filePath, success);
}


- (void)cancel {
    __weak typeof(self) weakSelf = self;
    if (weakSelf.writer.status == AVAssetWriterStatusUnknown
        || weakSelf.writer.status == AVAssetWriterStatusCancelled
        || weakSelf.writer.status == AVAssetWriterStatusCompleted) {
        return;
    }
    
    if (weakSelf.writer.status == AVAssetWriterStatusWriting) {
        [weakSelf.writerInput markAsFinished];
    }
    [weakSelf.writer cancelWriting];
}

#pragma mark - Frame Handle

- (void)handlePixelBuffer:(CVPixelBufferRef)buffer frameTime:(CMTime)frameTime {
    __weak typeof(self) weakSelf = self;
    // 检查是否已经开始录制
    if (CMTIME_IS_INVALID(weakSelf.startTime)) {
        [weakSelf.writer startWriting];
        [weakSelf.writer startSessionAtSourceTime:frameTime];
        weakSelf.startTime = frameTime;
    }
    
    CVPixelBufferLockBaseAddress(buffer, 0);
    
    if (weakSelf.writerInput.readyForMoreMediaData) {
        
        if (weakSelf.writer.status == AVAssetWriterStatusWriting) {
            BOOL success = [weakSelf.writerInputPixelBufferAdaptor appendPixelBuffer:buffer
                                                                withPresentationTime:frameTime];
            if (!success) {
                NSLog(@"write frame failed");
            }
            
            if (weakSelf.progressUpdate) {
                CMTime deltaTime = CMTimeSubtract(frameTime, weakSelf.startTime);
                NSInteger timeMs = deltaTime.value * 1000 / deltaTime.timescale;
                weakSelf.progressUpdate(timeMs);
            }
        }
#ifdef DEBUG
        else {
            NSLog(@"status != AVAssetWriterStatusWriting");
        }
#endif
    }
#ifdef DEBUG
    else {
        NSLog(@"readyForMoreMediaData false");
    }
#endif
    
    CVPixelBufferUnlockBaseAddress(buffer, 0);
    weakSelf.previousFrameTime = frameTime;
}

@end
