//
//  APGLSticker.h
//  ap_camera
//
//  Created by wooplus on 2021/5/12.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface APGLSticker : NSObject

// 是否已加载资源
@property (nonatomic, readonly) BOOL loaded;

// 贴纸名
@property (nonatomic, readonly) NSString *name;

// 文件路径
@property (nonatomic, readonly) NSString *file;

// 是否是Gif动图
@property (nonatomic, readonly) BOOL isGif;

// 该贴纸展示的周期时间 ms
// 如果是静态图，该值为0，如果是Gif图，则为其实际展示周期
@property (nonatomic, readonly) NSUInteger duration;

// 该贴纸的每一帧图，注意，需要加载资源后才有值
@property (nonatomic, readonly) NSArray<UIImage*> *frameImages;

// 该贴纸每帧间隔 ms，注意，需要加载资源后才有
// 如果是静态图，该值为0
@property (nonatomic, readonly) NSUInteger frameInterval;

/**
 初始化
 
 @param name 贴纸名
 @param file Gif贴纸全路径
 @param duration 该贴纸的周期时间 ms，如果是Gif图，需要指定该值
 */
- (instancetype)initWithName:(NSString*)name
                        file:(NSString*)file
                    duration:(NSUInteger)duration;

// 从文件加载Gif资源
- (void)loadResource;

// 释放资源
- (void)releaseResource;

@end

NS_ASSUME_NONNULL_END
