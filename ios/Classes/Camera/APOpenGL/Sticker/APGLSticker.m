//
//  APGLSticker.m
//  ap_camera
//
//  Created by wooplus on 2021/5/12.
//

#import "APGLSticker.h"

@interface APGLSticker()


@property (nonatomic, assign) BOOL loaded;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *file;
@property (nonatomic, assign) NSUInteger duration;
@property (nonatomic, strong) NSArray<UIImage*> *frameImages;
@property (nonatomic, assign) NSUInteger frameInterval;

@end

@implementation APGLSticker

- (instancetype)initWithName:(NSString*)name
                        file:(NSString*)file
                    duration:(NSUInteger)duration {
    
    if (self = [super init]) {
        
        _name = name;
        _file = file;
        _duration = duration;
        _loaded = NO;
        _frameImages = @[];
    }
    return self;
}

- (void)loadResource {
    
    if (_loaded) return;
    
    // 加载资源，并读取其每一帧
    NSURL *url = [NSURL fileURLWithPath:_file];
    CGImageSourceRef source = CGImageSourceCreateWithURL((CFURLRef)url, NULL);
    size_t count = CGImageSourceGetCount(source);

    NSMutableArray *images = [NSMutableArray array];
    for (NSInteger i = 0; i < count; i++) {
        @autoreleasepool {
            CGImageRef imageRef = CGImageSourceCreateImageAtIndex(source, i, NULL);
            UIImage *image = [UIImage imageWithCGImage:imageRef];
            [images addObject:image];
            CGImageRelease(imageRef);
        }
    }
    
    CFRelease(source);
    _frameImages = images;
    if(_frameImages.count > 0) {
        _isGif = YES;
        _frameInterval = _duration / _frameImages.count;
    }
    else {
        _isGif = NO;
        _duration = 0;
        _frameInterval = 0;
    }
    
    _loaded = YES;
}

- (void)releaseResource {
    
    if (!_loaded) return;
    _frameImages = @[];
    _frameInterval = 0;
    _loaded = NO;
}


@end
