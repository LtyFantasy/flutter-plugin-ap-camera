//
//  APGLStickerManager.h
//  ap_camera
//
//  Created by wooplus on 2021/5/13.
//

#import <Foundation/Foundation.h>
#import "APGLSticker.h"

NS_ASSUME_NONNULL_BEGIN

@interface APGLStickerManager : NSObject

@property (nonatomic, readonly) NSArray<APGLSticker*> *stickers;

+ (instancetype)instance;

@end

NS_ASSUME_NONNULL_END
