//
//  APGLStickerManager.m
//  ap_camera
//
//  Created by wooplus on 2021/5/13.
//

#import "APGLStickerManager.h"

@interface APGLStickerManager()

// APSticker资源包
@property (nonatomic, strong) NSBundle *bundle;

// 配置文件
@property (nonatomic, strong) NSDictionary *config;

// 所有的Sticker
@property (nonatomic, strong) NSArray<APGLSticker*> *stickers;

@end

@implementation APGLStickerManager

#pragma mark - Init

+ (instancetype)instance {
    
    static APGLStickerManager *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [APGLStickerManager new];
    });
    return instance;
}

- (instancetype)init {
    
    if (self = [super init]) {
        
        // 解析资源包
        NSBundle *rootBundle = [NSBundle bundleForClass:[self class]];
        NSString *stickerPath = [rootBundle pathForResource:@"APSticker" ofType:@"bundle"];
        _bundle = [NSBundle bundleWithPath:stickerPath];
        
        NSString *configPath = [_bundle pathForResource:@"sticker" ofType:@"json"];
        
        NSError *error = nil;
        NSData *data = [NSData dataWithContentsOfFile:configPath];
        NSDictionary *configDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
        
        if (error || !configDict) {
            NSLog(@"APGLStickerManager load config error: %@", error);
        }
        else {
            
            _config = configDict;
            NSArray<NSDictionary*> *stickersConfig = _config[@"stickers"];
            NSMutableArray *stickers = [NSMutableArray array];
            for (NSDictionary *dict in stickersConfig) {
                
                @autoreleasepool {
                 
                    NSString *name = dict[@"name"];
                    NSString *file = dict[@"file"];
                    NSString *filepath = [_bundle pathForResource:file ofType:nil];
                    NSUInteger duration = [dict[@"duration"] unsignedIntegerValue];
                    if (name && filepath && duration) {
                        
                        APGLSticker *s = [[APGLSticker alloc] initWithName:name
                                                                      file:filepath
                                                                  duration:duration];
                        [stickers addObject:s];
                    }
#ifdef DEBUG
                    else {
                        NSLog(@"贴纸配置解析失败, file %@, path %@, duration %tu", file, filepath, duration);
                    }
#endif
                }
            }
            
            _stickers = stickers;
            
#ifdef DEBUG
            NSLog(@"APGLStickerManager 加载成功");
#endif
        }
    }
    return self;
}

@end
