//
//  APGLProgram.h
//  ap_camera-Shader
//
//  Created by wooplus on 2021/4/28.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface APGLProgram : NSObject

/**
 初始化Programe
 
 设置vertex和fragment的shader代码
 */
+ (instancetype)programWithVertexShader:(NSString*)vertex fragmentShader:(NSString*)fragment;

// 获取uniform变量
- (GLuint)uniformIndex:(NSString *)uniName;

// 获取attribute变量
- (GLuint)attributeIndex:(NSString *)attrName;

// 激活program
- (void)useProgram;

// 清理资源
- (void)clean;

@end

NS_ASSUME_NONNULL_END
