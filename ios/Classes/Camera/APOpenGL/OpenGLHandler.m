//
//  OpenGLHandler.m
//  ap_camera
//
//  Created by wooplus on 2021/4/27.
//

#import "OpenGLHandler.h"
#import "APGLDefines.h"
#import "APGLProgram.h"
#import "APGLEmptyFilter.h"

#import <CoreVideo/CoreVideo.h>

NSString * const kVertetShader = SHADER_STRING
(
 attribute vec4 a_Position;
 attribute vec2 a_TexCoordIn;
 varying vec2 v_TexCoordOut;

 void main(void) {
     
     gl_Position = a_Position;
     v_TexCoordOut = a_TexCoordIn;
 }
);

NSString * const kFragmentShader = SHADER_STRING
(
 precision mediump float;
 uniform sampler2D u_Texture;
 varying vec2 v_TexCoordOut;

 void main(void) {
     
     vec4 color = texture2D(u_Texture, v_TexCoordOut);
     gl_FragColor = color;
 }
);

@interface OpenGLHandler ()

@property (nonatomic, assign) CGSize size;

@property (nonatomic, strong) EAGLContext *context;
@property (nonatomic, assign) GLuint frameBuffer;
@property (nonatomic, assign) CVOpenGLESTextureCacheRef textureCacheRef;
@property (nonatomic, assign) CVOpenGLESTextureRef textureRef;
@property (nonatomic, assign) CVPixelBufferRef pixelBuffer;

@property (nonatomic, assign) GLuint texture;

@property (nonatomic, strong) APGLProgram *program;
@property (nonatomic, assign) GLuint pa_position;
@property (nonatomic, assign) GLuint pu_texture;
@property (nonatomic, assign) GLuint pa_textureCoor;

@property (nonatomic, strong) APGLEmptyFilter *emptyFilter;
@property (nonatomic, strong) NSMutableArray<APGLFilter*> *filters;
@property (nonatomic, strong) NSMutableArray<APGLStickerFilter*> *stickers;

@end

@implementation OpenGLHandler

#pragma mark - Init

- (instancetype)initWithVideoSize:(CGSize)size {
    
    if (self = [super init]) {
        
        _size = size;
        _filters = [NSMutableArray array];
        _stickers = [NSMutableArray array];
        [self setupContext];
        [self createBuffer];
        [self createProgram];
        
        _emptyFilter = [APGLEmptyFilter filter];
        [_emptyFilter setupWithSize:size];
        _texture = [self createTexture];
    }
    return self;
}

- (void)close {
    
    if (_pixelBuffer) {
        CFRelease(_pixelBuffer);
        _pixelBuffer = NULL;
    }
    
    if (_textureRef) {
        CFRelease(_textureRef);
        _textureRef = NULL;
    }
    
    if (_textureCacheRef) {
        CVOpenGLESTextureCacheFlush(_textureCacheRef, 0);
        CFRelease(_textureCacheRef);
        _textureCacheRef = NULL;
    }
    
    if (_frameBuffer) {
        glDeleteBuffers(1, &_frameBuffer);
        _frameBuffer = 0;
    }

    if (_program) {
        [_program clean];
        _program = nil;
    }
    
    if (_texture) {
        glDeleteTextures(1, &_texture);
        _texture = 0;
    }
    
    if ([EAGLContext currentContext] == _context) {
        [EAGLContext setCurrentContext:nil];
        _context = nil;
    }
}

- (void)setupContext {

    _context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES3];
    [EAGLContext setCurrentContext:_context];
}

- (void)createBuffer {
    
    [self createTextureCache];
    
    glGenFramebuffers(1, &_frameBuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, _frameBuffer);
    
    // 此处没有使用RenderBuffer，因为我们不需要显示到原生Layer上
    //glGenRenderbuffers(1, &_renderBuffer);
    //glBindFramebuffer(GL_RENDERBUFFER, _renderBuffer);

    // 绑定Render Buffer 到 Frame Buffer
    //glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, _renderBuffer);
    //[_context renderbufferStorage:GL_RENDERBUFFER fromDrawable:_eaglLayer];
    
    // 将纹理附加到帧缓冲区上
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, CVOpenGLESTextureGetName(_textureRef), 0);
    
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        NSLog(@"fbo status error %x", glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }
}

- (void)createTextureCache {
    
    CVReturn err = CVOpenGLESTextureCacheCreate(kCFAllocatorDefault, NULL, _context, NULL, &_textureCacheRef);
    if (err) {
        NSLog(@"CVOpenGLESTextureCacheCreate error %d", err);
        return;
    }
    
    CFDictionaryRef empty;
    CFMutableDictionaryRef attrs;
    empty = CFDictionaryCreate(kCFAllocatorDefault, NULL, NULL, 0, &kCFTypeDictionaryKeyCallBacks, &kCFTypeDictionaryValueCallBacks);
    attrs = CFDictionaryCreateMutable(kCFAllocatorDefault, 1, &kCFTypeDictionaryKeyCallBacks, &kCFTypeDictionaryValueCallBacks);
    
    CFDictionarySetValue(attrs, kCVPixelBufferIOSurfacePropertiesKey, empty);
    err = CVPixelBufferCreate(kCFAllocatorDefault, _size.width, _size.height, kCVPixelFormatType_32BGRA, attrs, &_pixelBuffer);
    if (err) {
        NSLog(@"CVPixelBufferCreate %d", err);
        err = 0;
    }
    
    err = CVOpenGLESTextureCacheCreateTextureFromImage(kCFAllocatorDefault, _textureCacheRef, _pixelBuffer, NULL, GL_TEXTURE_2D, GL_RGBA, _size.width, _size.height, GL_RGBA, GL_UNSIGNED_BYTE, 0, &_textureRef);
    if (err) {
        NSLog(@"CVOpenGLESTextureCacheCreateTextureFromImage %d", err);
        err = 0;
    }
    
    CFRelease(empty);
    CFRelease(attrs);
}

#pragma mark - Shader Program

- (void)createProgram {
    
    _program = [APGLProgram programWithVertexShader:kVertetShader
                                     fragmentShader:kFragmentShader];
    
    _pa_position = [_program attributeIndex:@"a_Position"];
    _pu_texture = [_program uniformIndex:@"u_Texture"];
    _pa_textureCoor = [_program attributeIndex:@"a_TexCoordIn"];
}


#pragma mark - Draw

- (void)drawTextureWithCMTime:(CMTime)time {
            
    if ([EAGLContext currentContext] != _context) {
        [EAGLContext setCurrentContext:_context];
    }
    
    glViewport(0, 0, _size.width, _size.height);
    
    //----------
    
    // 滤镜链处理
    GLuint processTexture = _texture;
    for (APGLFilter *filter in _filters) {
        processTexture = [filter processWithTexture:processTexture currentTime:time];
    }
    
    if (_filters.count == 0){
        processTexture = [_emptyFilter processWithTexture:processTexture currentTime:time];
    }
    
    // 贴纸链处理，避免数据动态改变
    NSArray<APGLStickerFilter*> *copyStickers = [_stickers copy];
    for (APGLStickerFilter *sticker in copyStickers) {
        processTexture = [sticker processWithTexture:processTexture currentTime:time];
    }
    
    //----------
    
    glBindFramebuffer(GL_FRAMEBUFFER, _frameBuffer);

    // 清屏
    glClearColor(1, 1, 1, 1);
    glClear(GL_COLOR_BUFFER_BIT);
    [_program useProgram];
    
    const GLfloat vertices[] = {
        -1, -1,   //左下
        1,  -1,   //右下
        -1, 1,   //左上
        1,  1 }; //右上
    glEnableVertexAttribArray(_pa_position);
    glVertexAttribPointer(_pa_position, 2, GL_FLOAT, GL_FALSE, 0, vertices);
    
    // normal
    static const GLfloat coords[] = {
        0, 0,
        1, 0,
        0, 1,
        1, 1
    };

    glEnableVertexAttribArray(_pa_textureCoor);
    glVertexAttribPointer(_pa_textureCoor, 2, GL_FLOAT, GL_FALSE, 0, coords);

    // 激活纹理，并绘制
    [self activeTexture:processTexture];
    
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    
    // 绘制完，解绑
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glBindTexture(GL_TEXTURE_2D, 0);
    
    glFlush();
}

#pragma mark - Convert

- (void)handleSampleBuffer:(CMSampleBufferRef)sampleBuffer {
    
    CMTime frameTime = CMSampleBufferGetPresentationTimeStamp(sampleBuffer);
    [self textureFromBuffer:sampleBuffer];
    [self drawTextureWithCMTime:frameTime];
    
    if (_delegate && [_delegate respondsToSelector:@selector(ouputVideoPixelBuffer:sampleBuffer:)]) {
        [_delegate ouputVideoPixelBuffer:_pixelBuffer sampleBuffer:sampleBuffer];
    }
}

- (GLuint)createTexture {
    
    glEnable(GL_TEXTURE_2D);
    GLuint texture;
    glGenTextures(1, &texture);
    return texture;
}

- (void)textureFromBuffer:(CMSampleBufferRef)sampleBuffer {
    

    glBindTexture(GL_TEXTURE_2D, _texture);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    
    // 获取Buffer
    CVImageBufferRef buffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    CVPixelBufferLockBaseAddress(buffer, kCVPixelBufferLock_ReadOnly);
    
    // 基本信息
    uint8_t *base = CVPixelBufferGetBaseAddress(buffer);
    size_t width = CVPixelBufferGetWidth(buffer);
    size_t height = CVPixelBufferGetHeight(buffer);
    
    // 绑定到纹理上
    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 GL_RGBA,
                 (GLsizei)width,
                 (GLsizei)height,
                 0,
                 GL_RGBA,
                 GL_UNSIGNED_BYTE,
                 base);
    
    CVPixelBufferUnlockBaseAddress(buffer, kCVPixelBufferLock_ReadOnly);
}

- (void)activeTexture:(GLuint)texture {
    
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, texture);
    glUniform1i(_pu_texture, 1);
}

#pragma mark - 滤镜设置

- (void)addFilter:(APGLFilter *)filter {
 
    [filter setupWithSize:_size];
    [_filters addObject:filter];
}

- (void)insertFilter:(APGLFilter *)filter atIndex:(NSUInteger)index {
    
    if (index > _filters.count) {
        index = _filters.count;
    }
    [filter setupWithSize:_size];
    [_filters insertObject:filter atIndex:index];
}

- (void)removeFilter:(APGLFilter *)filter {
    [_filters removeObject:filter];
}

- (void)cleanFilter {
    [_filters removeAllObjects];
}

- (void)addSticker:(APGLStickerFilter*)sticker {

    [sticker setupWithSize:_size];
    [_stickers addObject:sticker];
}

- (void)insertSticker:(APGLStickerFilter*)sticker atIndex:(NSUInteger)index {
    
    if (index > _stickers.count) {
        index = _stickers.count;
    }
    [sticker setupWithSize:_size];
    [_stickers insertObject:sticker atIndex:index];
}

- (void)removeSticker:(APGLStickerFilter*)sticker {
    
    [_stickers removeObject:sticker];
}

- (void)cleanSticker {
    
    [_stickers removeAllObjects];
}

@end
