//
//  APGLDefines.h
//  ap_camera
//
//  Created by wooplus on 2021/4/30.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

#pragma mark - Macros

#define STRINGIZE(x) #x
#define STRINGIZE2(x) STRINGIZE(x)
#define SHADER_STRING(text) @ STRINGIZE2(text)

#pragma mark - OpenGL Type

struct APGLVector4 {
    GLfloat one;
    GLfloat two;
    GLfloat three;
    GLfloat four;
};
typedef struct APGLVector4 APGLVector4;

#pragma mark - Protocol

@protocol APGLInput <NSObject>

@end

@protocol APGLOutput <NSObject>

@end


NS_ASSUME_NONNULL_END
