//
//  APGLGrayFilter.m
//  ap_camera
//
//  Created by wooplus on 2021/5/8.
//

#import "APGLGrayFilter.h"


NSString * const kGrayVertexShader = SHADER_STRING
(
 attribute vec4 a_Position;
 attribute vec2 a_TexCoordIn;
 varying vec2 v_TexCoordOut;

 void main(void)
 {
     gl_Position = a_Position;
     v_TexCoordOut = a_TexCoordIn;
 }
);

NSString * const kGrayFragmentShader = SHADER_STRING
(
 precision mediump float;
 uniform sampler2D u_Texture;
 varying vec2 v_TexCoordOut;
 const vec3 W = vec3(0.2125, 0.7154, 0.0721);
 uniform float saturation;

 void main(void)
 {
     vec4 color = texture2D(u_Texture, v_TexCoordOut);
     float lumiance = dot(color.rgb, W);
     vec3 grayScale = vec3(lumiance);
     gl_FragColor = vec4(mix(grayScale, color.rgb, saturation), color.w);
     
 }
);

@interface APGLGrayFilter ()

@property (nonatomic, assign) GLuint a_position;
@property (nonatomic, assign) GLuint u_texture;
@property (nonatomic, assign) GLuint a_texCoordIn;
@property (nonatomic, assign) GLuint u_saturation;

@end

@implementation APGLGrayFilter

+ (instancetype)filter {
    return [[APGLGrayFilter alloc] initWithVertexShader:kGrayVertexShader
                                         fragmentShader:kGrayFragmentShader];
}

- (instancetype)initWithVertexShader:(NSString *)vertex fragmentShader:(NSString *)fragment {
    
    if (self = [super initWithVertexShader:vertex fragmentShader:fragment]) {
        _a_position = [self.program attributeIndex:@"a_Position"];
        _u_texture = [self.program uniformIndex:@"u_Texture"];
        _a_texCoordIn = [self.program attributeIndex:@"a_TexCoordIn"];
        _u_saturation = [self.program uniformIndex:@"saturation"];
    }
    return self;
}

- (GLuint)processWithTexture:(GLuint)texture currentTime:(CMTime)time {
    
    glBindFramebuffer(GL_FRAMEBUFFER, self.frameBuffer);
    
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    glViewport(0, 0, (GLsizei)self.size.width, (GLsizei)self.size.height);
    
    [self.program useProgram];
    
    // 传递调节对比度的值区间 (0 - 2)
    glUniform1f(_u_saturation, 0.2);
    // 传递原始纹理数据
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, texture);
    glUniform1i(_u_texture, 1);
    
    const GLfloat vertices[] = {
        -1, -1,   //左下
        1,  -1,   //右下
        -1, 1,   //左上
        1,  1 }; //右上
    glEnableVertexAttribArray(_a_position);
    glVertexAttribPointer(_a_position, 2, GL_FLOAT, GL_FALSE, 0, vertices);
    
    // normal
    static const GLfloat coords[] = {
        0, 0,
        1, 0,
        0, 1,
        1, 1
    };
    
    glEnableVertexAttribArray(_a_texCoordIn);
    glVertexAttribPointer(_a_texCoordIn, 2, GL_FLOAT, GL_FALSE, 0, coords);
    
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    
    return self.texture;
}


@end
