//
//  APGLFilter.h
//  ap_camera
//
//  Created by wooplus on 2021/5/8.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <OpenGLES/ES2/gl.h>

#import "APGLDefines.h"
#import "APGLProgram.h"

NS_ASSUME_NONNULL_BEGIN

@interface APGLFilter : NSObject

@property (nonatomic, readonly) APGLProgram *program;
@property (nonatomic, readonly) BOOL initSuccess;
@property (nonatomic, readonly) CGSize size;

@property (nonatomic, readonly) GLuint frameBuffer;
@property (nonatomic, readonly) GLuint texture;

+ (instancetype)filter;

- (instancetype)initWithVertexShader:(NSString*)vertex
                      fragmentShader:(NSString*)fragment;

- (void)setupWithSize:(CGSize)size;

- (GLuint)processWithTexture:(GLuint)texture currentTime:(CMTime)time;

@end

NS_ASSUME_NONNULL_END
