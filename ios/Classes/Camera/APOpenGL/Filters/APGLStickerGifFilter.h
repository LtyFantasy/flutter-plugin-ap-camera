//
//  APGLStickerGifFilter.h
//  ap_camera
//
//  Created by wooplus on 2021/5/17.
//

#import "APGLStickerFilter.h"

NS_ASSUME_NONNULL_BEGIN

@interface APGLStickerGifFilter : APGLStickerFilter

- (void)startWithRepeat:(BOOL)repeat
               complete:(void(^ _Nullable)(void))complete;

- (void)stop;

@end

NS_ASSUME_NONNULL_END
