//
//  APGLBrightnessFilter.m
//  ap_camera
//
//  Created by wooplus on 2021/5/8.
//

#import "APGLBrightnessFilter.h"

NSString * const kBrightVertexShader = SHADER_STRING
(
 attribute vec4 a_Position;
 attribute vec2 a_TexCoordIn;
 varying vec2 v_TexCoordOut;

 void main(void)
 {
     gl_Position = a_Position;
     v_TexCoordOut = a_TexCoordIn;
 }
);

NSString * const kBrightFragmentShader = SHADER_STRING
(
 precision mediump float;
 uniform sampler2D u_Texture;
 varying vec2 v_TexCoordOut;
 uniform float brightness;

 void main()
 {
     vec4 textureColor = texture2D(u_Texture, v_TexCoordOut);
     gl_FragColor = vec4((textureColor.rgb + vec3(brightness)), textureColor.w);
 }
);

@interface APGLBrightnessFilter ()

@property (nonatomic, assign) GLuint a_position;
@property (nonatomic, assign) GLuint u_texture;
@property (nonatomic, assign) GLuint a_texCoordIn;
@property (nonatomic, assign) GLuint u_brightness;

@end

@implementation APGLBrightnessFilter

#pragma mark - Init

+ (instancetype)filter {
    return [[APGLBrightnessFilter alloc] initWithVertexShader:kBrightVertexShader
                                               fragmentShader:kBrightFragmentShader];
}

- (instancetype)initWithVertexShader:(NSString *)vertex fragmentShader:(NSString *)fragment {
    
    if (self = [super initWithVertexShader:vertex fragmentShader:fragment]) {
        _a_position = [self.program attributeIndex:@"a_Position"];
        _u_texture = [self.program uniformIndex:@"u_Texture"];
        _a_texCoordIn = [self.program attributeIndex:@"a_TexCoordIn"];
        _u_brightness = [self.program uniformIndex:@"brightness"];
    }
    return self;
}

- (GLuint)processWithTexture:(GLuint)texture currentTime:(CMTime)time {
    
    glBindFramebuffer(GL_FRAMEBUFFER, self.frameBuffer);
    
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    glViewport(0, 0, (GLsizei)self.size.width, (GLsizei)self.size.height);
    
    [self.program useProgram];
    
    // 传递调节亮度的值区间 (-1 - 1)
    glUniform1f(_u_brightness, -0.5);
    // 传递原始纹理数据
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, texture);
    glUniform1i(_u_texture, 1);
    
    const GLfloat vertices[] = {
        -1, -1, 0,   //左下
        1,  -1, 0,   //右下
        -1, 1,  0,   //左上
        1,  1,  0 }; //右上
    glEnableVertexAttribArray(_a_position);
    glVertexAttribPointer(_a_position, 3, GL_FLOAT, GL_FALSE, 0, vertices);
    
    // normal
    static const GLfloat coords[] = {
        0, 0,
        1, 0,
        0, 1,
        1, 1
    };
    
    glEnableVertexAttribArray(_a_texCoordIn);
    glVertexAttribPointer(_a_texCoordIn, 2, GL_FLOAT, GL_FALSE, 0, coords);
    
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    
    return self.texture;
}

@end
