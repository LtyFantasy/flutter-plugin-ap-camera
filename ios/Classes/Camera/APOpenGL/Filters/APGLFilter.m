//
//  APGLFilter.m
//  ap_camera
//
//  Created by wooplus on 2021/5/8.
//

#import "APGLFilter.h"

@interface APGLFilter()

@property (nonatomic, strong) APGLProgram *program;

@property (nonatomic, assign) BOOL initSuccess;
@property (nonatomic, assign) CGSize size;

@property (nonatomic, assign) GLuint frameBuffer;
@property (nonatomic, assign) GLuint texture;

@end

@implementation APGLFilter

+ (instancetype)filter {
    return nil;
}

- (instancetype)initWithVertexShader:(NSString *)vertex fragmentShader:(NSString *)fragment {
    
    if (self = [super init]) {
        _program = [APGLProgram programWithVertexShader:vertex
                                         fragmentShader:fragment];
    }
    return self;
}

- (void)dealloc {
    
    [_program clean];
    _program = nil;
}

- (void)setupWithSize:(CGSize)size {
    
    if (_initSuccess) return;
    
    _size = size;
    glGenFramebuffers(1, &_frameBuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, _frameBuffer);
    
    glGenTextures(1, &_texture);
    glBindTexture(GL_TEXTURE_2D, _texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA,  size.width, size.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, _texture, 0);
    
    GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if(status != GL_FRAMEBUFFER_COMPLETE) {
        NSLog(@"[%@] failed to make complete framebuffer object %x", [self class], status);
    }
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glBindTexture(GL_TEXTURE_2D, 0);
    
    _initSuccess = YES;
}

- (GLuint)processWithTexture:(GLuint)texture currentTime:(CMTime)time {
    return texture;
}

@end
