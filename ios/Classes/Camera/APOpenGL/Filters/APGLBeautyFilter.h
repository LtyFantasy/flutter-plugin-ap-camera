//
//  APGLBeautyFilter.h
//  ap_camera
//
//  Created by wooplus on 2021/5/10.
//

#import "APGLFilter.h"

NS_ASSUME_NONNULL_BEGIN

@interface APGLBeautyFilter : APGLFilter

- (void)adjustBright:(CGFloat)bright beauty:(CGFloat)beauty tone:(CGFloat)tone;

@end

NS_ASSUME_NONNULL_END
