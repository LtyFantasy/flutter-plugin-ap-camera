//
//  APGLBeautyFilter.m
//  ap_camera
//
//  Created by wooplus on 2021/5/10.
//

#import "APGLBeautyFilter.h"

NSString * const kBeautyVertexShader = SHADER_STRING
(
 attribute vec4 position;
 attribute vec4 inputTextureCoordinate;
 
 varying vec2 textureCoordinate;
 
 void main()
 {
     gl_Position = position;
     textureCoordinate = inputTextureCoordinate.xy;
 }
);

NSString * const kBeautyFragmentShader = SHADER_STRING
(
    precision highp float;
    varying highp vec2 textureCoordinate;

    uniform sampler2D inputImageTexture;

    uniform highp vec2 singleStepOffset;
    uniform highp vec4 params;
    uniform highp float brightness;

    const highp vec3 W = vec3(0.299, 0.587, 0.114);
    const highp mat3 saturateMatrix = mat3(
        1.1102, -0.0598, -0.061,
        -0.0774, 1.0826, -0.1186,
        -0.0228, -0.0228, 1.1772
    );
    highp vec2 blurCoordinates[24];

    highp float hardLight(highp float color) {
        if (color <= 0.5)
            color = color * color * 2.0;
        else
            color = 1.0 - ((1.0 - color)*(1.0 - color) * 2.0);
        return color;
    }

    void main() {
    
        highp vec3 centralColor = texture2D(inputImageTexture, textureCoordinate).rgb;
        blurCoordinates[0] = textureCoordinate.xy + singleStepOffset * vec2(0.0, -10.0);
        blurCoordinates[1] = textureCoordinate.xy + singleStepOffset * vec2(0.0, 10.0);
        blurCoordinates[2] = textureCoordinate.xy + singleStepOffset * vec2(-10.0, 0.0);
        blurCoordinates[3] = textureCoordinate.xy + singleStepOffset * vec2(10.0, 0.0);
        blurCoordinates[4] = textureCoordinate.xy + singleStepOffset * vec2(5.0, -8.0);
        blurCoordinates[5] = textureCoordinate.xy + singleStepOffset * vec2(5.0, 8.0);
        blurCoordinates[6] = textureCoordinate.xy + singleStepOffset * vec2(-5.0, 8.0);
        blurCoordinates[7] = textureCoordinate.xy + singleStepOffset * vec2(-5.0, -8.0);
        blurCoordinates[8] = textureCoordinate.xy + singleStepOffset * vec2(8.0, -5.0);
        blurCoordinates[9] = textureCoordinate.xy + singleStepOffset * vec2(8.0, 5.0);
        blurCoordinates[10] = textureCoordinate.xy + singleStepOffset * vec2(-8.0, 5.0);
        blurCoordinates[11] = textureCoordinate.xy + singleStepOffset * vec2(-8.0, -5.0);
        blurCoordinates[12] = textureCoordinate.xy + singleStepOffset * vec2(0.0, -6.0);
        blurCoordinates[13] = textureCoordinate.xy + singleStepOffset * vec2(0.0, 6.0);
        blurCoordinates[14] = textureCoordinate.xy + singleStepOffset * vec2(6.0, 0.0);
        blurCoordinates[15] = textureCoordinate.xy + singleStepOffset * vec2(-6.0, 0.0);
        blurCoordinates[16] = textureCoordinate.xy + singleStepOffset * vec2(-4.0, -4.0);
        blurCoordinates[17] = textureCoordinate.xy + singleStepOffset * vec2(-4.0, 4.0);
        blurCoordinates[18] = textureCoordinate.xy + singleStepOffset * vec2(4.0, -4.0);
        blurCoordinates[19] = textureCoordinate.xy + singleStepOffset * vec2(4.0, 4.0);
        blurCoordinates[20] = textureCoordinate.xy + singleStepOffset * vec2(-2.0, -2.0);
        blurCoordinates[21] = textureCoordinate.xy + singleStepOffset * vec2(-2.0, 2.0);
        blurCoordinates[22] = textureCoordinate.xy + singleStepOffset * vec2(2.0, -2.0);
        blurCoordinates[23] = textureCoordinate.xy + singleStepOffset * vec2(2.0, 2.0);

        highp float sampleColor = centralColor.g * 22.0;
        sampleColor += texture2D(inputImageTexture, blurCoordinates[0]).g;
        sampleColor += texture2D(inputImageTexture, blurCoordinates[1]).g;
        sampleColor += texture2D(inputImageTexture, blurCoordinates[2]).g;
        sampleColor += texture2D(inputImageTexture, blurCoordinates[3]).g;
        sampleColor += texture2D(inputImageTexture, blurCoordinates[4]).g;
        sampleColor += texture2D(inputImageTexture, blurCoordinates[5]).g;
        sampleColor += texture2D(inputImageTexture, blurCoordinates[6]).g;
        sampleColor += texture2D(inputImageTexture, blurCoordinates[7]).g;
        sampleColor += texture2D(inputImageTexture, blurCoordinates[8]).g;
        sampleColor += texture2D(inputImageTexture, blurCoordinates[9]).g;
        sampleColor += texture2D(inputImageTexture, blurCoordinates[10]).g;
        sampleColor += texture2D(inputImageTexture, blurCoordinates[11]).g;
        sampleColor += texture2D(inputImageTexture, blurCoordinates[12]).g * 2.0;
        sampleColor += texture2D(inputImageTexture, blurCoordinates[13]).g * 2.0;
        sampleColor += texture2D(inputImageTexture, blurCoordinates[14]).g * 2.0;
        sampleColor += texture2D(inputImageTexture, blurCoordinates[15]).g * 2.0;
        sampleColor += texture2D(inputImageTexture, blurCoordinates[16]).g * 2.0;
        sampleColor += texture2D(inputImageTexture, blurCoordinates[17]).g * 2.0;
        sampleColor += texture2D(inputImageTexture, blurCoordinates[18]).g * 2.0;
        sampleColor += texture2D(inputImageTexture, blurCoordinates[19]).g * 2.0;
        sampleColor += texture2D(inputImageTexture, blurCoordinates[20]).g * 3.0;
        sampleColor += texture2D(inputImageTexture, blurCoordinates[21]).g * 3.0;
        sampleColor += texture2D(inputImageTexture, blurCoordinates[22]).g * 3.0;
        sampleColor += texture2D(inputImageTexture, blurCoordinates[23]).g * 3.0;

        sampleColor = sampleColor / 62.0;

        highp float highPass = centralColor.g - sampleColor + 0.5;

        for (int i = 0; i < 5; i++) {
            highPass = hardLight(highPass);
        }
        highp float lumance = dot(centralColor, W);

        highp float alpha = pow(lumance, params.r);

        highp vec3 smoothColor = centralColor + (centralColor-vec3(highPass))*alpha*0.1;

        smoothColor.r = clamp(pow(smoothColor.r, params.g), 0.0, 1.0);
        smoothColor.g = clamp(pow(smoothColor.g, params.g), 0.0, 1.0);
        smoothColor.b = clamp(pow(smoothColor.b, params.g), 0.0, 1.0);

        highp vec3 lvse = vec3(1.0)-(vec3(1.0)-smoothColor)*(vec3(1.0)-centralColor);
        highp vec3 bianliang = max(smoothColor, centralColor);
        highp vec3 rouguang = 2.0*centralColor*smoothColor + centralColor*centralColor - 2.0*centralColor*centralColor*smoothColor;

        gl_FragColor = vec4(mix(centralColor, lvse, alpha), 1.0);
        gl_FragColor.rgb = mix(gl_FragColor.rgb, bianliang, alpha);
        gl_FragColor.rgb = mix(gl_FragColor.rgb, rouguang, params.b);

        highp vec3 satcolor = gl_FragColor.rgb * saturateMatrix;
        gl_FragColor.rgb = mix(gl_FragColor.rgb, satcolor, params.a);
        gl_FragColor.rgb = vec3(gl_FragColor.rgb + vec3(brightness));
    }
);

@interface APGLBeautyFilter ()

@property (nonatomic, assign) GLuint a_position;
@property (nonatomic, assign) GLuint a_inputTextureCoordinate;

@property (nonatomic, assign) GLuint u_inputImageTexture;
@property (nonatomic, assign) GLuint u_singleStepOffset;
@property (nonatomic, assign) GLuint u_brightness;
@property (nonatomic, assign) GLuint u_params;

@property (nonatomic, assign) CGFloat beautyLevel;
@property (nonatomic, assign) CGFloat brightLevel;
@property (nonatomic, assign) CGFloat toneLevel;

@end

@implementation APGLBeautyFilter

+ (instancetype)filter {
    return [[APGLBeautyFilter alloc] initWithVertexShader:kBeautyVertexShader
                                           fragmentShader:kBeautyFragmentShader];
}

- (instancetype)initWithVertexShader:(NSString *)vertex fragmentShader:(NSString *)fragment {
    
    if (self = [super initWithVertexShader:vertex fragmentShader:fragment]) {

        _a_position = [self.program attributeIndex:@"position"];
        _a_inputTextureCoordinate = [self.program attributeIndex:@"inputTextureCoordinate"];
        
        _u_inputImageTexture = [self.program uniformIndex:@"inputImageTexture"];
        _u_singleStepOffset = [self.program uniformIndex:@"singleStepOffset"];
        _u_brightness = [self.program uniformIndex:@"brightness"];
        _u_params = [self.program uniformIndex:@"params"];
        
        _toneLevel = 0.0;
        _beautyLevel = 0.68;
        _brightLevel = 0.44;
    }
    return self;
}

- (GLuint)processWithTexture:(GLuint)texture currentTime:(CMTime)time {
    
    glBindFramebuffer(GL_FRAMEBUFFER, self.frameBuffer);
    
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    glViewport(0, 0, (GLsizei)self.size.width, (GLsizei)self.size.height);
    
    [self.program useProgram];
    
    // 参数设置
    [self setSingleStepOffset:self.size];
    [self setBright:_brightLevel];
    [self setBeauty:_beautyLevel tone:_toneLevel];
    
    // 传递原始纹理数据
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, texture);
    glUniform1i(_u_inputImageTexture, 1);
    
    const GLfloat vertices[] = {
        -1, -1, 0,   //左下
        1,  -1, 0,   //右下
        -1, 1,  0,   //左上
        1,  1,  0 }; //右上
    glEnableVertexAttribArray(_a_position);
    glVertexAttribPointer(_a_position, 3, GL_FLOAT, GL_FALSE, 0, vertices);
    
    // normal
    static const GLfloat coords[] = {
        0, 0,
        1, 0,
        0, 1,
        1, 1
    };
    glEnableVertexAttribArray(_a_inputTextureCoordinate);
    glVertexAttribPointer(_a_inputTextureCoordinate, 2, GL_FLOAT, GL_FALSE, 0, coords);

    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    return self.texture;
}

- (void)setSingleStepOffset:(CGSize)size {
    
    CGFloat x = 2.0f / size.width;
    CGFloat y = 2.0f / size.height;
    
    GLfloat positionArray[2] = {x, y};
    glUniform2fv(_u_singleStepOffset, 1, positionArray);
}

- (void)adjustBright:(CGFloat)bright beauty:(CGFloat)beauty tone:(CGFloat)tone {
    
    _brightLevel = bright;
    _beautyLevel = beauty;
    _toneLevel = tone;
}

- (void)setBright:(CGFloat)bright {
    
    CGFloat value = 0.6 * (-0.5 + bright);
    glUniform1f(_u_brightness, value);
}

- (void)setBeauty:(CGFloat)beauty tone:(CGFloat)tone {
 
    APGLVector4 fBeautyParam;
    fBeautyParam.one = 1.0 - 0.6 * beauty;
    fBeautyParam.two = 1.0 - 0.3 * beauty;
    fBeautyParam.three = 0.1 + 0.3 * tone;
    fBeautyParam.four = 0.1 + 0.3 * tone;
    glUniform4fv(_u_params, 1, (GLfloat *)&fBeautyParam);
}

@end
