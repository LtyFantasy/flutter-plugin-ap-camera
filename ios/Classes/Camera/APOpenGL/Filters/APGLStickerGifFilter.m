//
//  APGLStickerGifFilter.m
//  ap_camera
//
//  Created by wooplus on 2021/5/17.
//

#import "APGLStickerGifFilter.h"

@interface APGLStickerGifFilter()

@property (nonatomic, assign) BOOL play;
@property (nonatomic, assign) BOOL repeat;
@property (nonatomic, copy) void (^completeBlock) (void);

@property (nonatomic, strong) NSMutableDictionary *textureMap;
@property (nonatomic, assign) CMTime startTime;

@end

@implementation APGLStickerGifFilter

#pragma mark - Init

- (instancetype)initWithSticker:(APGLSticker *)sticker  {
    
    if (self = [super initWithSticker:sticker]) {
        _textureMap = [NSMutableDictionary dictionaryWithCapacity:sticker.frameImages.count];
    }
    return self;
}

- (void)dealloc {
    
    for (NSNumber *n in _textureMap.allValues) {
        GLuint texture = n.intValue;
        if (texture > 0) {
            glDeleteTextures(1, &texture);
        }
    }
}

#pragma mark - Control

- (void)startWithRepeat:(BOOL)repeat complete:(void (^)(void))complete {
    
    if (_play) return;
    
    _startTime = kCMTimeInvalid;
    _repeat = repeat;
    _completeBlock = complete;
    _play = YES;
}

- (void)stop {
    
    _play = NO;
    _startTime = kCMTimeInvalid;
}

#pragma mark - OpenGL

- (GLuint)nextTextureWithTime:(CMTime)time {
    return [self textureWithTime:time];
}

- (GLuint)textureWithTime:(CMTime)time {
    
    NSUInteger index = 0;
    
    // 播放Gif，循环取帧
    if (_play) {
        
        if (CMTIME_IS_INVALID(_startTime)) {
            _startTime = time;
        }
        
        // 计算当前应该用第几帧图，并且进行纹理缓存复用
        CMTime deltaTime = CMTimeSubtract(time, _startTime);
        NSUInteger deltaMS = deltaTime.value * 1000.f / deltaTime.timescale;
        
        index = deltaMS / self.sticker.frameInterval;
        
        // 检查是否执行完一个周期
        if (index > self.sticker.frameImages.count && !_repeat) {
            [self stop];
            if (_completeBlock) {
                _completeBlock();
                _completeBlock = nil;
            }
        }
        
        index = index % self.sticker.frameImages.count;
    }
    
    // 纹理如果存在，使用缓存
    // 不存在则创建
    GLuint texture;
    NSNumber *value = _textureMap[@(index)];
    if (!value) {
        texture = [self textureWithImage:self.sticker.frameImages[index]];
        _textureMap[@(index)] = @(texture);
    }
    else {
        texture = value.intValue;
    }
    return texture;
}

@end
