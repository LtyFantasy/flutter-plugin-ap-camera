//
//  APGLStickerFilter.h
//  ap_camera
//
//  Created by wooplus on 2021/5/13.
//

#import "APGLFilter.h"
#import "APGLSticker.h"
#import "APGLAnimator.h"

NS_ASSUME_NONNULL_BEGIN

@interface APGLStickerFilter : APGLFilter

// 设置给Filter的Sticker必须是已经加载好资源的
@property (nonatomic, strong) APGLSticker *sticker;

// 贴纸显示位置
@property (nonatomic, assign) CGRect stickerFrame;

// 贴纸动画控制器
@property (nonatomic, strong) APGLAnimator *animator;

// 初始化
+ (instancetype)filterWithSticker:(APGLSticker*)sticker;

// 初始化
- (instancetype)initWithSticker:(APGLSticker*)sticker;

// 获取下一帧纹理进行渲染
// 子类可以重写该方法，实现纹理替换
- (GLuint)nextTextureWithTime:(CMTime)time;

// 计算下一帧的顶点坐标
// 子类可以重写该方法，实现缩放、位移
- (void)calNextVertices:(GLfloat*)vertices withTime:(CMTime)time;

// 图片转纹理
- (GLuint)textureWithImage:(UIImage *)image;

// 计算指定位置、大小对应的顶点坐标
- (void)calculatePosition:(CGRect)position array:(GLfloat*)array;

@end

NS_ASSUME_NONNULL_END
