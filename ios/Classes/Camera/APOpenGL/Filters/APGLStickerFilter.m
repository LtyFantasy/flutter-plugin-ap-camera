//
//  APGLStickerFilter.m
//  ap_camera
//
//  Created by wooplus on 2021/5/13.
//

#import "APGLStickerFilter.h"

NSString *const kStickerVertex = SHADER_STRING
(
 attribute vec4 a_Position;
 attribute vec2 a_TexCoordIn;
 varying vec2 v_TexCoordOut;

 void main(void) {
     
     gl_Position = a_Position;
     v_TexCoordOut = a_TexCoordIn;
 }
);


NSString *const kStickerFragment = SHADER_STRING
(
 precision mediump float;
 uniform sampler2D u_Texture;
 varying vec2 v_TexCoordOut;

 void main(void) {
     
    vec4 color = texture2D(u_Texture, v_TexCoordOut);
    vec4 newColor = vec4(color.z, color.y, color.x, color.w);
    gl_FragColor = newColor;
 }
);

@interface APGLStickerFilter()

@property (nonatomic, assign) GLuint a_position;
@property (nonatomic, assign) GLuint u_texture;
@property (nonatomic, assign) GLuint a_texCoordIn;

@property (nonatomic, assign) GLuint imgTexture;

@end

@implementation APGLStickerFilter

+ (instancetype)filterWithSticker:(APGLSticker*)sticker {
    return [[self alloc] initWithSticker:sticker];
}

- (instancetype)initWithSticker:(APGLSticker*)sticker {
    return [self initWithVertexShader:kStickerVertex
                       fragmentShader:kStickerFragment
                              sticker:sticker];
}

- (instancetype)initWithVertexShader:(NSString *)vertex
                      fragmentShader:(NSString *)fragment
                             sticker:(APGLSticker*)sticker {
    
    if (self = [super initWithVertexShader:vertex fragmentShader:fragment]) {
        
        _a_position = [self.program attributeIndex:@"a_Position"];
        _u_texture = [self.program uniformIndex:@"u_Texture"];
        _a_texCoordIn = [self.program attributeIndex:@"a_TexCoordIn"];
        
        _sticker = sticker;
    }
    return self;
}

- (void)dealloc {
    
    if (_imgTexture > 0) {
        glDeleteTextures(1, &_imgTexture);
        _imgTexture = 0;
    }
}

- (GLuint)processWithTexture:(GLuint)texture currentTime:(CMTime)time {
    
    glEnable(GL_BLEND);
    glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
    
    glBindFramebuffer(GL_FRAMEBUFFER, self.frameBuffer);
    glViewport(0, 0, (GLsizei)self.size.width, (GLsizei)self.size.height);
    
    [self.program useProgram];
    
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, texture);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture, 0);
    
    // 传递纹理数据
    GLuint stickerTexture = [self nextTextureWithTime:time];
    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, stickerTexture);
    glUniform1i(_u_texture, 2);
    
    // 计算贴纸位置
    GLfloat vertices[8];
    [self calNextVertices:vertices withTime:time];
    
    glEnableVertexAttribArray(_a_position);
    glVertexAttribPointer(_a_position, 2, GL_FLOAT, GL_FALSE, 0, vertices);
    
    const GLfloat coords[] = {
        0, 1,
        1, 1,
        0, 0,
        1, 0
    };
    glEnableVertexAttribArray(_a_texCoordIn);
    glVertexAttribPointer(_a_texCoordIn, 2, GL_FLOAT, GL_FALSE, 0, coords);
    
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    
    // 解绑
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glBindTexture(GL_TEXTURE_2D, 0);
    
    glDisable(GL_BLEND);
    
    return texture;
}

- (GLuint)nextTextureWithTime:(CMTime)time {
    
    if (_imgTexture > 0) return _imgTexture;
    _imgTexture = [self textureWithImage:_sticker.frameImages.firstObject];
    return _imgTexture;
}

- (void)calNextVertices:(GLfloat *)vertices withTime:(CMTime)time {
    
    CGRect frame = _stickerFrame;
    if (_animator) {
        NSUInteger timestamp = time.value * 1000 / time.timescale;
        frame = [_animator processFrame:frame withTime:timestamp];
    }
    
    [self calculatePosition:frame array:vertices];
}

- (GLuint)textureWithImage:(UIImage *)image {
    
    CGImageRef imageRef = image.CGImage;
    if (!imageRef) return 0;
    
    size_t width = CGImageGetWidth(imageRef);
    size_t height = CGImageGetHeight(imageRef);
    GLubyte *imageData = (GLubyte *)calloc(width * height * 4, sizeof(GLubyte));
    CGContextRef context = CGBitmapContextCreate(imageData,
                                                 width,
                                                 height,
                                                 8,
                                                 width * 4,
                                                 CGImageGetColorSpace(imageRef),
                                                 kCGImageAlphaPremultipliedLast);
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), imageRef);
    CGContextRelease(context);
    
    GLuint texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    
    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 GL_RGBA,
                 (GLsizei)width,
                 (GLsizei)height,
                 0,
                 GL_RGBA,
                 GL_UNSIGNED_BYTE,
                 imageData);
    free(imageData);
    return texture;
}

- (void)calculatePosition:(CGRect)position array:(GLfloat*)array {
    
    GLfloat width = position.size.width * 2.0f / self.size.width;
    GLfloat height = position.size.height * 2.0f / self.size.height;
    GLfloat tlX = (position.origin.x * 2.0f / self.size.width) - 1.0f;
    GLfloat tlY = (position.origin.y * 2.0f / self.size.height) - 1.0f;
    
    // bottom left
    array[0] = tlX;
    array[1] = tlY + height;
    // bottom right
    array[2] = tlX + width;
    array[3] = tlY + height;
    // top left
    array[4] = tlX;
    array[5] = tlY;
    // top right
    array[6] = tlX + width;
    array[7] = tlY;
}

@end
