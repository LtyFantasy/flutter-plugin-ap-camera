//
//  APGLAnimator.m
//  ap_camera
//
//  Created by wooplus on 2021/5/17.
//

#import "APGLAnimator.h"

#import "APGLInterpolator.h"
#import "APGLEaseInOut.h"

@interface APGLAnimator ()

@property (nonatomic, assign) NSUInteger durationMS;

@property (nonatomic, assign) BOOL start;
@property (nonatomic, assign) BOOL revers;
@property (nonatomic, assign) NSUInteger lastTime;
@property (nonatomic, assign) CGFloat percentProgress;
@property (nonatomic, copy) void(^completeBlock)(void);

@end

@implementation APGLAnimator

- (instancetype)initWithDurationMS:(NSUInteger)duration {
    
    if (self = [super init]) {
        _durationMS = duration;
        _start = NO;
        _revers = NO;
        _lastTime = 0;
        _percentProgress = 0;
    }
    return self;
}

#pragma mark - Animation Process

- (CGRect)processFrame:(CGRect)frame withTime:(NSUInteger)time {
    
    CGFloat percent;
    if (_start) {
        percent = [self calculatePercentWithTime:time];
    }
    else {
        percent = _percentProgress;
    }
    
    CGRect output = [self outoutWithOrigin:frame percent:percent];
    // 检查是否动画结束
    if ((_percentProgress >= 1 && !_revers)
        || (_percentProgress <= 0 && _revers)) {
        [self stopAnimation];
    }
    
    return output;
}

- (CGFloat)calculatePercentWithTime:(NSUInteger)time {
    
    CGFloat deltaTime = 0;
    if (_lastTime <= 0) {
        _lastTime = time;
    }
    else {
        deltaTime = time - _lastTime;
        _lastTime = time;
    }
    
    // 计算进度增量
    CGFloat deltaPercent = deltaTime / (CGFloat)(self.durationMS);
    if (_revers) {
        _percentProgress -= deltaPercent;
        if (_percentProgress < 0) {
            _percentProgress = 0;
        }
    }
    else {
        _percentProgress += deltaPercent;
        if (_percentProgress > 1) {
            _percentProgress = 1;
        }
    }
    
    return _percentProgress;
}

- (CGRect)outoutWithOrigin:(CGRect)frame percent:(CGFloat)percent {
    return frame;
}

- (CGFloat)percentWithCurve:(CGFloat)percent {
    
    switch (_curve) {
        case APGLAnimationCurveEasyInOut:
            return [APGLEaseInOut valueWithPercent:percent];
            
        default:
            return [APGLInterpolator valueWithPercent:percent];
    }
}

#pragma mark - Control

- (void)startAnimationWithCompleteBlock:(void(^)(void))block {
    
    if (_start) return;
    _revers = NO;
    _lastTime = 0;
    _completeBlock = block;
    _start = YES;
}

- (void)reverseAnimationWithCompleteBlock:(void (^)(void))block {
    
    if (_start) return;
    _revers = YES;
    _lastTime = 0;
    _completeBlock = block;
    _start = YES;
}

- (void)stopAnimation {
    
    if (!_start) return;
    _start = NO;
    _revers = NO;
    _lastTime = 0;
    
    if (_completeBlock) {
        _completeBlock();
        _completeBlock = nil;
    }
}

- (void)cancelAnimation {
    
    _start = NO;
    _revers = NO;
    _lastTime = 0;
    _percentProgress = 0;
    _completeBlock = nil;
}

@end
