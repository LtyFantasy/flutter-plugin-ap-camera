//
//  APGLAnimator.h
//  ap_camera
//
//  Created by wooplus on 2021/5/17.
//

#import <UIKit/UIkit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM (NSUInteger, APGLAnimationCurve) {
    APGLAnimationCurveLinear,
    APGLAnimationCurveEasyInOut,
};

@interface APGLAnimator : NSObject

@property (nonatomic, readonly) NSUInteger durationMS;
@property (nonatomic, assign) APGLAnimationCurve curve;

- (instancetype)initWithDurationMS:(NSUInteger)duration;

- (CGRect)processFrame:(CGRect)frame withTime:(NSUInteger)time;
- (CGRect)outoutWithOrigin:(CGRect)frame percent:(CGFloat)percent;

- (void)startAnimationWithCompleteBlock:(void(^)(void))block;
- (void)reverseAnimationWithCompleteBlock:(void(^)(void))block;
- (void)stopAnimation;
- (void)cancelAnimation;

@end

NS_ASSUME_NONNULL_END
