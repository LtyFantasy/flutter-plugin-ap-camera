//
//  APGLScaleAnimator.m
//  ap_camera
//
//  Created by wooplus on 2021/5/17.
//

#import "APGLScaleAnimator.h"
#import "APGLEaseInOut.h"

@interface APGLScaleAnimator()

@property (nonatomic, assign) CGFloat scale;

@end

@implementation APGLScaleAnimator

#pragma mark - Init

+ (instancetype)animatorWithDurationMS:(NSUInteger)duration scale:(CGFloat)scale {
    return [[APGLScaleAnimator alloc] initWithDuration:duration scale:scale];
}

- (instancetype)initWithDuration:(NSUInteger)duration scale:(CGFloat)scale {
    
    if (self = [super initWithDurationMS:duration]) {
        
        _scale = scale;
        if (_scale < 0) {
            _scale = 0;
        }
    }
    return self;
}

#pragma mark - Animation

- (CGRect)outoutWithOrigin:(CGRect)frame percent:(CGFloat)percent {
    
    CGFloat destWidth = frame.size.width * (1 + (_scale - 1) * percent) ;
    CGFloat destHeight = frame.size.height * (1 + (_scale - 1) * percent);
    CGFloat destX = frame.origin.x - ((destWidth - frame.size.width) / 2);
    CGFloat destY = frame.origin.y - ((destHeight - frame.size.height) / 2);
    return CGRectMake(destX, destY, destWidth, destHeight);;
}

@end
