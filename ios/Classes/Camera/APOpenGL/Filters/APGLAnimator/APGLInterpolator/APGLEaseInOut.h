//
//  APGLEaseInOut.h
//  ap_camera
//
//  Created by wooplus on 2021/5/17.
//

#import <Foundation/Foundation.h>
#import "APGLInterpolator.h"

NS_ASSUME_NONNULL_BEGIN

@interface APGLEaseInOut : APGLInterpolator

@end

NS_ASSUME_NONNULL_END
