//
//  APGLInterpolator.h
//  ap_camera
//
//  Created by wooplus on 2021/5/17.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface APGLInterpolator : NSObject

+ (CGFloat)valueWithPercent:(CGFloat)percent;

@end

NS_ASSUME_NONNULL_END
