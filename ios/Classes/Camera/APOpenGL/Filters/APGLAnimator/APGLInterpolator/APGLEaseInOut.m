//
//  APGLEaseInOut.m
//  ap_camera
//
//  Created by wooplus on 2021/5/17.
//

#import "APGLEaseInOut.h"

@implementation APGLEaseInOut

+ (CGFloat)valueWithPercent:(CGFloat)percent {
    
    if(percent <= 0.5f) {
        return 2.0f * percent * percent;
    }
    else {
        percent -= 0.5f;
        return 2.0f * percent * (1.0f - percent) + 0.5f;
    }
}

@end
