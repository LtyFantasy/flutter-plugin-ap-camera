//
//  APGLScaleAnimator.h
//  ap_camera
//
//  Created by wooplus on 2021/5/17.
//

#import "APGLAnimator.h"

NS_ASSUME_NONNULL_BEGIN

@interface APGLScaleAnimator : APGLAnimator

+ (instancetype)animatorWithDurationMS:(NSUInteger)duration scale:(CGFloat)scale;

@end

NS_ASSUME_NONNULL_END
