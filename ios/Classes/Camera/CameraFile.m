//
//  CameraFile.m
//  ap_camera
//
//  Created by wooplus on 2021/4/30.
//

#import "CameraFile.h"

@implementation CameraFile

- (NSDictionary *)toMap {
    return @{
        @"isVideo":     @(_isVideo),
        @"filename":    _filename,
        @"fullPath":    _fullPath,
        @"size":        @(_size),
        @"createAt":    @(_createAt)
    };
}

@end
