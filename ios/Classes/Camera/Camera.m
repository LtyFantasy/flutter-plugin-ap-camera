//
//  Camera.m
//  ap_camera
//
//  Created by wooplus on 2021/4/21.
//

#import "Camera.h"
#import "CameraManager.h"
#import "OpenGLHandler.h"
#import "APGLMovieWriter.h"
#import "MLFaceDetect.h"
#import "APImageDataTools.h"

#import <UIKit/UIKit.h>
#import <Accelerate/Accelerate.h>
#import <CoreMotion/CoreMotion.h>
#import <libkern/OSAtomic.h>
#import <stdatomic.h>

#pragma mark - Global Method

AVCaptureDevicePosition cameraPositionForString(NSString * _Nullable position) {
    if ([position isEqualToString:@"front"]) {
        return AVCaptureDevicePositionFront;
    } else if ([position isEqualToString:@"back"]) {
        return AVCaptureDevicePositionBack;
    } else {
        return AVCaptureDevicePositionUnspecified;
    }
}

NSString* cameraPositionStringForPosition(AVCaptureDevicePosition position) {
    if (position == AVCaptureDevicePositionFront) {
        return @"front";
    } else if (position == AVCaptureDevicePositionBack) {
        return @"back";
    } else {
        return @"unknown";
    }
}

CameraResolution cameraResolutionForString(NSString * _Nullable resolution) {
    if ([resolution isEqualToString:@"veryLow"]) {
        return CameraResolutionVeryLow;
    } else if ([resolution isEqualToString:@"low"]) {
        return CameraResolutionLow;
    } else if ([resolution isEqualToString:@"medium"]) {
        return CameraResolutionMedium;
    } else if ([resolution isEqualToString:@"high"]) {
        return CameraResolutionHigh;
    } else if ([resolution isEqualToString:@"veryHigh"]) {
        return CameraResolutionVeryHigh;
    } else if ([resolution isEqualToString:@"ultraHigh"]) {
        return CameraResolutionUltraHigh;
    } else if ([resolution isEqualToString:@"max"]) {
        return CameraResolutionMax;
    } else {
        return CameraResolutionMedium;
    }
}

#pragma mark - Main Class

@interface Camera() <AVCaptureVideoDataOutputSampleBufferDelegate, AVCaptureAudioDataOutputSampleBufferDelegate, OpenGLHandlerDelegate, APGLMovieWriterDelegate>

@property (nonatomic, strong) dispatch_queue_t queue;

@property (nonatomic, copy) NSString *cameraId;
@property (nonatomic, assign) CameraResolution resolution;
@property (nonatomic, assign) BOOL enableAudio;
@property (nonatomic, assign) UIDeviceOrientation orientation;
@property (nonatomic, assign) UIDeviceOrientation lockedOrientation;
@property (nonatomic, assign) CameraFlashMode flashMode;
@property (nonatomic, assign) CameraExposureMode exposureMode;
@property (nonatomic, assign) CameraFocusMode focusMode;

@property (nonatomic, strong) AVCaptureSession *session;
@property (nonatomic, strong) AVCaptureDevice *device;
@property (nonatomic, strong) AVCaptureDeviceInput *input;
@property (nonatomic, strong) AVCaptureVideoDataOutput *videoOutput;
@property (nonatomic, strong) AVCapturePhotoOutput *photoOutput;
@property (nonatomic, strong) CMMotionManager *motionManager;

@property (nonatomic, assign) OSType videoFormat;

@property (nonatomic, strong) OpenGLHandler *glHandler;
@property (nonatomic, strong) APGLMovieWriter *movieWriter;


@property (nonatomic, strong) MLFaceDetect *faceDetect;
@property (nonatomic, assign) BOOL enableFaceDetect;
@property (nonatomic, assign) NSInteger faceDetectCount;
@property (nonatomic, assign) BOOL enableFaceDetectDataTransition;
@property (nonatomic, copy) void(^faceDetectCallbak)(MLFaceDetectResult*, CVPixelBufferRef, CMTime);

/// 最近一帧视频数据
@property (nonatomic, assign) CVPixelBufferRef volatile latestPixelBuffer;

/// 是否录制中
@property (nonatomic, assign) BOOL isRecording;

@end

@implementation Camera

#pragma mark - Init

- (instancetype)initWithDevice:(AVCaptureDevice*)device
                    resolution:(CameraResolution)resolution
                   enableAudio:(BOOL)enableAudio
                   orientation:(UIDeviceOrientation)orientation
                         error:(NSError**)error {
    
    if (self = [super init]) {
        
        _enableAudio = enableAudio;
        _queue = dispatch_queue_create("com.aplus.camera.queue", NULL);
        _orientation = orientation;
        _lockedOrientation = UIDeviceOrientationPortrait;
        _exposureMode = CameraExposureModeAuto;
        _focusMode = CameraFocusModeAuto;
        
        // 分辨率值保护
        if (!resolution || (resolution < CameraResolutionVeryLow || resolution > CameraResolutionMax)) {
            resolution = CameraResolutionMax;
        }
        else {
            _resolution = resolution;
        }
        
        // AV相关对象初始化
        _session = [AVCaptureSession new];
        _device = device;
        _flashMode = _device.hasFlash ? CameraFlashModeAuto : CameraFlashModeOff;
        
        NSError *localError;
        _input = [[AVCaptureDeviceInput alloc] initWithDevice:_device error:&localError];
        if (localError) {
            *error = localError;
            return nil;
        }
        
        // 视频输出
        _videoOutput = [AVCaptureVideoDataOutput new];
        _videoOutput.videoSettings = @{(NSString *)kCVPixelBufferPixelFormatTypeKey : @(kCVPixelFormatType_32BGRA)};
        [_videoOutput setAlwaysDiscardsLateVideoFrames:YES];
        [_videoOutput setSampleBufferDelegate:self queue:_queue];
        
        AVCaptureConnection *connection = [AVCaptureConnection connectionWithInputPorts:_input.ports
                                                                                 output:_videoOutput];
        // 前置摄像头开启镜像
        BOOL isFrontCamera = NO;
        if ([_device position] == AVCaptureDevicePositionFront) {
            connection.videoMirrored = YES;
            isFrontCamera = YES;
        }
        
        // 添加输入输出
        [_session addInputWithNoConnections:_input];
        [_session addOutputWithNoConnections:_videoOutput];
        [_session addConnection:connection];
        
        // 图片输出
        _photoOutput = [AVCapturePhotoOutput new];
        [_photoOutput setHighResolutionCaptureEnabled:YES];
        [_session addOutput:_photoOutput];
        
        _motionManager = [[CMMotionManager alloc] init];
        [_motionManager startAccelerometerUpdates];
        
        [self setCaptureSessionPreset:_resolution isFront:isFrontCamera];
        [self updateOrientation];
        
        // OpenGL
        _glHandler = [[OpenGLHandler alloc] initWithVideoSize:_viewPortSize];
        _glHandler.delegate = self;
        
        // AI Face
        _faceDetect = [[MLFaceDetect alloc] initWithCameraPosition:device.position];
    }
    
    return self;
}

- (void)close {
    
    if (_isRecording) {
#warning 移除文件
        [self stopRecordWithCompletionBlock:nil];
    }
    
    [self stop];
    
    for (AVCaptureInput *input in [_session inputs]) {
        [_session removeInput:input];
    }
    for (AVCaptureOutput *output in [_session outputs]) {
        [_session removeOutput:output];
    }
    
    if (_latestPixelBuffer) {
        CVPixelBufferRelease(_latestPixelBuffer);
        _latestPixelBuffer = NULL;
    }
    [_motionManager stopAccelerometerUpdates];
    
    dispatch_sync(_queue, ^{
        [_glHandler close];
    });
    _glHandler = nil;
    _faceDetect = nil;
}

- (void)selectCamera:(CALayer*)layer {
    
    // 也可以是kCVPixelFormatType_420YpCbCr8BiPlanarVideoRange
    _videoFormat = kCVPixelFormatType_420YpCbCr8BiPlanarVideoRange;
    
    AVCaptureVideoPreviewLayer *captureVideoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:_session];
    //captureVideoPreviewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [layer addSublayer:captureVideoPreviewLayer];
    captureVideoPreviewLayer.frame = layer.bounds;
}

#pragma mark - Resolution

/**
 设置对应的分辨率
 */
- (void)setCaptureSessionPreset:(CameraResolution)resolution isFront:(BOOL)isFront {
    
    switch (resolution) {
            
        case CameraResolutionMax:
        case CameraResolutionUltraHigh:
            
            if ([_session canSetSessionPreset:AVCaptureSessionPreset3840x2160]) {
                _session.sessionPreset = AVCaptureSessionPreset3840x2160;
                _previewSize = CGSizeMake(3840, 2160);
                break;
            }
            
            if ([_session canSetSessionPreset:AVCaptureSessionPresetHigh]) {
                _session.sessionPreset = AVCaptureSessionPresetHigh;
                _previewSize = CGSizeMake(_device.activeFormat.highResolutionStillImageDimensions.width, _device.activeFormat.highResolutionStillImageDimensions.height);
                break;
            }
            
        case CameraResolutionVeryHigh:
            
            if ([_session canSetSessionPreset:AVCaptureSessionPreset1920x1080]) {
                _session.sessionPreset = AVCaptureSessionPreset1920x1080;
                _previewSize = CGSizeMake(1920, 1080);
                break;
            }
            
        case CameraResolutionHigh:
            
            if ([_session canSetSessionPreset:AVCaptureSessionPreset1280x720]) {
                _session.sessionPreset = AVCaptureSessionPreset1280x720;
                //_previewSize = CGSizeMake(1280, 720);
                _previewSize = CGSizeMake(1024, 576);
                break;
            }
            
        case CameraResolutionMedium:
            
            if ([_session canSetSessionPreset:AVCaptureSessionPreset640x480]) {
                _session.sessionPreset = AVCaptureSessionPreset640x480;
                _previewSize = CGSizeMake(640, 480);
                break;
            }
            
        case CameraResolutionLow:
            
            if ([_session canSetSessionPreset:AVCaptureSessionPreset352x288]) {
                _session.sessionPreset = AVCaptureSessionPreset352x288;
                _previewSize = CGSizeMake(352, 288);
                break;
            }
            
        default:
            
            if ([_session canSetSessionPreset:AVCaptureSessionPresetLow]) {
                _session.sessionPreset = AVCaptureSessionPresetLow;
                _previewSize = CGSizeMake(352, 288);
            }
            else {
                
                NSError *error =[NSError errorWithDomain:NSCocoaErrorDomain
                                                    code:NSURLErrorUnknown
                                                userInfo:@{NSLocalizedDescriptionKey : @"No capture session available for current capture session."}];
                @throw error;
            }
    }
    
    // 如果是前置摄像头，宽高反转
    if (isFront) {
        _previewSize = CGSizeMake(_previewSize.height, _previewSize.width);
    }
    
    CGFloat scale = UIScreen.mainScreen.bounds.size.height / _previewSize.width;
    scale = scale * UIScreen.mainScreen.scale;
    _viewPortSize = CGSizeMake(_previewSize.width * scale, _previewSize.height *scale);
}

#pragma mark - Orientation

/// 更新方向
- (void)updateOrientation {
    
    if (_isRecording) {
        return;
    }
    
    UIDeviceOrientation orientation = (_lockedOrientation != UIDeviceOrientationUnknown)
    ? _lockedOrientation
    : _orientation;
    
    [self updateOrientation:orientation forCaptureOutput:_photoOutput];
    [self updateOrientation:orientation forCaptureOutput:_videoOutput];
}

/// 更新Ouput方向
- (void)updateOrientation:(UIDeviceOrientation)orientation
         forCaptureOutput:(AVCaptureOutput *)captureOutput {
    
    if (!captureOutput) {
        return;
    }
    
    AVCaptureConnection *connection = [captureOutput connectionWithMediaType:AVMediaTypeVideo];
    if (connection && connection.isVideoOrientationSupported) {
        connection.videoOrientation = [self getVideoOrientationForDeviceOrientation:orientation];
    }
}

/// 根据设备方向计算Video方向
- (AVCaptureVideoOrientation)getVideoOrientationForDeviceOrientation:(UIDeviceOrientation)deviceOrientation {
    
    /// 注意，设备方向如果是Landscape，则Video方向是相反的
    if (deviceOrientation == UIDeviceOrientationPortrait) {
        return AVCaptureVideoOrientationPortrait;
    }
    else if (deviceOrientation == UIDeviceOrientationLandscapeLeft) {
        return AVCaptureVideoOrientationLandscapeRight;
    }
    else if (deviceOrientation == UIDeviceOrientationLandscapeRight) {
        return AVCaptureVideoOrientationLandscapeLeft;
    }
    else if (deviceOrientation == UIDeviceOrientationPortraitUpsideDown) {
        return AVCaptureVideoOrientationPortraitUpsideDown;
    }
    else {
        return AVCaptureVideoOrientationPortrait;
    }
}

#pragma mark - Innel Calculate

/// 根据当前闪光灯模式，获取对应的系统闪光模式
- (AVCaptureFlashMode)getAVCaptureFlashModeForFlashMode {
    switch (_flashMode) {
        case CameraFlashModeOff:
            return AVCaptureFlashModeOff;
        case CameraFlashModeAuto:
            return AVCaptureFlashModeAuto;
        case CameraFlashModeAlways:
            return AVCaptureFlashModeOn;
        case CameraFlashModeTorch:
        default:
            return -1;
    }
}

#pragma mark - Control

- (void)start {
    [_session startRunning];
}

- (void)stop {
    [_session stopRunning];
}

- (BOOL)startRecord:(NSURL*)file progressUpdate:(void(^)(NSUInteger timeMS))block {
    
    if (_isRecording) return YES;
    
    _movieWriter = [APGLMovieWriter writerWithURL:file
                                             size:_previewSize
                                         delegate:self];
    _movieWriter.progressUpdate = block;
    _isRecording = YES;
    return YES;
}

- (void)stopRecordWithCompletionBlock:(void (^)(NSString * _Nullable, BOOL))block {
    
    if (!_isRecording) return;
    
    __weak typeof(self) weakSelf = self;
    [_movieWriter stopWithCompletion:^(NSString *filepath, BOOL success){
        weakSelf.movieWriter = nil;
        weakSelf.isRecording = NO;
        if (block) {
            block(filepath, success);
        }
    }];
}

- (void)cancelRecord {
    
    if (!_isRecording) return;
    _isRecording = NO;
    [_movieWriter cancel];
    _movieWriter = nil;
}

#pragma mark - 滤镜 & 贴纸

- (void)addFilter:(APGLFilter *)filter {
    [_glHandler addFilter:filter];
}

- (void)insertFilter:(APGLFilter *)filter atIndex:(NSUInteger)index {
    [_glHandler insertFilter:filter atIndex:index];
}

- (void)removeFilter:(APGLFilter *)filter {
    [_glHandler removeFilter:filter];
}

- (void)cleanFilter {
    [_glHandler cleanFilter];
}

- (void)addSticker:(APGLStickerFilter*)sticker {
    [_glHandler addSticker:sticker];
}
- (void)insertSticker:(APGLStickerFilter*)sticker atIndex:(NSUInteger)index {
    [_glHandler insertSticker:sticker atIndex:index];
}
- (void)removeSticker:(APGLStickerFilter*)sticker {
    [_glHandler removeSticker:sticker];
}
- (void)cleanSticker {
    [_glHandler cleanSticker];
}

#pragma mark - AI 人脸识别

- (void)setFaceDetect:(BOOL)enable completionBlock:(void (^)(MLFaceDetectResult*, CVPixelBufferRef, CMTime))block {
    _enableFaceDetect = enable;
    _faceDetectCallbak = block;
    _faceDetectCount = 0;
}

- (void)setFaceDetectDataTransition:(BOOL)enable {
    _enableFaceDetectDataTransition = enable;
}

#pragma mark - AVCaptureVideoDataOutputSampleBufferDelegate & Audio

- (void)captureOutput:(AVCaptureOutput *)output didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection {
    // 视频输出
    if (output == _videoOutput) {
        [self openGLHandleBuffer:sampleBuffer];
    }
}

#pragma mark - OpenGLHandler & OpenGLHandlerDelegate

- (void)openGLHandleBuffer:(CMSampleBufferRef)sampleBuffer {
    [_glHandler handleSampleBuffer:sampleBuffer];
}

// OpenGL输出最终渲染图片
- (void)ouputVideoPixelBuffer:(CVPixelBufferRef)buffer sampleBuffer:(CMSampleBufferRef)sampleBuffer {
    __weak typeof(self) weakSelf = self;
    
    if (_delegate && [_delegate respondsToSelector:@selector(didOutput:sampleBuffer:)]) {
        [_delegate didOutput:buffer sampleBuffer:sampleBuffer];
    }
    
    CVPixelBufferRetain(buffer);
    CVPixelBufferRef old = _latestPixelBuffer;
    while (!OSAtomicCompareAndSwapPtrBarrier(old, buffer, (void **)&_latestPixelBuffer)) {
      old = _latestPixelBuffer;
    }
    if (old != nil) {
        CVPixelBufferRelease(old);
    }
    
    // 人脸识别
    if (_enableFaceDetect) {
        
        _faceDetectCount++;
        if (_faceDetectCount > 10) {
         
            _faceDetectCount = 0;
            
            CMTime deltaTime = kCMTimeZero;
            CMTime frameTime = CMSampleBufferGetPresentationTimeStamp(sampleBuffer);
            if (CMTIME_IS_VALID(_movieWriter.startTime)) {
                deltaTime = CMTimeSubtract(frameTime, _movieWriter.startTime);
            }
            
            CVPixelBufferRetain(buffer);
            [_faceDetect detectWithSampleBuffer:sampleBuffer
                               completeCallback:^(MLFaceDetectResult * _Nonnull result) {
                
                if (result.faces.count > 0 && weakSelf.enableFaceDetectDataTransition) {
                    result.data = [APImageDataTools dataWithPixelBuffer:buffer];
                }
                
                
                if (weakSelf.faceDetectCallbak) {
                    weakSelf.faceDetectCallbak(result, buffer, deltaTime);
                }
                CVPixelBufferRelease(buffer);
            }];
        }
    }
    
    // 通知Flutter纹理更新
    if (_textureNeedUpdate) {
        _textureNeedUpdate(_textureId);
    }
    
    // 录像
    if (_movieWriter) {
        CMTime frameTime = CMSampleBufferGetPresentationTimeStamp(sampleBuffer);
        [_movieWriter handlePixelBuffer:buffer frameTime:frameTime];
    }
}

#pragma mark - FlutterTexture

- (CVPixelBufferRef)copyPixelBuffer {
    __block CVPixelBufferRef pixelBuffer;
    pixelBuffer = _latestPixelBuffer;
    while (!OSAtomicCompareAndSwapPtrBarrier(pixelBuffer, nil, (void **)&_latestPixelBuffer)) {
        pixelBuffer = _latestPixelBuffer;
    }
    return pixelBuffer;
}

#pragma mark - APGLMovieWriterDelegate

- (void)movieWriterFailedWithError:(NSError *)error {
    
    NSLog(@"movie writer error %@", error);
}

@end
