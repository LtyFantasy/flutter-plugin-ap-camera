//
//  ApCameraDefines.h
//  Pods
//
//  Created by wooplus on 2021/5/7.
//

#ifndef ApCameraDefines_h
#define ApCameraDefines_h


#pragma mark - 错误码

typedef NS_ENUM(NSUInteger, ApCameraError) {
    
    // --------- 通用错误码 1xxx ---------
    
    ApCameraError_Common_InvalidParams = 1000,
    
    // --------- 摄像头、音视频相关 2xxx ---------
      
    // 合成视频失败
    ApCameraError_AV_ComposeFailed = 2020,
    
    // --------- 滤镜设置相关 3xxx ---------
    
    // 视频录制失败
    ApCameraError_Record_Failed = 2000,
    
    // --------- AI人脸识别相关 4xxx ---------
    
    // 人脸识别，MLKit返回error
    ApCameraError_AIFace_Detect = 4000,
    
    // 人脸识别，抓拍图片保存失败
    ApCameraError_AIFace_SavePhotoFile = 4001,
};

#endif /* ApCameraDefines_h */
