//
//  MLFaceDetech.m
//  ap_camera
//
//  Created by wooplus on 2021/5/6.
//

#import "MLFaceDetect.h"
#import <GoogleMLKit/MLKit.h>
#import "ApCameraDefines.h"
#import <Flutter/Flutter.h>

#pragma mark - Sub Class

@implementation MLFaceObject

- (NSDictionary *)toMap {
    return @{
        @"x":               @(_frame.origin.x),
        @"y":               @(_frame.origin.y),
        @"width":           @(_frame.size.width),
        @"height":          @(_frame.size.height),
        @"cameraWidth":     @(_cameraWidth),
        @"cameraHeight":    @(_cameraHeight),
        @"trackId":         @(_trackId),
        @"smile":           @(_smile),
        @"leftEyeOpen":     @(_leftEyeOpen),
        @"rightEyeOpen":    @(_rightEyeOpen)
    };
}

- (NSString *)description {
    return [NSString stringWithFormat:@"Face %zd, smile %f, l_eye %f, r_eye %f",
            _trackId, _smile, _leftEyeOpen, _rightEyeOpen];
}

@end

@implementation MLFaceDetectResult

- (NSDictionary *)toMap {
    
    NSMutableDictionary *map = [NSMutableDictionary dictionary];
    if (_error) {
        
        map[@"errorCode"] = @(ApCameraError_AIFace_Detect);
        map[@"errorMsg"] = _error.description;
    }
    else {
        
        NSMutableArray *list = [NSMutableArray array];
        if (_data) {
            map[@"data"] = [FlutterStandardTypedData typedDataWithBytes:_data];
        }
        map[@"list"] = list;
        if (_faces.count > 0) {
            for (MLFaceObject *f in _faces) {
                [list addObject:[f toMap]];
            }
        }
    }
    return map;
}

@end

#pragma mark - Main Class

@interface MLFaceDetect()

@property (nonatomic, assign) AVCaptureDevicePosition cameraPosition;
@property (nonatomic, strong) MLKFaceDetector *detector;


@end

@implementation MLFaceDetect

#pragma mark - Init

- (instancetype)initWithCameraPosition:(AVCaptureDevicePosition)position {
    
    if (self = [super init]) {
        _cameraPosition = position;
        [self dataInit];
    }
    return self;
}

- (void)dataInit {
 
    _detector = [MLKFaceDetector faceDetectorWithOptions:[self options]];
}

- (MLKFaceDetectorOptions*)options {
    
    MLKFaceDetectorOptions *options = [MLKFaceDetectorOptions new];
    options.performanceMode = MLKFaceDetectorPerformanceModeFast;
    options.landmarkMode = MLKFaceDetectorLandmarkModeNone;
    options.classificationMode = MLKFaceDetectorClassificationModeAll;
    return options;
}

#pragma mark - Data


- (UIImageOrientation)
  imageOrientationFromDeviceOrientation:(UIDeviceOrientation)deviceOrientation
                         cameraPosition:(AVCaptureDevicePosition)cameraPosition {
  switch (deviceOrientation) {
    case UIDeviceOrientationPortrait:
      return cameraPosition == AVCaptureDevicePositionFront ? UIImageOrientationLeftMirrored
                                                            : UIImageOrientationRight;

    case UIDeviceOrientationLandscapeLeft:
      return cameraPosition == AVCaptureDevicePositionFront ? UIImageOrientationDownMirrored
                                                            : UIImageOrientationUp;
    case UIDeviceOrientationPortraitUpsideDown:
      return cameraPosition == AVCaptureDevicePositionFront ? UIImageOrientationRightMirrored
                                                            : UIImageOrientationLeft;
    case UIDeviceOrientationLandscapeRight:
      return cameraPosition == AVCaptureDevicePositionFront ? UIImageOrientationUpMirrored
                                                            : UIImageOrientationDown;
    case UIDeviceOrientationUnknown:
    case UIDeviceOrientationFaceUp:
    case UIDeviceOrientationFaceDown:
      return UIImageOrientationUp;
  }
}

#pragma mark - Face Detect

- (void)detectWithImage:(UIImage *)img completeCallback:(void (^)(MLFaceDetectResult * _Nonnull))callback {
    
    MLKVisionImage *image = [[MLKVisionImage alloc] initWithImage:img];
    image.orientation = img.imageOrientation;
    
    [_detector processImage:image
                 completion:^(NSArray<MLKFace *> * _Nullable faces, NSError * _Nullable error) {

            
        if (callback) {
            
            MLFaceDetectResult *result = [MLFaceDetectResult new];
            result.error = error;
            
            NSMutableArray *array = [NSMutableArray array];
            // 获取人脸数据
            for (MLKFace *face in faces) {

                MLFaceObject *obj = [MLFaceObject new];
                obj.frame = face.frame;
                obj.trackId = face.trackingID;
                obj.smile = face.smilingProbability;
                obj.leftEyeOpen = face.leftEyeOpenProbability;
                obj.rightEyeOpen = face.rightEyeOpenProbability;
                [array addObject:obj];
            }
            
            result.faces = array;
            callback(result);
        }
    }];
}

- (void)detectWithSampleBuffer:(CMSampleBufferRef)sampleBuffer
              completeCallback:(void(^)(MLFaceDetectResult *))callback {
    

    MLKVisionImage *image = [[MLKVisionImage alloc] initWithBuffer:sampleBuffer];
    //image.orientation = [self imageOrientationFromDeviceOrientation:UIDevice.currentDevice.orientation cameraPosition:_cameraPosition];
    image.orientation = UIImageOrientationUp;
    
    [_detector processImage:image
                 completion:^(NSArray<MLKFace *> * _Nullable faces, NSError * _Nullable error) {

            
        if (callback) {
            
            MLFaceDetectResult *result = [MLFaceDetectResult new];
            result.error = error;
            
            NSMutableArray *array = [NSMutableArray array];
            // 获取人脸数据
            for (MLKFace *face in faces) {

                MLFaceObject *obj = [MLFaceObject new];
                obj.frame = face.frame;
                obj.cameraWidth = 720;
                obj.cameraHeight= 1280;
                obj.trackId = face.trackingID;
                obj.smile = face.smilingProbability;
                obj.leftEyeOpen = face.leftEyeOpenProbability;
                obj.rightEyeOpen = face.rightEyeOpenProbability;
                [array addObject:obj];
            }
            
            result.faces = array;
            callback(result);
        }
    }];
}

@end
