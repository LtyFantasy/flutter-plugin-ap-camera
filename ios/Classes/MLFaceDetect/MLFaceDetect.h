//
//  MLFaceDetech.h
//  ap_camera
//
//  Created by wooplus on 2021/5/6.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MLFaceObject : NSObject

@property (nonatomic, assign) CGRect frame;
@property (nonatomic, assign) CGFloat cameraWidth;
@property (nonatomic, assign) CGFloat cameraHeight;
@property (nonatomic, assign) NSInteger trackId;
@property (nonatomic, assign) CGFloat smile;
@property (nonatomic, assign) CGFloat leftEyeOpen;
@property (nonatomic, assign) CGFloat rightEyeOpen;

- (NSDictionary*)toMap;

@end

@interface MLFaceDetectResult : NSObject

@property (nonatomic, strong) NSError *error;
@property (nonatomic, strong) NSData *data;
@property (nonatomic, strong) NSArray<MLFaceObject*> *faces;

- (NSDictionary*)toMap;

@end

@interface MLFaceDetect : NSObject

- (instancetype)initWithCameraPosition:(AVCaptureDevicePosition)position;
- (void)detectWithImage:(UIImage*)image
       completeCallback:(void(^)(MLFaceDetectResult *result))callback;
- (void)detectWithSampleBuffer:(CMSampleBufferRef)sampleBuffer
              completeCallback:(void(^)(MLFaceDetectResult *result))callback;

@end

NS_ASSUME_NONNULL_END
