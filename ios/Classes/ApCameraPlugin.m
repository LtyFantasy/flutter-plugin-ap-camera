#import "ApCameraPlugin.h"
#import "ApCameraDefines.h"
#import "CameraManager.h"
#import "Camera.h"
#import "MLFaceDetect.h"
#import "APGLStickerManager.h"
#import "APMediaComposer.h"

#import <objc/runtime.h>
#import <objc/message.h>

#pragma mark - Defines

#define StrSEL(x)   NSStringFromSelector(@selector(x))

// ------------ Method 参数 -------------

#define kMethodParam_Call                           @"call"
#define kMethodParam_Result                         @"result"

// ------------ Method API定义 -------------

// 获取摄像头列表
#define kMethod_AvailableCameras                    @"availableCameras"
// 创建摄像头
#define kMethod_CreateCamera                        @"createCamera"
// 销毁摄像头
#define kMethod_ReleaseCamera                       @"releaseCamera"
// 开始获取视频数据
#define kMethod_StartCamera                         @"startCamera"
// 恢复获取视频数据
#define kMethod_ResumeCamera                        @"resumeCamera"
// 暂停获取视频数据
#define kMethod_PauseCamera                         @"pauseCamera"
// 开始录制视频
#define kMethod_StartRecord                         @"startRecord"
// 停止录制视频
#define kMethod_StopRecord                          @"stopRecord"
// 取消录制视频
#define kMethod_CancelRecord                        @"cancelRecord"
// 获取视频文件列表
#define kMethod_GetVideoFiles                       @"getVideoFiles"
// 抓拍当前视频图片
#define kMethod_TakePhoto                           @"takePhoto"
// 获取图片文件列表
#define kMethod_GetPhotoFiles                       @"getPhotoFiles"

// --- 缓存 ---

// 清理所有缓存
#define kMethod_CleanAllCache                       @"cleanAllCache"

// --- 滤镜 ---

// 美颜开关控制
#define kMethod_SetBeautifyFilter                   @"setBeautifyFilter"

// 美颜滤镜参数设置
#define kMethod_SetBeautifyFilterParams             @"setBeautifyFilterParams"

// -- 贴纸 --

// 获取所有表情贴纸
#define kMethod_GetAllEmojiStickers                 @"getAllEmojiStickers"

// 设置指定的贴纸资源
#define kMethod_UseEmojis                           @"useEmojis"

// 设置是否展示Emoji
#define kMethod_ShowEmoji                           @"showEmoji"

// 播放Emoji，一组Emoji都开始Gif轮播
#define kMethod_StartEmojiGif                       @"startEmojiGif"

// 停止Emoji，一组Emoji都停止Gif轮播
#define kMethod_StopEmojiGif                        @"stopEmojiGif"

// 展示Emoji 阶段1动画（循环动画）
#define kMethod_StartEmojiAnimation1                @"startEmojiAnimation1"

// 停止Emoji 阶段1动画
#define kMethod_StopEmojiAnimation1                 @"stopEmojiAnimation1"

// 展示Emoji 阶段2动画（依次缩放+播放Gif，结束后自动停止）
#define kMethod_StartEmojiAnimation2                @"startEmojiAnimation2"

// --- AI 人脸识别 ---

// 识别单张图片的人脸数
#define kMethod_DetectFaceNumWithImageFile          @"detectFaceNumWithImageFile"

// 人脸识别 开关控制
#define kMethod_SetFaceDetect                       @"setFaceDetect"

// 开启 人脸识别 抓拍表情功能
#define kMethod_StartFaceDetectTakePhoto            @"startFaceDetectTakePhoto"

// 开关、关闭 人脸识别 图片二进制数据传输
#define kMethod_SetFaceDetectPhotoDataEnable        @"setFaceDetectPhotoDataEnable"

// --- 玩法 ---

// 开启认证
#define kMethod_StartVerify                         @"startVerify"

// 中途停止认证
#define kMethod_StopVerify                          @"stopVerify"

// 开始合成视频+BGM
#define kMethod_StartVideoBGMCompose                @"startVideoBGMCompose"

// ------------ Event API定义 -------------

// Event错误定义
#define kEvent_Param_ErrorCode                      @"errorCode"
#define kEvent_Param_ErrorMsg                       @"errorMsg"

// 贴纸资源加载完毕
#define kEvent_StickerInitSuccess                   @"eventStickerInitSuccess"

// 人脸识别结果通知
#define kEvent_FaceDetect                           @"eventFaceDetect"

// 人脸抓拍结果通知
#define kEvent_FaceDetectTakePhoto                  @"eventFaceDetectTakePhoto"

// Emoji贴纸动画组，动画执行完毕
#define kEvent_StickerAnimationComplete             @"eventStickerAnimationComplete"

// 录制中，时间进度通知
#define kEvent_RecordingProgress                    @"eventRecordingProgress"

// 认证录制结束
#define kEvent_VerifyRecordComplete                 @"eventVerifyRecordComplete"

// 视频合成进度通知
#define kEvent_VideoBGMCompose                      @"eventVideoBGMCompose"

#pragma mark - Main Class

@interface ApCameraPlugin ()

@property (nonatomic, strong) NSObject<FlutterTextureRegistry> *registry;
@property (nonatomic, strong) NSObject<FlutterBinaryMessenger> *messenger;

@property (nonatomic, strong) FlutterMethodChannel *channel;
@property (nonatomic, strong) NSDictionary<NSString*, NSString*> *methodMap;

@end

@implementation ApCameraPlugin

#pragma mark - Init

- (instancetype)initWithRegistry:(NSObject<FlutterTextureRegistry> *)registry
                       messenger:(NSObject<FlutterBinaryMessenger> *)messenger
                         channel:(FlutterMethodChannel*)channel {
    if (self = [super init]) {
        _registry = registry;
        _messenger = messenger;
        _channel = channel;
        [self methodMapInit];
    }
    return self;
}

/// 初始化创建方法映射表
- (void)methodMapInit {
    
    _methodMap = @{
        kMethod_AvailableCameras:               StrSEL(methodAvailableCameras:result:),
        kMethod_CreateCamera:                   StrSEL(methodCreateCamera:result:),
        kMethod_ReleaseCamera:                  StrSEL(methodReleaseCamera:result:),
        kMethod_StartCamera:                    StrSEL(methodStartCamera:result:),
        kMethod_ResumeCamera:                   StrSEL(methodResumeCamera:result:),
        kMethod_PauseCamera:                    StrSEL(methodPauseCamera:result:),
        kMethod_StartRecord:                    StrSEL(methodStartRecord:result:),
        kMethod_StopRecord:                     StrSEL(methodStopRecord:result:),
        kMethod_CancelRecord:                   StrSEL(methodCancelRecord:result:),
        kMethod_GetVideoFiles:                  StrSEL(methodGetVideoFiles:result:),
        kMethod_TakePhoto:                      StrSEL(methodTakePhoto:result:),
        kMethod_GetPhotoFiles:                  StrSEL(methodGetPhotoFiles:result:),
        // --- 缓存 ---
        kMethod_CleanAllCache:                  StrSEL(methodCleanAllCache:result:),
        // --- 滤镜 ---
        kMethod_SetBeautifyFilter:              StrSEL(methodSetBeautifyFilter:result:),
        kMethod_SetBeautifyFilterParams:        StrSEL(methodSetBeautifyFilterParams:result:),
        // --- 贴纸 ---
        kMethod_GetAllEmojiStickers:            StrSEL(methodGetAllEmojiStickers:result:),
        kMethod_UseEmojis:                      StrSEL(methodUseEmojis:result:),
        kMethod_ShowEmoji:                      StrSEL(methodShowEmoji:result:),
        kMethod_StartEmojiGif:                  StrSEL(methodStartEmojiGif:result:),
        kMethod_StopEmojiGif:                   StrSEL(methodStopEmojiGif:result:),
        kMethod_StartEmojiAnimation1:           StrSEL(methodStartEmojiAnimation1:result:),
        kMethod_StopEmojiAnimation1:            StrSEL(methodStopEmojiAnimation1:result:),
        kMethod_StartEmojiAnimation2:           StrSEL(methodStartEmojiAnimation2:result:),
        // --- AI 人脸识别 ---
        kMethod_DetectFaceNumWithImageFile:     StrSEL(methodDetectFaceNumWithImageFile:result:),
        kMethod_SetFaceDetect:                  StrSEL(methodSetFaceDetect:result:),
        kMethod_StartFaceDetectTakePhoto:       StrSEL(methodStartFaceDetectTakePhoto:result:),
        kMethod_SetFaceDetectPhotoDataEnable:   StrSEL(methodSetFaceDetectPhotoDataEnable:result:),
        // --- 玩法 ---
        kMethod_StartVerify:                    StrSEL(methodStartVerify:result:),
        kMethod_StopVerify:                     StrSEL(methodStopVerify:result:),
        kMethod_StartVideoBGMCompose:           StrSEL(methodStartVideoBGMCompose:result:),
    };
}

#pragma mark - Plugin

+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
    
    FlutterMethodChannel* channel = [FlutterMethodChannel methodChannelWithName:@"com.aplus.plugin.camera"
                                                              binaryMessenger:[registrar messenger]];
    ApCameraPlugin* instance = [[ApCameraPlugin alloc] initWithRegistry:[registrar textures]
                                                              messenger:[registrar messenger]
                                                                channel:channel];
    [registrar addMethodCallDelegate:instance channel:channel];
    [registrar addApplicationDelegate:instance];
}

- (void)handleMethodCall:(FlutterMethodCall*)call result:(FlutterResult)result {
    
    NSString *selectorString = _methodMap[call.method];
    if (selectorString) {
        SEL selector = NSSelectorFromString(selectorString);
        ((void(*)(id, SEL ,FlutterMethodCall*, FlutterResult))objc_msgSend)(self, selector, call, result);
    }
    else {
        result(FlutterMethodNotImplemented);
    }
}

#pragma mark - App Life Cycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [APGLStickerManager instance];
    
    return YES;
}

#pragma mark - Method - Camera

- (void)methodAvailableCameras:(FlutterMethodCall*)call result:(FlutterResult)result {
    result([CameraManager getPositionDevices]);
}

- (void)methodCreateCamera:(FlutterMethodCall*)call result:(FlutterResult)result {
    
    Camera *camera = CameraManager.instance.camera;
    if (camera) [camera close];
    
    NSString *positionString = call.arguments[@"position"];
    AVCaptureDevicePosition position = cameraPositionForString(positionString);
    
    NSString *resolutionString = call.arguments[@"resolution"];
    CameraResolution resolution = cameraResolutionForString(resolutionString);
    
    BOOL enableAudio = call.arguments[@"enableAudio"];
    
    NSError *error;
    camera = [CameraManager.instance createCameraWithPosition:position
                                                  enableAudio:enableAudio
                                                   resolution:resolution
                                                        error:&error];
    
    if (error) {
        result([FlutterError errorWithCode:@"Camera create failed"
                                   message:error.description
                                   details:nil]);
        return;
    }
    
    int64_t textureId = [_registry registerTexture:camera];
    camera.textureId = textureId;
    
    NSDictionary *dict = @{
        @"textureId": @(textureId),
        @"width": @(camera.previewSize.width),
        @"height": @(camera.previewSize.height)
    };
    result(dict);
}

- (void)methodReleaseCamera:(FlutterMethodCall*)call result:(FlutterResult)result {
    
    Camera *camera = CameraManager.instance.camera;
    if (camera) {
        [_registry unregisterTexture:camera.textureId];
        [CameraManager.instance releaseCamera];
    }
    result(nil);
}

- (void)methodStartCamera:(FlutterMethodCall*)call result:(FlutterResult)result {
    
    __weak typeof(self) weakSelf = self;
    [CameraManager.instance startCameraWithTextureUpdate:^(NSInteger textureId) {
        [weakSelf.registry textureFrameAvailable:textureId];
    }];
    result(nil);
}

- (void)methodResumeCamera:(FlutterMethodCall*)call result:(FlutterResult)result {
    
    [CameraManager.instance resumeCamera];
    result(nil);
}

- (void)methodPauseCamera:(FlutterMethodCall*)call result:(FlutterResult)result {
    
    [CameraManager.instance pauseCamera];
    result(nil);
}

- (void)methodStartRecord:(FlutterMethodCall*)call result:(FlutterResult)result {
    
    BOOL success = [CameraManager.instance startVideoRecordWithProgressUpdate:^(NSUInteger timeMS) {
        
    }];
    result( success ? @(1) : @(0) );
}

- (void)methodStopRecord:(FlutterMethodCall*)call result:(FlutterResult)result {
    
    [CameraManager.instance stopVideoRecordWithCompletionBlock:^(NSString *filepath, BOOL success) {
        result(filepath);
    }];
}

- (void)methodCancelRecord:(FlutterMethodCall*)call result:(FlutterResult)result {
    
    [CameraManager.instance cancelVideoRecord];
    result(nil);
}

- (void)methodGetVideoFiles:(FlutterMethodCall*)call result:(FlutterResult)result {
    result([CameraManager.instance getVideoFiles]);
}

- (void)methodTakePhoto:(FlutterMethodCall*)call result:(FlutterResult)result {
    
    [CameraManager.instance takePhotoWithCompletionBlock:^(NSString *filepath, BOOL success) {
        result(filepath);
    }];
}

- (void)methodGetPhotoFiles:(FlutterMethodCall*)call result:(FlutterResult)result {
    result([CameraManager.instance getPhotoFiles]);
}

#pragma mark - Method - Cache

- (void)methodCleanAllCache:(FlutterMethodCall*)call result:(FlutterResult)result {
    
    [CameraManager.instance cleanAllCache];
    result(nil);
}

#pragma mark - Method - Filter

- (void)methodSetBeautifyFilter:(FlutterMethodCall*)call result:(FlutterResult)result {
    
    BOOL enable = [call.arguments[@"enable"] integerValue] > 0;
    [CameraManager.instance setEnableBeautyFilter:enable];
    result(nil);
}

- (void)methodSetBeautifyFilterParams:(FlutterMethodCall*)call result:(FlutterResult)result {
    
    CGFloat bright = [call.arguments[@"bright"] floatValue];
    CGFloat beauty = [call.arguments[@"beauty"] floatValue];
    CGFloat tone = [call.arguments[@"tone"] floatValue];
    [CameraManager.instance adjustBright:bright beauty:beauty tone:tone];
    result(nil);
}

- (void)methodGetAllEmojiStickers:(FlutterMethodCall*)call result:(FlutterResult)result {
    
    NSMutableArray *list = [NSMutableArray array];
    for (APGLSticker *s in APGLStickerManager.instance.stickers) {
        [list addObject:s.name];
    }
    result(list);
}

- (void)methodUseEmojis:(FlutterMethodCall*)call result:(FlutterResult)result {
    
    NSArray *emojis = call.arguments[@"emojis"];
    [CameraManager.instance useEmojis:emojis];
    result(nil);
}

- (void)methodShowEmoji:(FlutterMethodCall*)call result:(FlutterResult)result {
 
    BOOL enable = [call.arguments[@"enable"] integerValue] > 0;
    [CameraManager.instance setEmojiShow:enable];
    result(nil);
}

- (void)methodStartEmojiGif:(FlutterMethodCall*)call result:(FlutterResult)result {
 
    [CameraManager.instance emojiGifStart];
    result(nil);
}

- (void)methodStopEmojiGif:(FlutterMethodCall*)call result:(FlutterResult)result {
 
    [CameraManager.instance emojiGifStop];
    result(nil);
}

- (void)methodStartEmojiAnimation1:(FlutterMethodCall*)call result:(FlutterResult)result {
 
    result(nil);
}

- (void)methodStopEmojiAnimation1:(FlutterMethodCall*)call result:(FlutterResult)result {
 
    result(nil);
}

- (void)methodStartEmojiAnimation2:(FlutterMethodCall*)call result:(FlutterResult)result {
 
    [CameraManager.instance startEmojiAnimation2WithComplete:nil];
    result(nil);
}

#pragma mark - Method - AI Face

- (void)methodDetectFaceNumWithImageFile:(FlutterMethodCall*)call result:(FlutterResult)result {
    
    @try {
        NSString *file = call.arguments[@"file"];
        UIImage *image = [UIImage imageWithContentsOfFile:file];
        if (!image) {
            result(@(0));
            return;
        }
        
        MLFaceDetect *detect = [[MLFaceDetect alloc] initWithCameraPosition:AVCaptureDevicePositionFront];
        [detect detectWithImage:image completeCallback:^(MLFaceDetectResult * _Nonnull detectResult) {
            result(@(detectResult.faces.count));
        }];
    }
    @catch(NSException *e) {
        result(@(0));
    }
}

- (void)methodSetFaceDetect:(FlutterMethodCall*)call result:(FlutterResult)result {
    
    BOOL enable = [call.arguments[@"enable"] integerValue] > 0;
    if (enable) {
        
        __weak typeof(self) weakSelf = self;
        [CameraManager.instance startFaceDetect:^(MLFaceDetectResult *detectResult) {
            [weakSelf.channel invokeMethod:kEvent_FaceDetect
                                 arguments:[detectResult toMap]];
        }];
    }
    else {
        [CameraManager.instance stopFaceDetect];
    }
    result(nil);
}

- (void)methodStartFaceDetectTakePhoto:(FlutterMethodCall*)call result:(FlutterResult)result {
    
    __weak typeof(self) weakSelf = self;
    
    BOOL savePhoto = [call.arguments[@"savePhoto"] integerValue] > 0;
    [CameraManager.instance startFaceDetectTakePhoto:savePhoto
                                               block:^(MLFaceDetectTakePhotoResult *result) {
        [weakSelf.channel invokeMethod:kEvent_FaceDetectTakePhoto
                             arguments:[result toMap]];
    }];
    result(nil);
}

- (void)methodSetFaceDetectPhotoDataEnable:(FlutterMethodCall*)call result:(FlutterResult)result {
 
    BOOL enable = [call.arguments[@"enable"] integerValue] > 0;
    [CameraManager.instance enableFaceDetectDataTransition:enable];
    result(nil);
}

#pragma mark - Method - 玩法

- (void)methodStartVerify:(FlutterMethodCall*)call result:(FlutterResult)result {
    
    __weak typeof(self) weakSelf = self;
    NSThread *originalThread = [NSThread currentThread];
    // 开启录制
    [CameraManager.instance startVideoRecordWithProgressUpdate:^(NSUInteger timeMs) {
        // 通知录制进度
        [weakSelf performSelector:@selector(progressUpdate:) onThread:originalThread withObject:@(timeMs) waitUntilDone:NO];
    }];
    
    // 延迟0.8秒 开启Emoji动画
    dispatch_time_t delayTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(800 * NSEC_PER_MSEC));
    dispatch_after(delayTime, dispatch_get_main_queue(), ^{
        [CameraManager.instance startEmojiAnimation2WithComplete:nil];
    });
    
    result(nil);
}

- (void)progressUpdate:(NSNumber *)timeMs {
    // 通知录制进度
    [self.channel invokeMethod:kEvent_RecordingProgress arguments:@{
        @"timeMS": timeMs
    }];
    
    // 录制11秒
    if ([timeMs integerValue] >= 11*1000) {
        __weak typeof(self) weakSelf = self;
        [CameraManager.instance stopVideoRecordWithCompletionBlock:^(NSString *filepath, BOOL success) {
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            if (success) {
                dict[@"videoFile"] = filepath;
            }
            else {
                dict[kEvent_Param_ErrorCode] = @(ApCameraError_Record_Failed);
                dict[kEvent_Param_ErrorMsg] = @"Record failed";
            }
            [weakSelf.channel invokeMethod:kEvent_VerifyRecordComplete arguments:dict];
        }];
    }
}


- (void)methodStopVerify:(FlutterMethodCall*)call result:(FlutterResult)result {
    
    // 停止录制
    [CameraManager.instance cancelVideoRecord];
    // 停止Emoji动画
    [CameraManager.instance stopEmojiAnimation2];
    
    result(nil);
}

- (void)methodStartVideoBGMCompose:(FlutterMethodCall*)call result:(FlutterResult)result {
 
    NSString *videoFile = call.arguments[@"videoFile"];
    if (videoFile.length > 0) {
        
        
        __weak typeof(self) weakSelf = self;
        [CameraManager.instance startComposeVideo:videoFile
                                            audio:[APMediaComposer audioFile]
                                    completeBlock:^(BOOL success, NSString *outputFile, NSError *error) {
            
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            if (success) {
                dict[@"videoFile"] = outputFile;
            }
            else {
                dict[kEvent_Param_ErrorCode] = @(ApCameraError_AV_ComposeFailed);
                dict[kEvent_Param_ErrorMsg] = error.description ?: @"compose failed";
            }
            [weakSelf.channel invokeMethod:kEvent_VideoBGMCompose arguments:dict];
        }];
    }
    else {
        
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[kEvent_Param_ErrorCode] = @(ApCameraError_Common_InvalidParams);
        dict[kEvent_Param_ErrorMsg] = @"videoFile is empty";
        [_channel invokeMethod:kEvent_VideoBGMCompose arguments:dict];
    }
    
    result(nil);
}

@end
