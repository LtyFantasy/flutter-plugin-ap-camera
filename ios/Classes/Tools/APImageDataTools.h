//
//  APImageDataTools.h
//  ap_camera
//
//  Created by wooplus on 2021/7/22.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface APImageDataTools : NSObject

+ (UIImage*)imageWithPixelBuffer:(CVPixelBufferRef)buffer;
+ (NSData*)dataWithPixelBuffer:(CVPixelBufferRef)buffer;

@end

NS_ASSUME_NONNULL_END
