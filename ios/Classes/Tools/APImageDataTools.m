//
//  APImageDataTools.m
//  ap_camera
//
//  Created by wooplus on 2021/7/22.
//

#import "APImageDataTools.h"

@implementation APImageDataTools

+ (UIImage*)imageWithPixelBuffer:(CVPixelBufferRef)buffer {
    
    CVPixelBufferLockBaseAddress(buffer, 0);

    // 从 CVImageBufferRef 取得影像的细部信息
    uint8_t *base;
    size_t width, height, bytesPerRow;
    base = CVPixelBufferGetBaseAddress(buffer);
    width = CVPixelBufferGetWidth(buffer);
    height = CVPixelBufferGetHeight(buffer);
    bytesPerRow = CVPixelBufferGetBytesPerRow(buffer);

    // 利用取得影像细部信息格式化 CGContextRef
    CGColorSpaceRef colorSpace;
    CGContextRef cgContext;
    colorSpace = CGColorSpaceCreateDeviceRGB();
    cgContext = CGBitmapContextCreate(base, width, height, 8, bytesPerRow, colorSpace, kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst);
    CGColorSpaceRelease(colorSpace);

    // 透过 CGImageRef 将 CGContextRef 转换成 UIImage
    CGImageRef cgImage;
    UIImage *image;
    cgImage = CGBitmapContextCreateImage(cgContext);
    image = [UIImage imageWithCGImage:cgImage];
    CGImageRelease(cgImage);
    CGContextRelease(cgContext);

    CVPixelBufferUnlockBaseAddress(buffer, 0);
    return image;
}

+ (NSData*)dataWithPixelBuffer:(CVPixelBufferRef)buffer {
    
    UIImage *image = [self imageWithPixelBuffer:buffer];
    NSData *data = UIImageJPEGRepresentation(image, 0.2);
    return data;
}

@end
